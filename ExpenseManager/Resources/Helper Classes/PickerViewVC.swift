//
//  PickerViewVC.swift
//  ExpenseManager
//
//  Created by Piyu on 02/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
protocol PickerViewDelegate {
    func getSelectedPaymentType(strPType: String)
    func getSelectedRecurrentType(strRType: String,selectedIndex:Int)
    func getSelectedAccount(strAType: String,accountID: Int32)
    
}
enum TransactionType: Int {
    case payment = 0
    case recurrent = 1
    case account = 2
}

class PickerViewVC: UIViewController {

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var viewToolBar: UIView!

    
    var selctedType: TransactionType = .payment
    var delegate: PickerViewDelegate?
    var arrPaymentType = [String]()
    var arrRecurrentType = [String]()
    var arrAccount = [AccountInfo]()
    var selcatedValue : String = ""
    var selcatedIndex : Int = 0
    var finalAccountId : Int32 = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        viewToolBar.backgroundColor = UIColor(hexString: getSelectedThemeColor())
        btnDone.setTitle(AppDelegate.sharedInstance.LocalizedString(text: "Save"), for: .normal)
        btnCancel.setTitle(AppDelegate.sharedInstance.LocalizedString(text: "Cancel"), for: .normal)
        if self.selctedType == .payment {
            lblTitle.text = Messages.selectpaymenttype
            self.arrPaymentType.enumerated().forEach { (index,object) in
                if object == selcatedValue {
                    self.selcatedIndex = index
                    self.pickerView.selectRow(index, inComponent: 0, animated: true)
                }
            }
        }else if self.selctedType == .recurrent{
            lblTitle.text = Messages.selectrecurrenttype

            self.arrRecurrentType.enumerated().forEach { (index,object) in
                if object == selcatedValue {
                    self.selcatedIndex = index
                    self.pickerView.selectRow(index, inComponent: 0, animated: true)
                }
            }
        }else{
            lblTitle.text = Messages.selectaccount

            self.arrAccount.enumerated().forEach { (index,object) in
                if object.name == selcatedValue {
                    self.selcatedIndex = index
                    self.pickerView.selectRow(index, inComponent: 0, animated: true)
                }
            }
        }

    }


}
extension PickerViewVC {
    @IBAction func btnCancleTapped(_ sender: UIButton) {
        dismissView()
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        
        if self.selctedType == .payment {
             delegate?.getSelectedPaymentType(strPType: selcatedValue)
        }else if self.selctedType == .recurrent{
            if delegate != nil {
                delegate?.getSelectedRecurrentType(strRType: selcatedValue, selectedIndex: selcatedIndex)
            }
        }else{
            if delegate != nil {
                delegate?.getSelectedAccount(strAType: selcatedValue, accountID: finalAccountId)
            }
        }
        
        dismissView()
    }
    func dismissView() {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
}
// MARK:- Extension: UIPickerViewDelegate

extension PickerViewVC: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if self.selctedType == .payment {
             return arrPaymentType.count
        }else if self.selctedType == .recurrent{
            return arrRecurrentType.count
        }else{
            return arrAccount.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.selctedType == .payment {
            return arrPaymentType[row]
        }else if self.selctedType == .recurrent{
            return arrRecurrentType[row]
        }else{
            return arrAccount[row].name
        }
       
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){

        if self.selctedType == .payment {
            selcatedValue = arrPaymentType[row]
            self.selcatedIndex = row
        }else if self.selctedType == .recurrent{
            selcatedValue = arrRecurrentType[row]
            self.selcatedIndex = row
        }else{
            selcatedValue = arrAccount[row].name!
            self.selcatedIndex = row
            finalAccountId = arrAccount[row].id!
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label: UILabel? = nil
        
        if let viewTemp = view as? UILabel {
            label = viewTemp
        } else {
            label = UILabel(frame: .init(x: 0, y: 0, width: pickerView.frame.width, height: 40))
        }
        
        label?.font = FONT(FontName.roboto_regular.rawValue, size: FontSize.navigationTitle)
        if self.selctedType == .payment {
             label?.text = arrPaymentType[row]
        }else if self.selctedType == .recurrent{
             label?.text = arrRecurrentType[row]
        }else{
             label?.text = arrAccount[row].name!
            finalAccountId = arrAccount[row].id!
        }
        label?.textAlignment = .center
        label?.textColor = UIColor.black
        return label!
    }
}

