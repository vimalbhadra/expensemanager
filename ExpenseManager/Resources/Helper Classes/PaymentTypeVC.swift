//
//  FilerTypeView.swift
//  ExpenseManager
//
//  Created by Apple Customer on 18/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

struct PaymentType {
    
    var titleLeft: (String, Bool, Bool)
    var titleRight: (String, Bool)
    
    init(titleLeft: (String, Bool, Bool), titleRight: (String, Bool)) {
        self.titleLeft = titleLeft
        self.titleRight = titleRight
    }
}

protocol PaymentTypeVCDelegate {
    func getPaymentTypeArray(arrPaymentType: [PaymentType])
}

class PaymentTypeVC: UIViewController {

    @IBOutlet weak var viewTransparentBackground: UIView!
    @IBOutlet weak var viewCenterPaymentType: UIView!
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var tblViewPaymentType: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    var arrPaymentType = [PaymentType]() {
        didSet {
            reloadData()
        }
    }
    var delegate: PaymentTypeVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
}

extension PaymentTypeVC {
    
    func initialSetup() {
        
        lblPaymentType.backgroundColor = UIColor(hexString: getSelectedThemeColor())
        btnCancel.setTitleColor(UIColor(hexString: getSelectedThemeColor()), for: .normal)
        btnSave.setTitleColor(UIColor(hexString: getSelectedThemeColor()), for: .normal)
        btnSave.setTitle(AppDelegate.sharedInstance.LocalizedString(text: "Save"), for: .normal)
        btnCancel.setTitle(AppDelegate.sharedInstance.LocalizedString(text: "Cancel"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTransparentView))
        self.viewTransparentBackground.addGestureRecognizer(tap)
        
        self.showHidePaymentView(isShow: true)
    }
    
    @objc func tapOnTransparentView() {
        self.showHidePaymentView(isShow: false)
    }
    
    func reloadData() {
        runOnMainThread {
            self.tblViewPaymentType.reloadData()
        }
    }
    
    func showHidePaymentView(isShow: Bool) {
        
        runOnMainThread {
            self.viewCenterPaymentType.transform = isShow ? CGAffineTransform(scaleX: 0.001, y: 0.001) : CGAffineTransform.identity
            self.viewCenterPaymentType.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.viewCenterPaymentType.transform = isShow ? CGAffineTransform.identity : CGAffineTransform(scaleX: 0.001, y: 0.001)
                self.viewTransparentBackground.alpha = isShow ? 0.5 : 0
            }) { (result) in
                if !isShow {
                    self.view.removeFromSuperview()
                }
            }
        }
    }
    
     @objc func btnLeftRightTapped(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if sender.titleLabel?.text == AppDelegate.sharedInstance.LocalizedString(text: PaymentTypeName.All.rawValue) {
            
            arrPaymentType = arrPaymentType.map({ (object) -> PaymentType in
                
                var temp = object
                if temp.titleLeft.2 {
                    temp.titleLeft.1 = sender.isSelected
                } else {
                    temp.titleLeft.1 = false
                }
                temp.titleRight.1 = false
                return temp
            })
            
        } else {
            
            if let index = arrPaymentType.index(where: { (object) -> Bool in
                return AppDelegate.sharedInstance.LocalizedString(text: object.titleLeft.0) == sender.titleLabel?.text
            }) {
                arrPaymentType[index].titleLeft.1 = sender.isSelected
                
            } else  if let index = arrPaymentType.index(where: { (object) -> Bool in
                return AppDelegate.sharedInstance.LocalizedString(text: object.titleRight.0) == sender.titleLabel?.text
            }) {
                arrPaymentType[index].titleRight.1 = sender.isSelected
            }
            
            arrPaymentType = arrPaymentType.map({ (object) -> PaymentType in
                
                var temp = object
                if temp.titleLeft.2 {
                    temp.titleLeft.1 = false
                }
                return temp
            })
        }
    }
}

extension PaymentTypeVC {
    
    @IBAction func btnCancleTapped(_ sender: UIBarButtonItem) {
        showHidePaymentView(isShow: false)
    }
    
    @IBAction func btnSaveTapped(_ sender: UIBarButtonItem) {
        
        if !arrPaymentType.contains(where: { (object) -> Bool in
            return object.titleLeft.1 || object.titleRight.1
        }) {
            if let index = arrPaymentType.index(where: { (object) -> Bool in
                return object.titleLeft.2
            }) {
                arrPaymentType[index].titleLeft.1 = true
            }
        }
        
        if delegate != nil {
            delegate?.getPaymentTypeArray(arrPaymentType: arrPaymentType)
        }
        showHidePaymentView(isShow: false)
    }
}

extension PaymentTypeVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPaymentType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.PaymentTypeTVC) as! PaymentTypeTVC
        
        let object = arrPaymentType[indexPath.row]
        
        cell.btnLeft.addTarget(self, action: #selector(btnLeftRightTapped(_:)), for: .touchUpInside)
        cell.btnRight.addTarget(self, action: #selector(btnLeftRightTapped(_:)), for: .touchUpInside)
        
        cell.btnLeft.setTitle(AppDelegate.sharedInstance.LocalizedString(text: object.titleLeft.0), for: .normal)
        cell.btnRight.setTitle(AppDelegate.sharedInstance.LocalizedString(text: object.titleRight.0), for: .normal)
        
        cell.btnLeft.isSelected = object.titleLeft.1
        cell.btnRight.isSelected = object.titleRight.1
        
        return cell
    }
}
