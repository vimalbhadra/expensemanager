//
//  PasscodeView.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 13/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import Foundation
import LocalAuthentication

class PasscodeManager: NSObject {
    
    static let sharedManager = PasscodeManager()
    
    func addRemoveBlurView(isAdd: Bool) {
        
        if let rootViewController = APPDELEGATE.window?.rootViewController {
            
            if isAdd {
                
                if rootViewController.view.viewWithTag(Tag.viewBlur) == nil {
                    
                    let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = rootViewController.view.bounds
                    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                    blurEffectView.tag = Tag.viewBlur
                    rootViewController.view.isHidden = false
                    rootViewController.view.addSubview(blurEffectView)
                }
                
            } else {
                
                if let blurEffectView = rootViewController.view.viewWithTag(Tag.viewBlur) {
                    blurEffectView.removeFromSuperview()
                }
            }
        }
    }
    
    func openPasscodeView() {
        
        addRemoveBlurView(isAdd: true)
        
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            
            let reason = Messages.authenticateTitle
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) {
                [unowned self] success, authenticationError in
                
                DispatchQueue.main.async {
                    
                    if success {
                        self.addRemoveBlurView(isAdd: false)
                    } else {
                        APPDELEGATE.showAlert(title: Title.Alert.accessDenied, message: Messages.authenticateMessage, actions: [Title.Alert.OK]) { (index) in
                            self.openPasscodeView()
                        }
                    }
                }
            }
        }
    }
}
