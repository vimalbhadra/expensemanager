//
//  UserDefaultManager.swift
//  ExpenseManager
//
//  Created by Apple Customer on 24/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import Foundation

class UserDefaultManager: NSObject {
    
    class func setData(value: Any?, forKey: String) {
        
        if value == nil {
            UserDefaults.standard.removeObject(forKey: forKey)
        } else {
            UserDefaults.standard.set(value, forKey: forKey)
        }
        
        UserDefaults.standard.synchronize()
    }
    
    class func getData(forKey: String) -> Any? {
        return UserDefaults.standard.object(forKey: forKey)
    }
    
    class func setNotification(_ dictionary: Notification?) {
        
        if dictionary != nil {
            if let encode = try? JSONEncoder().encode(dictionary) {
                UserDefaults.standard.set(encode, forKey: Parameters.UserDefault.Notification)
            }
        } else {
            UserDefaults.standard.removeObject(forKey: Parameters.UserDefault.Notification)
        }
        
        UserDefaults.standard.synchronize()
    }
    
    class func getNotification() -> Notification? {
        
        if let userData = UserDefaults.standard.object(forKey: Parameters.UserDefault.Notification) as? Data {
            if let decode = try? JSONDecoder().decode(Notification.self, from: userData) {
                return decode
            }
        }
        return nil
    }
    
    class func setAccountInfo(_ dictionary: AccountInfo?) {
        
        if dictionary != nil {
            if let encode = try? JSONEncoder().encode(dictionary) {
                UserDefaults.standard.set(encode, forKey: Parameters.UserDefault.AccountInfo)
            }
        } else {
            UserDefaults.standard.removeObject(forKey: Parameters.UserDefault.AccountInfo)
        }
        
        UserDefaults.standard.synchronize()
    }
    
    class func getAccountInfo() -> AccountInfo? {
        
        if let userData = UserDefaults.standard.object(forKey: Parameters.UserDefault.AccountInfo) as? Data {
            if let decode = try? JSONDecoder().decode(AccountInfo.self, from: userData) {
                return decode
            }
        }
        return nil
    }
}
