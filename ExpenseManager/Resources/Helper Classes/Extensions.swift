//
//  Extensions.swift
//  ExpenseManager
//
//  Created by Apple Customer on 12/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
    
    static func random() -> UIColor {
        return UIColor(rgb: Int(CGFloat(arc4random()) / CGFloat(UINT32_MAX) * 0xFFFFFF))
    }
}

extension Date {
    
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
    func getStringFromDate(dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: self)
    }
    
    func getDateFromPreviousNext(previousNext: PreviousNext) -> Date {
        
        switch previousNext {
        case .nextDay:
            return tomorrow
        case .previousDay:
            return yesterday
        case .nextMonth:
            return nextMonth
        case .previousMonth:
            return previousMonth
        }
    }
    
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    
    var nextMonth: Date {
        return Calendar.current.date(byAdding: .month, value: 1, to: noon)!
    }
    
    var previousMonth: Date {
        return Calendar.current.date(byAdding: .month, value: -1, to: noon)!
    }

    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}

extension UIViewController {
    
    public func showInAppWindow() {
        
        if let window = APPDELEGATE.window, let rootViewController = APPDELEGATE.window?.rootViewController {
            window.addSubview(self.view)
            self.view.frame = window.frame
            rootViewController.addChildViewController(self)
        }
    }
}

extension UISearchBar {
    
    open override func awakeFromNib() {
        self.backgroundImage = UIImage()
    }
}
extension UIView {
    
    func setBorderAndCornerRadius(cornerRadius: CGFloat = 5, borderWidth: CGFloat = 1, borderColor: UIColor = .black) {
        
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
    }
}

let UNACCEPTABLE_CHARACTERSAlert = "%&\"<>\\'₹"

extension UnicodeScalar {
    
    var isEmoji: Bool {
        
        switch value {
        case 0x1F600...0x1F64F, // Emoticons
        0x1F300...0x1F5FF, // Misc Symbols and Pictographs
        0x1F680...0x1F6FF, // Transport and Map
        0x1F1E6...0x1F1FF, // Regional country flags
        0x2600...0x26FF,   // Misc symbols
        0x2700...0x27BF,   // Dingbats
        0xFE00...0xFE0F,   // Variation Selectors
        0x1F900...0x1F9FF,  // Supplemental Symbols and Pictographs
        127000...127600, // Various asian characters
        65024...65039, // Variation selector
        9100...9300, // Misc items
        8400...8447: // Combining Diacritical Marks for Symbols
            return true
            
        default: return false
        }
    }
    
    var isZeroWidthJoiner: Bool {
        
        return value == 8205
    }
}

extension String {
    
    func trimmingTrailingSpaces() -> String {
        var t = self
        while t.hasSuffix(" ") {
            t = "" + t.dropLast()
        }
        return t
    }
    
    mutating func trimmedTrailingSpaces() {
        self = self.trimmingTrailingSpaces()
    }
    
    func getDate(dateFormat: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.date(from: self)
    }
    
    //MARK: - IS EMPTY STRING
    func isStringEmpty() -> Bool {
        
        if self.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            return true
        }
        return false
    }
    
    func shortForm() -> String {
        return "\(self.first ?? " ")".capitalized
    }
    
    public var int: Int? {
        return Int(self)
    }
    
    var glyphCount: Int {
        let richText = NSAttributedString(string: self)
        let line = CTLineCreateWithAttributedString(richText)
        return CTLineGetGlyphCount(line)
    }
    
    var isSingleEmoji: Bool {
        return glyphCount == 1 && containsEmoji
    }
    
    var containsEmoji: Bool {
        return unicodeScalars.contains { $0.isEmoji }
    }
    
    var containsOnlyEmoji: Bool {
        return !isEmpty
            && !unicodeScalars.contains(where: {
                !$0.isEmoji
                    && !$0.isZeroWidthJoiner
            })
    }
    
    // The next tricks are mostly to demonstrate how tricky it can be to determine emoji's
    // If anyone has suggestions how to improve this, please let me know
    var emojiString: String {
        
        return emojiScalars.map { String($0) }.reduce("", +)
    }
    
    var emojis: [String] {
        
        var scalars: [[UnicodeScalar]] = []
        var currentScalarSet: [UnicodeScalar] = []
        var previousScalar: UnicodeScalar?
        
        for scalar in emojiScalars {
            
            if let prev = previousScalar, !prev.isZeroWidthJoiner && !scalar.isZeroWidthJoiner {
                
                scalars.append(currentScalarSet)
                currentScalarSet = []
            }
            currentScalarSet.append(scalar)
            
            previousScalar = scalar
        }
        
        scalars.append(currentScalarSet)
        
        return scalars.map { $0.map{ String($0) } .reduce("", +) }
    }
    
    fileprivate var emojiScalars: [UnicodeScalar] {
        
        var chars: [UnicodeScalar] = []
        var previous: UnicodeScalar?
        for cur in unicodeScalars {
            
            if let previous = previous, previous.isZeroWidthJoiner && cur.isEmoji {
                chars.append(previous)
                chars.append(cur)
                
            } else if cur.isEmoji {
                chars.append(cur)
            }
            
            previous = cur
        }
        
        return chars
    }
    
    var isDigit : Bool {
        return Int(self) != nil
    }
}

extension String {
    
    mutating func trim() -> String {
        self = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return self
    }
    
    mutating func trimNewLines() -> String {
        self = self.trimmingCharacters(in: .newlines)
        return self
    }
    
    func getDateFromString(dateFormat: String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.date(from: self)
    }
    
    func getAttibutedString(attrs1Color: UIColor, attrs2Color: UIColor, string1: String, string2: String, font1: UIFont, font2: UIFont) -> NSAttributedString {
        
        let combination = NSMutableAttributedString()
        let attrs1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor :  attrs1Color]
        let attrs2 = [NSAttributedStringKey.font: font2, NSAttributedStringKey.foregroundColor : attrs2Color]
        
        let attributedString1 = NSMutableAttributedString(string:string1, attributes:attrs1)
        let attributedString2 = NSMutableAttributedString(string: string2, attributes:attrs2)
        combination.append(attributedString1)
        combination.append(attributedString2)
        
        return combination
    }
}

func FONT(_ fontName:String , size:CGFloat) -> UIFont {
    
    let uppercaseAttribs = [
        kCTFontFeatureTypeIdentifierKey: kNumberCaseType,
        kCTFontFeatureSelectorIdentifierKey: kUpperCaseNumbersSelector
    ]
    
    let fontAttribs = [
        UIFontDescriptor.AttributeName.name: fontName,
        UIFontDescriptor.AttributeName.featureSettings: [uppercaseAttribs]
        ] as [UIFontDescriptor.AttributeName : Any]
    
    let descriptor = UIFontDescriptor(fontAttributes: fontAttribs)
    return UIFont(descriptor: descriptor, size: size)
}

extension UILabel {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.font = FONT(self.font.fontName, size: self.font.pointSize)
        if self.text != nil {
            self.text =  AppDelegate.sharedInstance.LocalizedString(text: self.text!)
        }
    }
}
