//
//  AboutUSVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 11/04/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

class AboutUSView: UIView {

    @IBOutlet weak var viewTransparentBackground: UIView!
    @IBOutlet weak var viewCenterAbout: UIView!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var btnOK: UIButton!
}

extension AboutUSView {
    
    func showInAppWindow() {
        
        if let window = APPDELEGATE.window {
            
            self.frame = window.frame
            window.addSubview(self)
            initialSetup()
        }
    }
    
    func initialSetup() {
        
        btnOK.setTitle("\(AppDelegate.sharedInstance.LocalizedString(text: "OK"))", for: .normal)
        btnOK.setTitleColor(UIColor(hexString: getSelectedThemeColor()), for: .normal)
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            lblVersion.text = "\(AppDelegate.sharedInstance.LocalizedString(text: "Version"))" + " : " + version
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTransparentView))
        self.viewTransparentBackground.addGestureRecognizer(tap)
        self.showHideAboutView(isShow: true)
    }
    
    @objc func tapOnTransparentView() {
        self.showHideAboutView(isShow: false)
    }
    
    func showHideAboutView(isShow: Bool) {
        
        runOnMainThread {
            self.viewCenterAbout.transform = isShow ? CGAffineTransform(scaleX: 0.001, y: 0.001) : CGAffineTransform.identity
            self.viewCenterAbout.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.viewCenterAbout.transform = isShow ? CGAffineTransform.identity : CGAffineTransform(scaleX: 0.001, y: 0.001)
                self.viewTransparentBackground.alpha = isShow ? 0.5 : 0
            }) { (result) in
                if !isShow {
                    self.removeFromSuperview()
                }
            }
        }
    }
    
    @IBAction func btnOKTapped(_ sender: UIButton) {
        showHideAboutView(isShow: false)
    }
}

