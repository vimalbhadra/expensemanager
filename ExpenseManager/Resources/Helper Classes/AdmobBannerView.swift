//
//  AdmobBannerView.swift
//  ExpenseManager
//
//  Created by Apple Customer on 20/03/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdmobBannerView: UIView {

    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var adsbannerView: GADBannerView!

    var contentView: UIView?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "AdmobBannerView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    
}
