//
//  FilerTypeView.swift
//  ExpenseManager
//
//  Created by Apple Customer on 18/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

struct SelectDatesData {
    var title: String
    var value: String
}

protocol SelectDatesVCDelegate {
    func getDates(dates: (String, String))
}

class SelectDatesVC: UIViewController {

    @IBOutlet weak var viewTransparentBackground: UIView!
    @IBOutlet weak var viewCenterPaymentType: UIView!
    @IBOutlet weak var lblSelectDates: UILabel!
    @IBOutlet weak var tblViewSelectDates: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    var arrDates = [SelectDatesData]() {
        didSet {
            reloadData()
        }
    }
    
    var dates = ("", "")
    var delegate: SelectDatesVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
}

extension SelectDatesVC {
    
    func initialSetup() {
        
        lblSelectDates.backgroundColor = UIColor(hexString: getSelectedThemeColor())
        btnCancel.setTitleColor(UIColor(hexString: getSelectedThemeColor()), for: .normal)
        btnSave.setTitleColor(UIColor(hexString: getSelectedThemeColor()), for: .normal)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTransparentView))
        self.viewTransparentBackground.addGestureRecognizer(tap)
        
        arrDates = [
            SelectDatesData(title: "Start Date", value: dates.0),
            SelectDatesData(title: "End Date", value: dates.1)
        ]
        
        self.showHideDatesView(isShow: true)
    }
    
    @objc func tapOnTransparentView() {
        self.showHideDatesView(isShow: false)
    }
    
    func reloadData() {
        runOnMainThread {
            self.tblViewSelectDates.reloadData()
        }
    }
    
    func showHideDatesView(isShow: Bool) {
        
        runOnMainThread {
            self.viewCenterPaymentType.transform = isShow ? CGAffineTransform(scaleX: 0.001, y: 0.001) : CGAffineTransform.identity
            self.viewCenterPaymentType.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.viewCenterPaymentType.transform = isShow ? CGAffineTransform.identity : CGAffineTransform(scaleX: 0.001, y: 0.001)
                self.viewTransparentBackground.alpha = isShow ? 0.5 : 0
            }) { (result) in
                if !isShow {
                    self.view.removeFromSuperview()
                }
            }
        }
    }
}

extension SelectDatesVC {
    
    @IBAction func btnCancleTapped(_ sender: UIBarButtonItem) {
        showHideDatesView(isShow: false)
    }
    
    @IBAction func btnSaveTapped(_ sender: UIBarButtonItem) {
        if delegate != nil {
             delegate?.getDates(dates: dates)
        }
        showHideDatesView(isShow: false)
    }
}

extension SelectDatesVC: DatePickerViewDelegate {
    
    func getSelectedDate(date: Date) {
        
    }
    
    func getDates(dates: (Date, Date, Bool)) {
        
        if dates.2 {
            self.dates.0 = dates.0.getStringFromDate(dateFormat: DateFormat.ddmmyyyy)
            arrDates[0].value = dates.0.getStringFromDate(dateFormat: DateFormat.ddmmyyyy)
        } else {
            self.dates.1 = dates.1.getStringFromDate(dateFormat: DateFormat.ddmmyyyy)
            arrDates[1].value = dates.1.getStringFromDate(dateFormat: DateFormat.ddmmyyyy)
        }
    }
}

extension SelectDatesVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.SelectDatesTVC) as! SelectDatesTVC
        
        let object = arrDates[indexPath.row]
        
        cell.lblTitle.text = object.title
        cell.lblValue.text = object.value
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let datePickerView = DatePickerView.instanceFromNib()
        datePickerView.delegate = self
        datePickerView.dates = (dates.0.getDateFromString(dateFormat: DateFormat.ddmmyyyy)!, dates.1.getDateFromString(dateFormat: DateFormat.ddmmyyyy)!, (indexPath.row == 0) ? true : false)
        datePickerView.showInAppWindow()
    }
}
