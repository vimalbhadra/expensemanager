//
//  DatePickerView.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 14/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import StoreKit

class ProVersionView: UIView {
    
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    
    
    @IBOutlet weak var viewCenter: UIView!
    @IBOutlet weak var viewTransparent: UIView!
    @IBOutlet weak var viewTopBackground: UIView!
    @IBOutlet weak var lblUpgradeToPro: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var viewAmount: UIView!
    @IBOutlet weak var lblAdvertisement: UILabel!
    @IBOutlet weak var btnUpgrade: UIButton!
    @IBOutlet weak var btnRestore: UIButton!

    @IBOutlet weak var const_viewTopBackground_top: NSLayoutConstraint!
    
    class func instanceFromNib() -> ProVersionView {
        let views = UINib(nibName: "ProVersionView", bundle: nil).instantiate(withOwner: nil, options: nil)
        return views[0] as! ProVersionView
    }
}

extension ProVersionView {
    
    func showInAppWindow() {
        
        if let window = APPDELEGATE.window {
            
            self.frame = window.frame
            window.addSubview(self)
            
            if !isSimulator {
                fetchAvailableProducts()
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTransparentView))
            self.viewTransparent.addGestureRecognizer(tap)
            
            self.layoutIfNeeded()
            viewTopBackground.layer.cornerRadius = viewTopBackground.frame.height / 2
            const_viewTopBackground_top.constant = -(viewTopBackground.frame.height - 65)
            if let themeColor = UserDefaultManager.getData(forKey: Parameters.UserDefault.ThemeColor) as? String {
                let bgColor = UIColor(hexString: themeColor)
                viewTopBackground.backgroundColor = bgColor
                lblUpgradeToPro.textColor = bgColor
                viewAmount.setBorderAndCornerRadius(borderColor: bgColor)
                btnUpgrade.backgroundColor = bgColor
                btnRestore.backgroundColor = bgColor

                lblAmount.attributedText = "".getAttibutedString(attrs1Color: bgColor, attrs2Color: UIColor.black, string1: "$ 2.99", string2: "\nfor lifetime", font1: FONT(FontName.roboto_medium.rawValue, size: FontSize.subTitle), font2: FONT(FontName.roboto_regular.rawValue, size: FontSize.small))
            }
            
            showHideProVersionView(isShow: true)
        }
    }
    
    @objc func tapOnTransparentView() {
        showHideProVersionView(isShow: false)
    }
    
    func showHideProVersionView(isShow: Bool) {
        
        runOnMainThread {
            self.viewCenter.transform = isShow ? CGAffineTransform(scaleX: 0.001, y: 0.001) : CGAffineTransform.identity
            self.viewCenter.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.viewCenter.transform = isShow ? CGAffineTransform.identity : CGAffineTransform(scaleX: 0.001, y: 0.001)
                self.viewTransparent.alpha = isShow ? 0.5 : 0
            }) { (result) in
                if !isShow {
                    self.removeFromSuperview()
                }
            }
        }
    }
}

extension ProVersionView {
    
    // MARK: - FETCH AVAILABLE IAP PRODUCTS
    func fetchAvailableProducts()  {
        
        let productIdentifiers = NSSet(objects: IPA_REMOVEAD)
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers as! Set<String>)
        productsRequest.delegate = self
        productsRequest.start()
    }
    // MARK:  RESTORE ACTION
    @objc func restorePurchase() {
        APPDELEGATE.showAppLoader()
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    @IBAction func btnUpgradeTapped(_ sender: UIButton) {
        if self.iapProducts.count > 0{
            self.purchaseMyProduct(product: self.iapProducts[0])
        }else{
            self.fetchAvailableProducts()
        }
    }
    @IBAction func btnRestoreTapped(_ sender: UIButton) {
        APPDELEGATE.showAppLoader()
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}
extension ProVersionView : SKProductsRequestDelegate, SKPaymentTransactionObserver{
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        APPDELEGATE.removeLoader()
        APPDELEGATE.showAlert(title: nil, message: "Failed to restored your purchase, Please check Apple Id you have used to purchase this app before.", actions: [Title.Alert.OK], completionHandler: nil)
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        APPDELEGATE.removeLoader()
        kdefaults.set(false, forKey: kShowAds)
        APPDELEGATE.showAlert(title: nil, message: "You've successfully restored your purchase!", actions: [Title.Alert.OK], completionHandler: nil)

    }
    
    func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        APPDELEGATE.removeLoader()
        if (response.products.count > 0) {
            iapProducts = response.products
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        APPDELEGATE.removeLoader()
         APPDELEGATE.showAlert(title: nil, message: "Unable to connect with In-App Store at this moment", actions: [Title.Alert.OK], completionHandler: nil)
        
    }
    
    // MARK: - MAKE PURCHASE OF A PRODUCT
    func canMakePurchases() -> Bool {  return SKPaymentQueue.canMakePayments()  }
    
    func purchaseMyProduct(product: SKProduct) {
        APPDELEGATE.showAppLoader()
        if self.canMakePurchases() {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            print("PRODUCT TO PURCHASE: \(product.productIdentifier)")
        } else {
            APPDELEGATE.removeLoader()
             APPDELEGATE.showAlert(title: nil, message: "Purchases are disabled in your device!", actions: [Title.Alert.OK], completionHandler: nil)
            
        }
    }
    
    // MARK: IN APP PAYMENT QUEUE
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                case .purchased:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    kdefaults.set(false, forKey: kShowAds)
                    APPDELEGATE.showAlert(title: nil, message: "Ads Removed, Wish your loved onces without interferance of Ads", actions: [Title.Alert.OK], completionHandler: nil)
                    APPDELEGATE.removeLoader()
                    showHideProVersionView(isShow: false)


                    break
                case .failed:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                     APPDELEGATE.showAlert(title: nil, message: "Error, failed to remove Ads", actions: [Title.Alert.OK], completionHandler: nil)
                  
                    APPDELEGATE.removeLoader()
                    break
                case .restored:
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    showHideProVersionView(isShow: false)
                    APPDELEGATE.removeLoader()
                    break
                default: break
                }
            }
        }
    }

}
