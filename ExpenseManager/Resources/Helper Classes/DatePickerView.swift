//
//  DatePickerView.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 14/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

protocol DatePickerViewDelegate {
    func getSelectedDate(date: Date)
    func getDates(dates: (Date, Date, Bool))
}

class DatePickerView: UIView {
    
    @IBOutlet weak var datePickerSuperView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var const_datePickerSuperView_bottom: NSLayoutConstraint!
    @IBOutlet weak var viewTransparent: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnDone: UIButton!

    var delegate: DatePickerViewDelegate?
    var selectedDate: Date!
    var dates: (Date, Date, Bool)? = nil
    var datePickerMode: UIDatePickerMode = .date
    
    class func instanceFromNib() -> DatePickerView {
        let views = UINib(nibName: "DatePickerView", bundle: nil).instantiate(withOwner: nil, options: nil)
        return views[0] as! DatePickerView
    }
}

extension DatePickerView {
    
    func showInAppWindow() {
        
        if let window = APPDELEGATE.window {
            
            self.frame = window.frame
            window.addSubview(self)
            
            if dates != nil {
                if (dates?.2)! {
                    datePicker.date = (dates?.0)!
                    datePicker.maximumDate = (dates?.1)!
                } else {
                    datePicker.minimumDate = dates?.0
                    datePicker.date = (dates?.1)!
                }
            } else {
                datePicker.date = selectedDate
            }
            
            datePicker.datePickerMode = datePickerMode
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTransparentView))
            self.viewTransparent.addGestureRecognizer(tap)
            
            showHideDatePickerView(isShow: true)
        }
    }
    
    @objc func tapOnTransparentView() {
        showHideDatePickerView(isShow: false)
    }
    
    func showHideDatePickerView(isShow: Bool) {
        
        btnDone.setTitle(AppDelegate.sharedInstance.LocalizedString(text: "Done"), for: .normal)
        btnCancel.setTitle(AppDelegate.sharedInstance.LocalizedString(text: "Cancel"), for: .normal)
        
        runOnMainThread {
            self.const_datePickerSuperView_bottom.constant = isShow ? 0 : -(self.datePickerSuperView.frame.height)
            UIView.animate(withDuration: 0.3, animations: {
                self.layoutIfNeeded()
                self.viewTransparent.alpha = isShow ? 0.5 : 0
            }) { (result) in
                if !isShow {
                    self.removeFromSuperview()
                }
            }
        }
    }
}

extension DatePickerView {
    
    @IBAction func btnCancleTapped(_ sender: UIButton) {
        showHideDatePickerView(isShow: false)
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        
        if delegate != nil {
            if dates != nil {
                if (dates?.2)! {
                    dates?.0 = datePicker.date
                } else {
                    dates?.1 = datePicker.date
                }
                delegate?.getDates(dates: dates!)
            } else {
                delegate?.getSelectedDate(date: datePicker.date)
            }
        }
        showHideDatePickerView(isShow: false)
    }
}
