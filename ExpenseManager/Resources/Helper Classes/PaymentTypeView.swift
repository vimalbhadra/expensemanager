//
//  FilerTypeView.swift
//  ExpenseManager
//
//  Created by Apple Customer on 18/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

struct PaymentType {
    
    var titleLeft: String
    var isCheckedLeft: Bool
    var titleRight: String
    var isCheckedRight: Bool
    var isAll: Bool
    
    init(titleLeft: String, titleRight: String, isCheckedLeft: Bool = false, isCheckedRight: Bool = false, isAll: Bool = false) {
        self.titleLeft = titleLeft
        self.isCheckedLeft = isCheckedLeft
        self.titleRight = titleRight
        self.isCheckedRight = isCheckedRight
        self.isAll = isAll
    }
}

class PaymentTypeView: UIView {

    @IBOutlet weak var viewTransparentBackground: UIView!
    @IBOutlet weak var viewCenterPaymentType: UIView!
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var tblViewPaymentType: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!

    var arrPaymentType = [
        PaymentType(titleLeft: "All", titleRight: "CASH", isCheckedLeft: true, isCheckedRight: false, isAll: true),
        PaymentType(titleLeft: "CHEQUE", titleRight: "BANK TRANSFER"),
        PaymentType(titleLeft: "CREDIT CARD", titleRight: "DEBIT CARD")
    ]
    
    class func instanceFromNib() -> PaymentTypeView {
        let views = UINib(nibName: "PaymentTypeView", bundle: nil).instantiate(withOwner: nil, options: nil)
        return views[0] as! PaymentTypeView
    }
}

extension PaymentTypeView {
    
    
}

extension PaymentTypeView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPaymentType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.DrawerMenuTVC) as! PaymentTypeTVC
        
        let object = arrPaymentType[indexPath.row]
        cell.btnLeft.setTitle(object.titleLeft, for: .normal)
        cell.btnRight.setTitle(object.titleLeft, for: .normal)
        cell.btnLeft.isSelected = object.isCheckedLeft
        cell.btnRight.isSelected = object.isCheckedRight
        return cell
    }
}
