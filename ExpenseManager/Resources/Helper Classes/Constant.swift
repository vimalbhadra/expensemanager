//
//  Constant.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 11/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import Foundation
import UIKit

let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
public let IPA_REMOVEAD = "RemoveAds001"

let kdefaults = UserDefaults.standard
var klaunchedBefore = "launchedBefore"
var kshowpopupRecurrent = "kshowpopupRecurrent"
var kcurrencyINFO = "currencyinfo"
var kLanguagecode = "LNGCODE"
var kLanguage = "LANGUAGE"
var kShowAds = "ShowAds"
var admobBannerID = "ca-app-pub-6468416250061667/9867070717"
var admobInterstitialID = "ca-app-pub-6468416250061667/9483927336"
var GOOGLE_AD_ID = "ca-app-pub-6468416250061667~5189459100"
var kShareText = "Expense Manager,Easily manage Income and Expense! Download URL : \(kAppUrl)"
var kAppUrl = "https://itunes.apple.com/us/app/gsw-expense-manager/id1457992735?ls=1&mt=8"
var kPrivacyUrl = "https://docs.google.com/document/d/e/2PACX-1vSBaAzihYF5BswbgWrxYgmwMfrZQayEdfNbV6TLPcEmgbX-cDgwgZIXBSQdTBeL5GSHXH7hjyvF_4EW/pub"
var kAppID = "id1457992735"
//MARK: - IS SIMULATOR
public let isSimulator: Bool = {
    var isSim = false
    #if arch(i386) || arch(x86_64)
    isSim = true
    #endif
    return isSim
}()

struct ConstantValues {
    
    static let ZIP_FOLDERNAME = "Backup"
    static let DATABASE_NAME = "expense_manager_Database.sqlite"
    static let DATABASE_NAME_TXT = "expense_manager_Database_Backup.txt"
    static let APP_VERSION = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? "1.0"
    static let date = Date().getStringFromDate(dateFormat: DateFormat.dateMonthYearHHMMSS)
    static let ZIP_FILENAME = "Expense_Manager_\(APP_VERSION)_\(date)"
    static let BACKUP_FOLDERNAME = "/Apps/Expense Manager - Manage Income & Expense/"
    static let DROPBOX_APP_KEY = "cfuzgmd24vlj59k"
    static let zipExtension = ".zip"
    static let DEFAULT_THEME_COLOR = "#0099A9"
    static let DEFAULT_NOTIFICATION_TIME = "8:00 PM"
    static let TRANSACTIONS = "Transactions"
    static let shortIncome = "In"
    static let shortExpense = "Ex"
}

struct Identifier {
    
    struct Storyboard {
        static let Main = "Main"
        static let Setting = "Setting"
        static let SB_Category = "SB_Category"
        static let SB_AddEditTransaction = "SB_AddEditTransaction"
        static let SB_RecurrentTransaction = "SB_RecurrentTransaction"
        static let SB_Charts = "SB_Charts"
        static let SB_Currency = "SB_Currency"

    }
    
    struct ViewController {
        
        static let SideMenuNavigationController = "UISideMenuNavigationController"
        static let DrawerMenuVC = "DrawerMenuVC"
        static let AnalyticsVC = "AnalyticsVC"
        static let AddAccountView = "AddAccountView"
        static let ProVersionView = "ProVersionView"
        static let ManageAccountVC = "ManageAccountVC"
        static let BackupVC = "BackupVC"
        static let AddEditTransactionVC = "AddEditTransactionVC"
        static let ExpenseManagerVC = "ExpenseManagerVC"
        static let CategoryManagmentVC = "CategoryManagmentVC"
        static let AddCategoryVC = "AddCategoryVC"
        static let CategoryIconVC = "CategoryIconVC"
        static let RecurrentTransactionListVC = "RecurrentTransactionListVC"
        static let RecurrentEditTransactionVC = "RecurrentEditTransactionVC"
        static let ChartsVC = "ChartsVC"
        static let PaymentTypeVC = "PaymentTypeVC"
        static let CurrencyVC = "CurrencyVC"
        static let SettingsVC = "SettingsVC"
        static let SelectDatesVC = "SelectDatesVC"
        static let ThemeColorVC = "ThemeColorVC"
        static let SelectLanguageVC = "SelectLanguageVC"
        static let DatePickerView = "DatePickerView"
        static let PrivacyVC = "PrivacyVC"

    }
    
    struct Cell {
        static let DrawerMenuTVC = "DrawerMenuTVC"
        static let SectionHeaderDrawerMenu = "SectionHeaderDrawerMenu"
        static let AnalyticsCVC = "AnalyticsCVC"
        static let AnalyticsSectionTVC = "AnalyticsSectionTVC"
        static let AnalyticsTVC = "AnalyticsTVC"
        static let PaymentTypeTVC = "PaymentTypeTVC"
        static let ManageAccountTVC = "ManageAccountTVC"
        static let AddEditTVC = "AddEditTVC"
        static let AddEditAmountTVC = "AddEditAmountTVC"
        static let ExpenseManagerTVC = "ExpenseManagerTVC"
        static let BackupTVC = "BackupTVC"
        static let CategoryTVC = "CategoryTVC"
        static let CategoryIconCVC = "CategoryIconCVC"
        static let CurrencyTVC = "CurrencyTVC"
        static let SelectDatesTVC = "SelectDatesTVC"
        static let SettingsTVC = "SettingsTVC"
        static let ThemeColorCVC = "ThemeColorCVC"
    }
}

struct ImageName {

    struct Navigation {
        static let arrow_back = "arrow_back"
        static let navcorrect = "nav_iconwhite_true"
        static let navmenu = "nav_menu"
        static let navdelete = "nav_icon_delete"
    }
    
    struct Analytics {
        static let active_mode = "active_mode"
        static let altitude_dashboard = "altitude_dashboard"
        static let arrow_mode = "arrow_mode"
    }
    
    struct DrawerMenu {
        
        static let ic_accounts = "ic_accounts"
        static let ic_charts = "ic_charts"
        static let ic_recurrent = "ic_recurrent"
        static let ic_analytics = "ic_analytics"
        static let ic_backup = "ic_backup"
        static let ic_category = "ic_category"
        static let ic_currency = "ic_currency"
        static let ic_settings_drawer = "ic_settings_drawer"
        static let ic_share = "ic_share"
        static let ic_more = "ic_more"
        static let ic_pro = "ic_pro"
        static let ic_email = "ic_email"
        static let ic_add_drawer = "ic_add_drawer"
        static let ic_edit = "ic_edit"



        static let arrow_mode = "arrow_mode"
        static let course_dashboard = "course_dashboard"
        static let custom_mode = "custom_mode"
        static let debug = "debug"
        static let design_down_mode = "design_down_mode"
        static let edit_dashboard = "edit_dashboard"
        static let gps_dashboard = "gps_dashboard"
        static let inactive_dashboard = "inactive_dashboard"
        static let lite_mode = "lite_mode"
        static let position_dashboard = "position_dashboard"
    }
}

//------------------------------------------------

// MARK:- Structure: Title

//------------------------------------------------

struct Title {
    
    static let Analytics = "Analytics"
    static let userMode = "User Mode"
    static let infoSelection = "Latest Data"
    static let Settings = "Settings"
    static let debugMode = "Debug Mode"
    static let Accounts = "Accounts"
    static let Backup = "Backup & Restore"
    static let AddTransaction = "Add Transaction"
    static let EditTransaction = "Edit Transaction"
    static let ExpenseManager = "Expense Manager"
    static let SelectCategory = "Select Category"
    static let ManageCategory = "Manage Category"
    static let editAccounts = "Edit Account"
    static let AddCategory = "Add Category"
    static let EditCategory = "Edit Category"
    static let RecurrentTransaction = "Recurrent Transactions"
    static let Charts = "Charts"
    static let SelectCurrency = "Select Currency"
    static let SelectLanguage = "Select language"
    static let PrivacyPolicy = "Privacy Policy"

    
    struct Alert {
        static let OK = "OK"
        static let CANCEL = "CANCEL"
        static let deleteAccount = "Delete account"
        static let DELETE = "DELETE"
        static let Delete = "Delete"
        static let Alert = "Alert"
        static let Dontshowagain = "DON'T SHOW AGAIN"
        static let accessDenied = "Access Denied"
        static let Done = "Done"

    }
}

struct Color {
    
    struct Navigation {
        static let navigationTitle = UIColor.white
        static let navigationBG = UIColor(rgb: 0x00A9BE)
    }
    struct textColor {
        static let textcolorDark = UIColor(hexString: "212121").cgColor
        static let textColorSemiDark = UIColor(hexString: "454545").cgColor
        static let textColorGrey = UIColor(hexString: "747474").cgColor
        static let textColorLightGrey = UIColor(hexString: "A2A2A2").cgColor
        static let textColorWhiteLight = UIColor(hexString: "747474").cgColor
        static let textColorWhite = UIColor(hexString: "FFFFFF").cgColor
        static let textColorOrange = UIColor(hexString: "FF9800").cgColor
        static let colorIncome = UIColor(hexString: "6CAD6F")
        static let colorExpense = UIColor(hexString: "E46565")

    }
    
    struct Dashboard {
        static let latestData = [UIColor(hexString: "6683f0").cgColor, UIColor(hexString: "3977d5").cgColor]
        static let trackerOn = [UIColor(hexString: "3cce4d").cgColor, UIColor(hexString: "15b236").cgColor]
        static let trackerOff = [UIColor(hexString: "ce3c3c").cgColor, UIColor(hexString: "b23e15").cgColor]
    }
}

public let isiPhoneX : Bool = {
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 2436, 2688, 1792:
            return true
        default:
            return false
        }
    }
    return false
}()

struct DateFormat {
    static let dayDateMonthYear = "EEE, dd MMM yyyy"
    static let MonthYear = "MMMM yyyy"
    static let dateMonthYearHHMMSS = "yyyy_MM_dd_HHmmss"
    static let yearmonthday = "yyyy-MM-dd"
    static let yearmonth = "yyyy-MM"
    static let year = "yyyy"
    static let month = "MMMM"
    static let daymonth = "dd MMM"
    static let monthmm = "MM"
    static let ddmmyyyy = "dd-MM-yyyy"
    static let monthyear = "MM-yyyy"
    static let monthNameyear = "MMM yyyy"
    static let day = "dd"
    static let monthnumer = "MM"
    static let hoursMinutes = "hh:mm a"
}

struct Tag {
    static let toastMessage = 3001
    static let viewBlur = 3002
}

enum FontName: String {
    case roboto_medium = "Roboto-Medium"
    case roboto_light = "Roboto-Light"
    case roboto_bold = "Roboto-Bold"
    case roboto_regular = "Roboto-Regular"
}

struct FontSize {
    static let navigationTitle: CGFloat = 20
    static let title: CGFloat = 17
    static let subTitle: CGFloat = 15
    static let small: CGFloat = 12
}

struct TableName {
    
    struct Account_Table {
        
        static let name = "Account_Table"
        static let Account_Id = "Account_Id"
        static let Account_Name = "Account_Name"
        
    }
    struct Category_Table {
        
        static let name = "Category_Table"
        static let Category_Id = "Category_Id"
        static let Category_Name = "Category_Name"
        static let Category_Icon_Name = "Category_Icon_Name"
        static let Category_Count = "Category_Count"
        static let Category_Account_Id = "Category_Account_Id"
        
    }
    struct Transaction_Table {
        
        static let name = "Transaction_Table"
        static let Transaction_Id = "Transaction_Id"
        static let Transaction_Type = "Transaction_Type"//income-expense
        static let Transaction_Amount = "Transaction_Amount"
        static let Transaction_Date = "Transaction_Date"
        static let Transaction_Cat_Id = "Transaction_Cat_Id"
        static let Transaction_PayType = "Transaction_PayType"//cash-bank etc
        static let Transaction_Account_Id = "Transaction_Account_Id"
        static let Transaction_Note = "Transaction_Note"

    }
    struct Recurrent_Table {
        
        static let name = "Recurrent_Table"
        static let Recurrent_Column_Id = "Recurrent_Column_Id"
        static let Recurrent_Transaction_Type = "Recurrent_Transaction_Type"//income-expense
        static let Recurrent_Amount = "Recurrent_Amount"
        static let Recurrent_Next_Date = "Recurrent_Next_Date"
        static let Recurrent_Cat_Id = "Recurrent_Cat_Id"
        static let Recurrent_PayType = "Recurrent_PayType"//cash-bank etc
        static let Recurrent_Type = "Recurrent_Type"//DAILY,WEEKLY
        static let Recurrent_Account_Id = "Recurrent_Account_Id"
        static let Recurrent_Note = "Recurrent_Note"
        
    }
}

struct Parameters {
    
    struct UserDefault {
        static let AccountInfo = "AccountInfo"
        static let ThemeColor = "ThemeColor"
        static let Notification = "Notification"
        static let TouchID = "TouchID"
    }
}

//MARK: -  GET VC FOR NAVIGATION
public func getStoryboard(_ storyboardName: String) -> UIStoryboard {
    return UIStoryboard(name: "\(storyboardName)", bundle: nil)
}

public func loadVC(_ strStoryboardId: String, strVCId: String) -> UIViewController {
    return getStoryboard(strStoryboardId).instantiateViewController(withIdentifier: strVCId)
}

public func instanceFromNib(nibName: String) -> UIView {
    let views = UINib(nibName: nibName, bundle: nil).instantiate(withOwner: nil, options: nil)
    return views[0] as! UIView
}

public func getSelectedThemeColor()->String{
     let themeColor = UserDefaultManager.getData(forKey: Parameters.UserDefault.ThemeColor) as? String
    return themeColor!
}


// MARK: - Get full screen size
public func fixedScreenSize() -> CGSize {
    let screenSize = UIScreen.main.bounds.size
    if NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1 &&
        UIDevice.current.orientation.isLandscape {
        return CGSize(width: screenSize.height, height: screenSize.width)
    }
    return screenSize
}

public func SCREENWIDTH() -> CGFloat{
    return fixedScreenSize().width
}

public func SCREENHEIGHT() -> CGFloat{
    return fixedScreenSize().height
}

/**
 Get image from image name
 */
//MARK: - Get image from image name
public func Set_Local_Image(_ imageName :String) -> UIImage{
    return UIImage(named:imageName)!
}

/**Log trace*/
//MARK: - Log trace
public func DLog<T>(_ message:T,  file: String = #file, function: String = #function, lineNumber: Int = #line ) {
    #if DEBUG
    if let text = message as? String {
        print("\((file as NSString).lastPathComponent) -> \(function) line: \(lineNumber): \(text)")
    }
    #endif
}

struct Messages {
    
    static let noInternet = "Internet connection appears to be offline."
    static let noIncomeFound = "  No income transactions found  "
    static let noExpenseFound = "  No expense transactions found  "
    static let incomeFound = "income transaction(s) found"
    static let expenseFound = "expense transaction(s) found"
    static let tryAgain = "OOPS! Something went wrong. Please try again!"
    static let accountNameBlank = "Account name can not be blank"
    static let accountExist = "Account already exists"
    static let notDeleteAccount = "You can not delete this account."
    static let deleteAccountPermission = "Are you sure you want to delete this account, all of your transactions with this account will be deleted?"
    static let deleteTransactionPermission = "Are you sure you want to delete this transaction?"
    static let noDropboxAccount = "You haven't yet linked your Dropbox. Kindly link it first to take backup"
    static let notBackupFiles = "No backup files found"
    static let unlinkDropboxAccount = "Expense Manager has been successfully unlinked from your Dropbox."
    static let backupUnsuccessful = "Unable to take backup, Please try again."
    static let backupDeleted = "Backup successfully deleted."
    static let backupRestored = "Backup successfully restored."
    static let backupUploaded = "Backup file uploaded successfully to Dropbox."
    static let backupDeletedPermission = "Are you sure you want to delete this backup file?"
    static let backupDeletedFail = "Failed to delete backup, Please try again."
    static let backupRestoredFail = "Failed to restore backup, Please try again."
    static let backupRestorePermission = "This backup process will replace all of your current records."
    
    static let authenticateTitle = "Authenticate with Touch ID"
    static let authenticateMessage = "You must have to authenticate with Touch ID"

    //category
    static let categoryNameBlank = "Category name can not be blank"
    static let categoryiconsel = "Please select Category Icon"
    static let categoryExist = "category already exists"
    
    //Transacation
    static let enteramount = "Transaction amount can not be less than zero"
    static let selectcategory = "Please select category"
    static let TransactionSuccess = "Transaction successfully saved"
    static let TransactionUpdate = "Transaction successfully updated"
    static let TransactionDelete = "Transaction successfully deleted"
    static let repeatTransactionPermission = "Are you sure you want to add auto repeat this transaction?"
    static let noTransaction = "No transactions found in this day"
    static let selectpaymenttype = "Select Payment Type"
    static let selectrecurrenttype = "Select Recurrent Type"
    static let selectaccount = "Select Account"
    static let INCOME = "INCOME"
    static let EXPENSE = "EXPENSE"
    static let BALANCE = "BALANCE"

}

struct catogoryColor{
    
    static let icn_category_bank = "3BFFEB"
    static let icn_category_baseball = "EF5350"
    static let icn_category_basketball = "C62828"
    static let icn_category_bed = "FF1744"
    static let icn_category_beer = "C51162"
    static let icn_category_bicycle = "FF4081"
    static let icn_category_boat = "D81B60"
    static let icn_category_book = "BA68C8"
    static let icn_category_bus = "8E24AA"
    static let icn_category_cake1 = "E040FB"
    static let icn_category_cake2 = "7E57C2"
    static let icn_category_call_center = "5E35B1"
    static let icn_category_car = "29B6F6"
    static let icn_category_cloths = "304FFE"
    static let icn_category_computer = "EB3BFF"
    static let icn_category_cricket = "F06292"
    static let icn_category_dress = "1E88E5"
    static let icn_category_drinks = "FF9800"
    static let icn_category_dumbell = "448AFF"
    static let icn_category_education = "01579B"
    static let icn_category_factory = "00B0FF"
    static let icn_category_flight = "0097A7"
    static let icn_category_food = "006064"
    static let icn_category_football = "00E5FF"
    static let icn_category_fuel = "00BFA5"
    static let icn_category_fun = "009688"
    static let icn_category_gift = "4DB6AC"
    static let icn_category_glass = "00796B"
    static let icn_category_headphone = "00695C"
    static let icn_category_health = "00C853"
    static let icn_category_home = "43A047"
    static let icn_category_hotel = "1B5E20"
    static let icn_category_label = "64DD17"
    static let icn_category_laptop = "B1FF3B"
    static let icn_category_mechanic = "558B2F"
    static let icn_category_mechanic_tools = "33691E"
    static let icn_category_merchandise = "827717"
    static let icn_category_personal = "C0CA33"
    static let icn_category_pets = "69F0AE"
    static let icn_category_pizza = "FF6E40"
    static let icn_category_plumber = "546E7A"
    static let icn_category_question = "FDD835"
    static let icn_category_restaurent = "795548"
    static let icn_category_restaurent2 = "A1887F"
    static let icn_category_road = "424242"
    static let icn_category_rugby = "757575"
    static let icn_category_sandle = "DD2C00"
    static let icn_category_school = "FF3D00"
    static let icn_category_scooter = "BF360C"
    static let icn_category_shoes = "E64A19"
    static let icn_category_taxi = "F57F17"
    static let icn_category_telephone = "FFD600"
    static let icn_category_tickets = "FBC02D"
    static let icn_category_tip = "0D47A1"
    static let icn_category_train = "E65100"
    static let icn_category_transport = "FFC107"
    static let icn_category_tshirt = "00E676"


}
