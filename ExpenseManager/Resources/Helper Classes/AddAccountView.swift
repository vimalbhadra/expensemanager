//
//  DatePickerView.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 14/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

protocol AddAccountViewDelegate {
    func refreshAccountData()
    func accountUpdated(_ accountInfo: AccountInfo)
}

class AddAccountView: UIView {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewCenter: UIView!
    @IBOutlet weak var viewTransparentBG: UIView!
    @IBOutlet weak var txtAccountName: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!

    var delegate: AddAccountViewDelegate?
    var accountInfo: AccountInfo? = nil
    
    class func instanceFromNib() -> AddAccountView {
        let views = UINib(nibName: "AddAccountView", bundle: nil).instantiate(withOwner: nil, options: nil)
        return views[0] as! AddAccountView
    }
}

extension AddAccountView {
    
    func showInAppWindow() {
        
        lblTitle.backgroundColor = UIColor(hexString: getSelectedThemeColor())
        btnCancel.setTitleColor(UIColor(hexString: getSelectedThemeColor()), for: .normal)
        btnSave.setTitleColor(UIColor(hexString: getSelectedThemeColor()), for: .normal)
        btnSave.setTitle(AppDelegate.sharedInstance.LocalizedString(text: "Save"), for: .normal)
        btnCancel.setTitle(AppDelegate.sharedInstance.LocalizedString(text: "Cancel"), for: .normal)

        if let window = APPDELEGATE.window {
            window.addSubview(self)
            self.frame = window.frame
            
            if self.accountInfo != nil {
                self.txtAccountName.text = self.accountInfo?.name
                self.lblTitle.text = Title.editAccounts
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTransparentView))
            self.viewTransparentBG.addGestureRecognizer(tap)
            
            self.showHideAddAccountView(isShow: true)
        }
    }
    
    @objc func tapOnTransparentView() {
        self.showHideAddAccountView(isShow: false)
    }
    
    func showHideAddAccountView(isShow: Bool) {
        
        runOnMainThread {
            self.viewCenter.transform = isShow ? CGAffineTransform(scaleX: 0.001, y: 0.001) : CGAffineTransform.identity
            self.viewCenter.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.viewCenter.transform = isShow ? CGAffineTransform.identity : CGAffineTransform(scaleX: 0.001, y: 0.001)
                self.viewTransparentBG.alpha = isShow ? 0.5 : 0
            }) { (result) in
                if !isShow {
                    self.removeFromSuperview()
                }
            }
        }
    }
}

extension AddAccountView {
    
    @IBAction func btnCancleTapped(_ sender: UIBarButtonItem) {
        showHideAddAccountView(isShow: false)
    }
    
    @IBAction func btnDoneTapped(_ sender: UIBarButtonItem) {
        
        if var text = txtAccountName.text, text.trim().count > 0 {
            
            if self.accountInfo != nil {
                self.accountInfo?.name = text
                if AccountInfo.updateAccount(accountInfo: self.accountInfo!) {
                    if delegate != nil {
                        delegate?.accountUpdated(self.accountInfo!)
                    }
                    showHideAddAccountView(isShow: false)
                }
            } else {
                var accountInfo = AccountInfo()
                accountInfo.name = text
                if AccountInfo.addAccount(accountInfo: accountInfo) {
                    if delegate != nil {
                        delegate?.refreshAccountData()
                    }
                     var temparrAccounts = [AccountInfo]()
                     temparrAccounts = AccountInfo.getAllAccount()
                    
                    AppDelegate.sharedInstance.addDefaultCategory(accountId: (temparrAccounts.last?.id)!)
                    showHideAddAccountView(isShow: false)
                }
            }
            
        } else {
            APPDELEGATE.showToastMessage(Messages.accountNameBlank, wantBottomSide: true, hideAutomatically: true)
        }
    }
}
