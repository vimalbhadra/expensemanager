//
//  PrivacyVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 11/04/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import WebKit
class PrivacyVC: BaseVC {

    var webView : WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [""], right_action: [], title: AppDelegate.sharedInstance.LocalizedString(text: Title.PrivacyPolicy))

        
        
        let myBlog = kPrivacyUrl
        let url = NSURL(string: myBlog)
        let request = NSURLRequest(url: url! as URL)
        
        // init and load request in webview.
        webView = WKWebView(frame: self.view.frame)
        webView.navigationDelegate = self
        webView.load(request as URLRequest)
        self.view.addSubview(webView)
        self.view.sendSubview(toBack: webView)
    }
    
    @objc override func btnBackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
   

}
extension PrivacyVC : WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        DLog(error.localizedDescription)
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        DLog("Strat to load")
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        DLog("finish to load")
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

    }
}
