//
//  PickerViewVC.swift
//  ExpenseManager
//
//  Created by Piyu on 02/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

protocol ThemeColorVCDelegate {
    func savedThemeColorSuccessful()
}

struct ThemeColor {
    var colorName: String
    var isSelected: Bool
}

class ThemeColorVC: UIViewController {
    
    @IBOutlet weak var viewTransparentBackground: UIView!
    @IBOutlet weak var viewCenterTheme: UIView!
    @IBOutlet weak var lblSelectTheme: UILabel!
    @IBOutlet weak var collectionViewColors: UICollectionView!
    
    var delegate: ThemeColorVCDelegate?
    
    var arrThemeColors = [
    
        ThemeColor(colorName: ConstantValues.DEFAULT_THEME_COLOR, isSelected: false),
        ThemeColor(colorName: "#568a58", isSelected: false),
        //ThemeColor(colorName: "#406742", isSelected: false),
        ThemeColor(colorName: "#03A9F4", isSelected: false),
        //ThemeColor(colorName: "#0288D1", isSelected: false),
        ThemeColor(colorName: "#37474F", isSelected: false),
//        ThemeColor(colorName: "#263238", isSelected: false),
        ThemeColor(colorName: "#3F51B5", isSelected: false),
//        ThemeColor(colorName: "#303F9F", isSelected: false),
        ThemeColor(colorName: "#252c87", isSelected: false),
//        ThemeColor(colorName: "#1d236c", isSelected: false),
        ThemeColor(colorName: "#b86f00", isSelected: false),
//        ThemeColor(colorName: "#935800", isSelected: false),
        ThemeColor(colorName: "#795548", isSelected: false),
//        ThemeColor(colorName: "#5D4037", isSelected: false),
        ThemeColor(colorName: "#9C27B0", isSelected: false),
//        ThemeColor(colorName: "#7B1FA2", isSelected: false),
        ThemeColor(colorName: "#673AB7", isSelected: false),
//        ThemeColor(colorName: "#512DA8", isSelected: false),
        ThemeColor(colorName: "#4DB6AC", isSelected: false),
//        ThemeColor(colorName: "#3d9189", isSelected: false),
        ThemeColor(colorName: "#009688", isSelected: false),
//        ThemeColor(colorName: "#00796B", isSelected: false),
        ThemeColor(colorName: "#CDDC39", isSelected: false),
//        ThemeColor(colorName: "#AFB42B", isSelected: false),
        ThemeColor(colorName: "#8BC34A", isSelected: false),
//        ThemeColor(colorName: "#689F38", isSelected: false),
        ThemeColor(colorName: "#4CAF50", isSelected: false),
//        ThemeColor(colorName: "#388E3C", isSelected: false),
        ThemeColor(colorName: "#378039", isSelected: false),
//        ThemeColor(colorName: "#2c662d", isSelected: false),
        ThemeColor(colorName: "#ff82aa", isSelected: false),
//        ThemeColor(colorName: "#cc6888", isSelected: false),
        ThemeColor(colorName: "#FF9800", isSelected: false),
//        ThemeColor(colorName: "#F57C00", isSelected: false),
        ThemeColor(colorName: "#FF5722", isSelected: false),
//        ThemeColor(colorName: "#E64A19", isSelected: false),
        ThemeColor(colorName: "#E91E63", isSelected: false),
//        ThemeColor(colorName: "#C2185B", isSelected: false),
        ThemeColor(colorName: "#F44336", isSelected: false),
//        ThemeColor(colorName: "#D32F2F", isSelected: false),
        ThemeColor(colorName: "#da3700", isSelected: false),
//        ThemeColor(colorName: "#ae2c00", isSelected: false),
        ThemeColor(colorName: "#bf1017", isSelected: false),
//        ThemeColor(colorName: "#980c12", isSelected: false),
        ThemeColor(colorName: "#880016", isSelected: false),
//        ThemeColor(colorName: "#6c0011", isSelected: false)
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
}

extension ThemeColorVC {
    
    func initialSetup() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTransparentView))
        self.viewTransparentBackground.addGestureRecognizer(tap)
        setThemeColor()
        self.showHideThemeView(isShow: true)
    }
    
    func setThemeColor() {
        
        if let themeColor = UserDefaultManager.getData(forKey: Parameters.UserDefault.ThemeColor) as? String {
            
            arrThemeColors = arrThemeColors.map { (tempObject) -> ThemeColor in
                var themeColorObject = tempObject
                themeColorObject.isSelected = (themeColorObject.colorName == themeColor)
                return themeColorObject
            }
        }
        
        self.reloadData()
    }
    
    @objc func tapOnTransparentView() {
        self.showHideThemeView(isShow: false)
    }
    
    func reloadData() {
        runOnMainThread {
            self.collectionViewColors.reloadData()
        }
    }
    
    func showHideThemeView(isShow: Bool) {
        
        runOnMainThread {
            self.viewCenterTheme.transform = isShow ? CGAffineTransform(scaleX: 0.001, y: 0.001) : CGAffineTransform.identity
            self.viewCenterTheme.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.viewCenterTheme.transform = isShow ? CGAffineTransform.identity : CGAffineTransform(scaleX: 0.001, y: 0.001)
                self.viewTransparentBackground.alpha = isShow ? 0.5 : 0
            }) { (result) in
                if !isShow {
                    self.view.removeFromSuperview()
                    self.removeFromParentViewController()
                }
            }
        }
    }
}

extension ThemeColorVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrThemeColors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier.Cell.ThemeColorCVC, for: indexPath) as! ThemeColorCVC
        
        let object = arrThemeColors[indexPath.item]
        cell.viewThemeColor.layer.cornerRadius = cell.viewThemeColor.frame.width / 2
        cell.viewThemeColor.backgroundColor = UIColor(hexString: object.colorName)
        cell.imgViewCheck.isHidden = !object.isSelected
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: collectionView.frame.width / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        UserDefaultManager.setData(value: arrThemeColors[indexPath.item].colorName, forKey: Parameters.UserDefault.ThemeColor)
        setThemeColor()
        if delegate != nil {
            delegate?.savedThemeColorSuccessful()
        }
        showHideThemeView(isShow: false)
    }
}

