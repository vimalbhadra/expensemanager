//
//  StudentInfo.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

struct AccountInfo: Codable {
    
    var id: Int32?
    var name: String?
    var status: Int?
    var amount: Double?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case status = "status"
        case amount = "amount"
    }
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int32.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        amount = try values.decodeIfPresent(Double.self, forKey: .amount)
    }
}

extension AccountInfo {
    
    static func getAllAccount() -> [AccountInfo] {
        ModelManager.sharedInstance.database!.open()
        let resultSet: FMResultSet! = ModelManager.sharedInstance.database!.executeQuery("SELECT * FROM \(TableName.Account_Table.name)", withArgumentsIn: nil)
        
        
        var arrAccounts = [AccountInfo]()
        if (resultSet != nil) {
            while resultSet.next() {
                var accountInfo: AccountInfo = AccountInfo()
                accountInfo.id = resultSet.int(forColumn: TableName.Account_Table.Account_Id)
                accountInfo.name = resultSet.string(forColumn: TableName.Account_Table.Account_Name)
                arrAccounts.append(accountInfo)
            }
        }
        ModelManager.sharedInstance.database!.close()
        return arrAccounts
    }
    
    static func isAlreadyExistAccount(name: String) -> Bool {
        
        ModelManager.sharedInstance.database!.open()
        let resultSet: FMResultSet! = ModelManager.sharedInstance.database!.executeQuery("SELECT * FROM \(TableName.Account_Table.name) WHERE \(TableName.Account_Table.Account_Name) == ?", withArgumentsIn: [name])
        if (resultSet != nil) {
            while resultSet.next() {
                return true
            }
        }
        
        ModelManager.sharedInstance.database!.close()
        return false
    }
    
    static func addAccount(accountInfo: AccountInfo) -> Bool {
        
        if !isAlreadyExistAccount(name: accountInfo.name ?? "") {
            
            ModelManager.sharedInstance.database!.open()
            let isInserted = ModelManager.sharedInstance.database!.executeUpdate("INSERT INTO \(TableName.Account_Table.name)(\(TableName.Account_Table.Account_Name)) VALUES(?)", withArgumentsIn: [accountInfo.name ?? ""])
            ModelManager.sharedInstance.database!.close()
            if !isInserted {
                APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
            }
            
            return isInserted
            
        } else {
            APPDELEGATE.showToastMessage(Messages.accountExist, wantBottomSide: true, hideAutomatically: true)
        }
        
        return false
    }
    
    static func updateAccount(accountInfo: AccountInfo) -> Bool {
        
        if !isAlreadyExistAccount(name: accountInfo.name ?? "") {
            
            ModelManager.sharedInstance.database!.open()
            let isUpdated = ModelManager.sharedInstance.database!.executeUpdate("UPDATE \(TableName.Account_Table.name) SET \(TableName.Account_Table.Account_Name) = ? WHERE \(TableName.Account_Table.Account_Id) = ?", withArgumentsIn: [(accountInfo.name ?? "") as Any, accountInfo.id ?? -1 as Any])
            ModelManager.sharedInstance.database!.close()
            if !isUpdated {
                APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
            }
            
            return isUpdated
            
        } else {
            APPDELEGATE.showToastMessage(Messages.accountExist, wantBottomSide: true, hideAutomatically: true)
        }
        return false
    }
    
    static func deleteAccount(accountInfo: AccountInfo) -> Bool {
        
        ModelManager.sharedInstance.database!.open()
        let isDeleted = ModelManager.sharedInstance.database!.executeUpdate("DELETE FROM \(TableName.Account_Table.name) WHERE \(TableName.Account_Table.Account_Id) = ?", withArgumentsIn: [accountInfo.id ?? -1])
        ModelManager.sharedInstance.database!.close()
        if !isDeleted {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        
        return isDeleted
    }
}
