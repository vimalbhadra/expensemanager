//
//  CategoryManagment.swift
//  ExpenseManager
//
//  Created by Apple Customer on 01/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

struct CategoryManagment: Codable {
    
    var categoryId: Int32?
    var categoryName: String?
    var categoryIconName: String?
    var categoryCount: String?
    var categoryAccountId: Int32?
  
    enum CodingKeys: String, CodingKey {
        case categoryId = "Category_Id"
        case categoryName = "Category_Name"
        case categoryIconName = "Category_Icon_Name"
        case categoryCount = "Category_Count"
        case categoryAccountId = "Category_Account_Id"

    }
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        categoryId = try values.decodeIfPresent(Int32.self, forKey: .categoryId)
        categoryName = try values.decodeIfPresent(String.self, forKey: .categoryName)
        categoryIconName = try values.decodeIfPresent(String.self, forKey: .categoryIconName)
        categoryCount = try values.decodeIfPresent(String.self, forKey: .categoryCount)
        categoryAccountId = try values.decodeIfPresent(Int32.self, forKey: .categoryAccountId)

    }
}
extension CategoryManagment {
    
    static func getAllCategory(account_Id: Int32) -> [CategoryManagment] {
        ModelManager.sharedInstance.database!.open()
        let resultSet: FMResultSet! = ModelManager.sharedInstance.database!.executeQuery("SELECT * FROM \(TableName.Category_Table.name)  WHERE \(TableName.Category_Table.Category_Account_Id) == \(account_Id)", withArgumentsIn: nil)
        var arrCategory = [CategoryManagment]()
        if (resultSet != nil) {
            while resultSet.next() {
                var categoryInfo: CategoryManagment = CategoryManagment()
                categoryInfo.categoryId = resultSet.int(forColumn: TableName.Category_Table.Category_Id)
                categoryInfo.categoryName = resultSet.string(forColumn: TableName.Category_Table.Category_Name)
                categoryInfo.categoryIconName = resultSet.string(forColumn: TableName.Category_Table.Category_Icon_Name)
                categoryInfo.categoryCount = resultSet.string(forColumn: TableName.Category_Table.Category_Count)
                categoryInfo.categoryAccountId = resultSet.int(forColumn: TableName.Category_Table.Category_Account_Id)
                arrCategory.append(categoryInfo)
            }        }
        ModelManager.sharedInstance.database!.close()
        return arrCategory
      }
    
   static func addCategoryData(categoryInfo: CategoryManagment) -> Bool {
        ModelManager.sharedInstance.database!.open()
    
        let isInserted = ModelManager.sharedInstance.database!.executeUpdate("INSERT INTO \(TableName.Category_Table.name)(\(TableName.Category_Table.Category_Name),\(TableName.Category_Table.Category_Icon_Name),\(TableName.Category_Table.Category_Count),\(TableName.Category_Table.Category_Account_Id)) VALUES(?,?,?,?)", withArgumentsIn: [categoryInfo.categoryName ?? "",categoryInfo.categoryIconName ?? "",categoryInfo.categoryCount ?? "0",categoryInfo.categoryAccountId ?? ""])
          ModelManager.sharedInstance.database!.close()

        if !isInserted {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        return isInserted
    }
    
    static func deleteAllCategory(categoryAccountID:Int32) -> Bool {
        
        ModelManager.sharedInstance.database!.open()
        let isDeleted = ModelManager.sharedInstance.database!.executeUpdate("DELETE  FROM \(TableName.Category_Table.name) WHERE \(TableName.Category_Table.Category_Account_Id) = ?", withArgumentsIn: [categoryAccountID])
        ModelManager.sharedInstance.database!.close()
        if !isDeleted {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        
        return isDeleted
    }
    
    static func isAlreadyExistCategory(name: String,accountID:Int32) -> Bool {
        
        ModelManager.sharedInstance.database!.open()
        let resultSet: FMResultSet! = ModelManager.sharedInstance.database!.executeQuery("SELECT * FROM \(TableName.Category_Table.name) WHERE \(TableName.Category_Table.Category_Name) == ? AND \(TableName.Category_Table.Category_Account_Id) == ?", withArgumentsIn: [name,accountID])
        if (resultSet != nil) {
            while resultSet.next() {
                return true
            }
        }
        
        ModelManager.sharedInstance.database!.close()
        return false
    }
    
    static func updateCategory(categoryInfo: CategoryManagment) -> Bool {
        
        if !isAlreadyExistCategory(name: categoryInfo.categoryName ?? "", accountID: categoryInfo.categoryAccountId!) {
            
            ModelManager.sharedInstance.database!.open()
            let isUpdated = ModelManager.sharedInstance.database!.executeUpdate("UPDATE \(TableName.Category_Table.name) SET \(TableName.Category_Table.Category_Name) = ?,\(TableName.Category_Table.Category_Icon_Name) = ? WHERE \(TableName.Category_Table.Category_Id) = ?", withArgumentsIn: [(categoryInfo.categoryName! ) as Any,(categoryInfo.categoryIconName! ) as Any, categoryInfo.categoryId! as Any])
            ModelManager.sharedInstance.database!.close()
            if !isUpdated {
                APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
            }
            
            return isUpdated
            
        } else {
            APPDELEGATE.showToastMessage(Messages.categoryExist, wantBottomSide: true, hideAutomatically: true)
        }
        return false
    }
}
