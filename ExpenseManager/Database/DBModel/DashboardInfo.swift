//
//  DashboardInfo.swift
//  ExpenseManager
//
//  Created by Piyu on 03/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

enum TransactionTypeName :String {
    
    case EXPENSE = "EXPENSE"
    case INCOME = "INCOME"
}

struct DashboardInfo: Codable {
    
    var transactionId: Int32?
    var transactionAmount: String?
    var transactionDate: String?
    var transactionCatId: Int32?
    var transactionPayType: String?
    var transactionType: String?
    var transactionNote: String?
    var transactionAccountId: Int32?
    var categoryName: String?
    var categoryIconName: String?

    enum CodingKeys: String, CodingKey {
        case transactionId = "Transaction_Id"
        case transactionAmount = "Transaction_Amount"
        case transactionDate = "Transaction_Date"//YYYY-MM-DD
        case transactionCatId = "Transaction_Cat_Id"
        case transactionPayType = "Transaction_PayType"//cash-credit
        case transactionType = "Transaction_Type"//income-expense
        case transactionAccountId = "Transaction_Account_Id"
        case transactionNote = "Transaction_Note"
        case categoryName = "Category_Name"
        case categoryIconName = "Category_Icon_Name"
    
    }
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        transactionId = try values.decodeIfPresent(Int32.self, forKey: .transactionId)
        transactionAmount = try values.decodeIfPresent(String.self, forKey: .transactionAmount)
        transactionDate = try values.decodeIfPresent(String.self, forKey: .transactionDate)
        transactionCatId = try values.decodeIfPresent(Int32.self, forKey: .transactionCatId)
        transactionPayType = try values.decodeIfPresent(String.self, forKey: .transactionPayType)
        transactionType = try values.decodeIfPresent(String.self, forKey: .transactionType)
        transactionNote = try values.decodeIfPresent(String.self, forKey: .transactionNote)
        transactionAccountId = try values.decodeIfPresent(Int32.self, forKey: .transactionAccountId)
        categoryName = try values.decodeIfPresent(String.self, forKey: .categoryName)
        categoryIconName = try values.decodeIfPresent(String.self, forKey: .categoryIconName)
        
    }
}
extension DashboardInfo {
    
    static func getAllTransacation(account_Id: Int32) -> [DashboardInfo] {
        ModelManager.sharedInstance.database!.open()
        let resultSet: FMResultSet! = ModelManager.sharedInstance.database!.executeQuery("SELECT \(TableName.Transaction_Table.Transaction_Id),\(TableName.Transaction_Table.Transaction_Date),\(TableName.Transaction_Table.Transaction_Cat_Id),\(TableName.Transaction_Table.Transaction_Account_Id),\(TableName.Category_Table.Category_Name),\(TableName.Transaction_Table.Transaction_Note),\(TableName.Transaction_Table.Transaction_Amount),\(TableName.Transaction_Table.Transaction_Type),\(TableName.Transaction_Table.Transaction_PayType),\(TableName.Category_Table.Category_Icon_Name) FROM \(TableName.Transaction_Table.name),\(TableName.Category_Table.name) where \(TableName.Transaction_Table.Transaction_Cat_Id) = \(TableName.Category_Table.Category_Id) AND \(TableName.Transaction_Table.Transaction_Account_Id) == ? order by \(TableName.Transaction_Table.Transaction_Date) desc , \(TableName.Transaction_Table.Transaction_Id) desc", withArgumentsIn: [account_Id])
        var arrDashboard = [DashboardInfo]()
        if (resultSet != nil) {
            while resultSet.next() {
                var dashboardInfo: DashboardInfo = DashboardInfo()
                dashboardInfo.transactionId = resultSet.int(forColumn: TableName.Transaction_Table.Transaction_Id)
                dashboardInfo.transactionAmount = resultSet.string(forColumn: TableName.Transaction_Table.Transaction_Amount)
                dashboardInfo.transactionAmount = dashboardInfo.transactionAmount?.replacingOccurrences(of: ",", with: ".")
                dashboardInfo.transactionDate = resultSet.string(forColumn: TableName.Transaction_Table.Transaction_Date)
                dashboardInfo.transactionPayType = resultSet.string(forColumn: TableName.Transaction_Table.Transaction_PayType)
                dashboardInfo.transactionType = resultSet.string(forColumn: TableName.Transaction_Table.Transaction_Type)
                 dashboardInfo.transactionNote = resultSet.string(forColumn: TableName.Transaction_Table.Transaction_Note)
                dashboardInfo.transactionAccountId = resultSet.int(forColumn: TableName.Transaction_Table.Transaction_Account_Id)
                dashboardInfo.transactionCatId = resultSet.int(forColumn: TableName.Transaction_Table.Transaction_Cat_Id)
                 dashboardInfo.categoryName = resultSet.string(forColumn: TableName.Category_Table.Category_Name)
                 dashboardInfo.categoryIconName = resultSet.string(forColumn: TableName.Category_Table.Category_Icon_Name)
                arrDashboard.append(dashboardInfo)
            }        }
        ModelManager.sharedInstance.database!.close()
        return arrDashboard
    }
}

