//
//  RecurrentInfo.swift
//  ExpenseManager
//
//  Created by Apple Customer on 04/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
struct RecurrentInfo: Codable {
    
    var recurrentColumnId: Int32?
    var recurrentAmount: String?
    var recurrentNextDate: String?
    var recurrentCatId: Int32?
    var recurrentPayType: String?
    var recurrentType: String?
    var recurrentTransactionType: String?
    var recurrentNote: String?
    var recurrentAccountId: Int32?
    var recurrentcategoryName: String?
    var recurrentcategoryIconName: String?
    
    enum CodingKeys: String, CodingKey {
        case recurrentColumnId = "Recurrent_Column_Id"
        case recurrentAmount = "Recurrent_Amount"
        case recurrentNextDate = "Recurrent_Next_Date"//YYYY-MM-DD
        case recurrentCatId = "Recurrent_Cat_Id"
        case recurrentPayType = "Recurrent_PayType"//cash-credit
        case recurrentType = "Recurrent_Type"//DAILY-WeeklY
        case recurrentTransactionType = "Recurrent_Transaction_Type"//income-expense
        case recurrentAccountId = "Recurrent_Account_Id"
        case recurrentNote = "Recurrent_Note"
        case recurrentcategoryName = "Category_Name"
        case recurrentcategoryIconName = "Category_Icon_Name"
    }
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        recurrentColumnId = try values.decodeIfPresent(Int32.self, forKey: .recurrentColumnId)
        recurrentAmount = try values.decodeIfPresent(String.self, forKey: .recurrentAmount)
        recurrentNextDate = try values.decodeIfPresent(String.self, forKey: .recurrentNextDate)
        recurrentCatId = try values.decodeIfPresent(Int32.self, forKey: .recurrentCatId)
        recurrentPayType = try values.decodeIfPresent(String.self, forKey: .recurrentPayType)
        recurrentType = try values.decodeIfPresent(String.self, forKey: .recurrentType)
        recurrentTransactionType = try values.decodeIfPresent(String.self, forKey: .recurrentTransactionType)
        recurrentNote = try values.decodeIfPresent(String.self, forKey: .recurrentNote)
        recurrentAccountId = try values.decodeIfPresent(Int32.self, forKey: .recurrentAccountId)
        recurrentcategoryName = try values.decodeIfPresent(String.self, forKey: .recurrentcategoryName)
        recurrentcategoryIconName = try values.decodeIfPresent(String.self, forKey: .recurrentcategoryIconName)
    }
}
extension RecurrentInfo{
    
    
    static func getAllRecurrentTransacation() -> [RecurrentInfo] {
        ModelManager.sharedInstance.database!.open()
        let resultSet: FMResultSet! = ModelManager.sharedInstance.database!.executeQuery("SELECT * FROM \(TableName.Recurrent_Table.name)", withArgumentsIn: nil)
        var arrRecurrentList = [RecurrentInfo]()
        if (resultSet != nil) {
            while resultSet.next() {
                var recurrentList: RecurrentInfo = RecurrentInfo()
                recurrentList.recurrentColumnId = resultSet.int(forColumn: TableName.Recurrent_Table.Recurrent_Column_Id)
                recurrentList.recurrentType = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_Type)
                recurrentList.recurrentNextDate = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_Next_Date)
                recurrentList.recurrentPayType = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_PayType)
                recurrentList.recurrentNote = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_Note)
                recurrentList.recurrentAmount = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_Amount)
                recurrentList.recurrentTransactionType = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_Transaction_Type)
                recurrentList.recurrentAccountId = resultSet.int(forColumn: TableName.Recurrent_Table.Recurrent_Account_Id)
                recurrentList.recurrentCatId = resultSet.int(forColumn: TableName.Recurrent_Table.Recurrent_Cat_Id)
                arrRecurrentList.append(recurrentList)
            }
        }
        ModelManager.sharedInstance.database!.close()
        return arrRecurrentList
    }
    
    
    
    
    
    
    
    
    static func getAllRecurrentTransacation(account_Id: Int32) -> [RecurrentInfo] {
        ModelManager.sharedInstance.database!.open()
        let resultSet: FMResultSet! = ModelManager.sharedInstance.database!.executeQuery("SELECT \(TableName.Recurrent_Table.Recurrent_Column_Id),\(TableName.Recurrent_Table.Recurrent_Type),\(TableName.Recurrent_Table.Recurrent_Next_Date),\(TableName.Recurrent_Table.Recurrent_PayType),\(TableName.Recurrent_Table.Recurrent_Note),\(TableName.Recurrent_Table.Recurrent_Amount),\(TableName.Recurrent_Table.Recurrent_Cat_Id),\(TableName.Recurrent_Table.Recurrent_Account_Id),\(TableName.Category_Table.Category_Name),\(TableName.Recurrent_Table.Recurrent_Transaction_Type),\(TableName.Category_Table.Category_Icon_Name) FROM \(TableName.Recurrent_Table.name),\(TableName.Category_Table.name) where \(TableName.Recurrent_Table.Recurrent_Cat_Id) = \(TableName.Category_Table.Category_Id) AND \(TableName.Recurrent_Table.Recurrent_Account_Id) == ? order by \(TableName.Recurrent_Table.Recurrent_Next_Date) desc, \(TableName.Recurrent_Table.Recurrent_Column_Id) desc", withArgumentsIn: [account_Id])
        var arrRecurrentList = [RecurrentInfo]()
        if (resultSet != nil) {
            while resultSet.next() {
                var recurrentList: RecurrentInfo = RecurrentInfo()
                recurrentList.recurrentColumnId = resultSet.int(forColumn: TableName.Recurrent_Table.Recurrent_Column_Id)
                recurrentList.recurrentType = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_Type)
                recurrentList.recurrentNextDate = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_Next_Date)
                recurrentList.recurrentPayType = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_PayType)
                recurrentList.recurrentNote = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_Note)
                recurrentList.recurrentAmount = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_Amount)
                recurrentList.recurrentTransactionType = resultSet.string(forColumn: TableName.Recurrent_Table.Recurrent_Transaction_Type)
                recurrentList.recurrentAccountId = resultSet.int(forColumn: TableName.Recurrent_Table.Recurrent_Account_Id)
                recurrentList.recurrentCatId = resultSet.int(forColumn: TableName.Recurrent_Table.Recurrent_Cat_Id)
                recurrentList.recurrentcategoryName = resultSet.string(forColumn: TableName.Category_Table.Category_Name)
                recurrentList.recurrentcategoryIconName = resultSet.string(forColumn: TableName.Category_Table.Category_Icon_Name)
                arrRecurrentList.append(recurrentList)
            }
            
        }
        ModelManager.sharedInstance.database!.close()
        return arrRecurrentList
    }
    
    
    static func addRecurrentTransaction(recurrentTransactionInfo: RecurrentInfo) -> Bool {
        ModelManager.sharedInstance.database!.open()
        
        let isInserted = ModelManager.sharedInstance.database!.executeUpdate("INSERT INTO \(TableName.Recurrent_Table.name)(\(TableName.Recurrent_Table.Recurrent_Transaction_Type),\(TableName.Recurrent_Table.Recurrent_Type),\(TableName.Recurrent_Table.Recurrent_Amount),\(TableName.Recurrent_Table.Recurrent_Next_Date),\(TableName.Recurrent_Table.Recurrent_Cat_Id),\(TableName.Recurrent_Table.Recurrent_PayType),\(TableName.Recurrent_Table.Recurrent_Account_Id),\(TableName.Recurrent_Table.Recurrent_Note)) VALUES(?,?,?,?,?,?,?,?)", withArgumentsIn: [recurrentTransactionInfo.recurrentTransactionType! ,recurrentTransactionInfo.recurrentType!,recurrentTransactionInfo.recurrentAmount!.replacingOccurrences(of: ",", with: ".") ,recurrentTransactionInfo.recurrentNextDate! ,recurrentTransactionInfo.recurrentCatId!,recurrentTransactionInfo.recurrentPayType! ,recurrentTransactionInfo.recurrentAccountId!,recurrentTransactionInfo.recurrentNote!])
        ModelManager.sharedInstance.database!.close()
        if !isInserted {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        return isInserted
    }
    
    static func editRecurrentTransaction(retransactionInfo: RecurrentInfo) -> Bool {
        ModelManager.sharedInstance.database!.open()
        let isUpdated = ModelManager.sharedInstance.database!.executeUpdate("UPDATE \(TableName.Recurrent_Table.name) SET \(TableName.Recurrent_Table.Recurrent_Transaction_Type) = ?,\(TableName.Recurrent_Table.Recurrent_Type) = ?, \(TableName.Recurrent_Table.Recurrent_Type) = ?,\(TableName.Recurrent_Table.Recurrent_Amount) = ?,\(TableName.Recurrent_Table.Recurrent_Next_Date) = ?,\(TableName.Recurrent_Table.Recurrent_Cat_Id) = ?,\(TableName.Recurrent_Table.Recurrent_Note) = ? WHERE \(TableName.Recurrent_Table.Recurrent_Column_Id) = ?", withArgumentsIn: [retransactionInfo.recurrentTransactionType!,retransactionInfo.recurrentPayType!,retransactionInfo.recurrentType!,retransactionInfo.recurrentAmount!.replacingOccurrences(of: ",", with: "."),retransactionInfo.recurrentNextDate!,retransactionInfo.recurrentCatId!,retransactionInfo.recurrentNote!, retransactionInfo.recurrentColumnId!])
        ModelManager.sharedInstance.database!.close()
        if !isUpdated {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        
        return isUpdated
    }
    
    static func updateRecurrentTransNextDate(nextDate:String,recurrentColumnId:Int32) -> Bool {
        ModelManager.sharedInstance.database!.open()
        let isUpdated = ModelManager.sharedInstance.database!.executeUpdate("UPDATE \(TableName.Recurrent_Table.name) SET \(TableName.Recurrent_Table.Recurrent_Next_Date) = ? WHERE \(TableName.Recurrent_Table.Recurrent_Column_Id) = ?", withArgumentsIn: [nextDate,recurrentColumnId])
        ModelManager.sharedInstance.database!.close()
        if !isUpdated {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        
        return isUpdated
    }
    
    static func deleteReCurrentTransacation(recurrentColumnID:Int32) -> Bool {
        
        ModelManager.sharedInstance.database!.open()
        let isDeleted = ModelManager.sharedInstance.database!.executeUpdate("DELETE FROM \(TableName.Recurrent_Table.name) WHERE \(TableName.Recurrent_Table.Recurrent_Column_Id) = ?", withArgumentsIn: [recurrentColumnID])
        ModelManager.sharedInstance.database!.close()
        if !isDeleted {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        
        return isDeleted
    }
    static func deleteAllRecurrentTransacation(accountID:Int32) -> Bool {
        
        ModelManager.sharedInstance.database!.open()
        let isDeleted = ModelManager.sharedInstance.database!.executeUpdate("DELETE  FROM \(TableName.Recurrent_Table.name) WHERE \(TableName.Recurrent_Table.Recurrent_Account_Id) = ?", withArgumentsIn: [accountID])
        ModelManager.sharedInstance.database!.close()
        if !isDeleted {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        
        return isDeleted
    }
}
