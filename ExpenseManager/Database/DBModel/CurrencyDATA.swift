//
//  CurrencyDATA.swift
//  ExpenseManager
//
//  Created by Piyu on 09/02/19.
//  Copyright © 2019 Jigar Parmar. All rights reserved.
//

import UIKit
struct CurrencyDATA : Codable {
    
    var cURRENCYSYMBOL : String?
    var cURRENCYSNAME : String?
    var cURRENCYFNAME : String?

    
    enum CodingKeys: String,CodingKey {
        case cURRENCYSYMBOL = "CRRENCYSYMBOL"
        case cURRENCYSNAME = "CURRENCYSNAME"
        case cURRENCYFNAME = "CURRENCYFNAME"

    }
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cURRENCYSYMBOL = try values.decodeIfPresent(String.self, forKey: .cURRENCYSYMBOL)
        cURRENCYSNAME = try values.decodeIfPresent(String.self, forKey: .cURRENCYSNAME)
        cURRENCYFNAME = try values.decodeIfPresent(String.self, forKey: .cURRENCYFNAME)

    }
    
}
