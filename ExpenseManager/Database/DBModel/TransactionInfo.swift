//
//  TransactionInfo.swift
//  ExpenseManager
//
//  Created by Piyu on 02/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
struct TransactionInfo: Codable {
    
    var transactionId: Int32?
    var transactionAmount: String?
    var transactionDate: String?
    var transactionCatId: Int32?
    var transactionPayType: String?
    var transactionType: String?
    var transactionNote: String?
    var transactionAccountId: Int32?

    enum CodingKeys: String, CodingKey {
        case transactionId = "Transaction_Id"
        case transactionAmount = "Transaction_Amount"
        case transactionDate = "Transaction_Date"//YYYY-MM-DD
        case transactionCatId = "Transaction_Cat_Id"
        case transactionPayType = "Transaction_PayType"//cash-credit
        case transactionType = "Transaction_Type"//income-expense
        case transactionAccountId = "Transaction_Account_Id"
        case transactionNote = "Transaction_Note"


    }
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        transactionId = try values.decodeIfPresent(Int32.self, forKey: .transactionId)
        transactionAmount = try values.decodeIfPresent(String.self, forKey: .transactionAmount)
        transactionDate = try values.decodeIfPresent(String.self, forKey: .transactionDate)
        transactionCatId = try values.decodeIfPresent(Int32.self, forKey: .transactionCatId)
        transactionPayType = try values.decodeIfPresent(String.self, forKey: .transactionPayType)
        transactionType = try values.decodeIfPresent(String.self, forKey: .transactionType)
        transactionNote = try values.decodeIfPresent(String.self, forKey: .transactionNote)
        transactionAccountId = try values.decodeIfPresent(Int32.self, forKey: .transactionAccountId)

    }
}

extension TransactionInfo{
    static func addTransaction(transactionInfo: TransactionInfo) -> Bool {
        ModelManager.sharedInstance.database!.open()
        
        let isInserted = ModelManager.sharedInstance.database!.executeUpdate("INSERT INTO \(TableName.Transaction_Table.name)(\(TableName.Transaction_Table.Transaction_Type),\(TableName.Transaction_Table.Transaction_Amount),\(TableName.Transaction_Table.Transaction_Date),\(TableName.Transaction_Table.Transaction_Cat_Id),\(TableName.Transaction_Table.Transaction_PayType),\(TableName.Transaction_Table.Transaction_Account_Id),\(TableName.Transaction_Table.Transaction_Note)) VALUES(?,?,?,?,?,?,?)", withArgumentsIn: [transactionInfo.transactionType ?? "",transactionInfo.transactionAmount?.replacingOccurrences(of: ",", with: ".") ?? "",transactionInfo.transactionDate ?? "",transactionInfo.transactionCatId ?? "",transactionInfo.transactionPayType ?? "",transactionInfo.transactionAccountId!,transactionInfo.transactionNote ?? ""])
        ModelManager.sharedInstance.database!.close()
        
        if !isInserted {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        return isInserted
    }
    static func editTransaction(transactionInfo: TransactionInfo) -> Bool {
        ModelManager.sharedInstance.database!.open()
        let isUpdated = ModelManager.sharedInstance.database!.executeUpdate("UPDATE \(TableName.Transaction_Table.name) SET \(TableName.Transaction_Table.Transaction_Type) = ?,\(TableName.Transaction_Table.Transaction_Amount) = ?,\(TableName.Transaction_Table.Transaction_Date) = ?,\(TableName.Transaction_Table.Transaction_Cat_Id) = ?,\(TableName.Transaction_Table.Transaction_Note) = ?,\(TableName.Transaction_Table.Transaction_Account_Id) = ? WHERE \(TableName.Transaction_Table.Transaction_Id) = ?", withArgumentsIn: [transactionInfo.transactionType!,transactionInfo.transactionAmount!.replacingOccurrences(of: ",", with: "."),transactionInfo.transactionDate!,transactionInfo.transactionCatId!,transactionInfo.transactionNote!, transactionInfo.transactionAccountId!, transactionInfo.transactionId!])
        ModelManager.sharedInstance.database!.close()
        if !isUpdated {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        
        return isUpdated
    }
    static func deleteTransacation(transactionID:Int32) -> Bool {
        
        ModelManager.sharedInstance.database!.open()
        let isDeleted = ModelManager.sharedInstance.database!.executeUpdate("DELETE FROM \(TableName.Transaction_Table.name) WHERE \(TableName.Transaction_Table.Transaction_Id) = ?", withArgumentsIn: [transactionID])
        ModelManager.sharedInstance.database!.close()
        if !isDeleted {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        
        return isDeleted
    }
    
    static func deleteAllTransacation(accountID:Int32) -> Bool {
        
        ModelManager.sharedInstance.database!.open()
        let isDeleted = ModelManager.sharedInstance.database!.executeUpdate("DELETE  FROM \(TableName.Transaction_Table.name) WHERE \(TableName.Transaction_Table.Transaction_Account_Id) = ?", withArgumentsIn: [accountID])
        ModelManager.sharedInstance.database!.close()
        if !isDeleted {
            APPDELEGATE.showToastMessage(Messages.tryAgain, wantBottomSide: true, hideAutomatically: true)
        }
        
        return isDeleted
    }
    
}
