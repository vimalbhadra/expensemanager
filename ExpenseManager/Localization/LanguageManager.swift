//
//  LanguageManager.swift
//  SMSLocalization
//
//  Created by Macuser on 5/19/16.
//  Copyright © 2016 Macuser. All rights reserved.
//

import UIKit

class LanguageManager: NSObject {
    
    
    var availableLocales = [CustomLocale]()
    static let sharedInstance = LanguageManager()
    var lprojBasePath = String()
    
    override fileprivate init() {
        
        super.init()
        let english = CustomLocale(languageCode: GlobalConstants.englishCode, countryCode: "en", name: "United Kingdom")
        let spanish  = CustomLocale(languageCode: GlobalConstants.spanishCode, countryCode: "es", name: "Spanish")
        let hindi  = CustomLocale(languageCode: GlobalConstants.hindiCode, countryCode: "hi", name: "Hindi")
        let gujrati  = CustomLocale(languageCode: GlobalConstants.gujratiCode, countryCode: "gu", name: "Gujrati")
        let arabic  = CustomLocale(languageCode: GlobalConstants.arabicCode, countryCode: "ar", name: "Arabic")
        let french  = CustomLocale(languageCode: GlobalConstants.frenchCode, countryCode: "fr", name: "French")
        let japanese  = CustomLocale(languageCode: GlobalConstants.japaneseCode, countryCode: "ja", name: "Japanese")
        let german  = CustomLocale(languageCode: GlobalConstants.germanCode, countryCode: "de", name: "German")
        let portuguese  = CustomLocale(languageCode: GlobalConstants.portuguese, countryCode: "pt", name: "Portuguese")
        let russian  = CustomLocale(languageCode: GlobalConstants.russian, countryCode: "ru", name: "Russian")

        self.availableLocales = [english,spanish,hindi,gujrati,arabic,french,japanese,german,portuguese,russian]
        self.lprojBasePath =  getSelectedLocale()
    }
    
    
    fileprivate func getSelectedLocale()->String{
        
        let lang = Locale.preferredLanguages//returns array of preferred languages
        let languageComponents: [String : String] = Locale.components(fromIdentifier: lang[0])
        if let languageCode: String = languageComponents["kCFLocaleLanguageCodeKey"]{
            
            for customlocale in availableLocales {
                
                if(customlocale.languageCode == languageCode){
                    
                    return customlocale.languageCode!
                }
            }
        }
        return "en"
    }
    
    func getCurrentBundle()->Bundle{
        
        if let bundle = Bundle.main.path(forResource: lprojBasePath, ofType: "lproj"){
            
            return Bundle(path: bundle)!
            
        }else{
            
            fatalError("lproj files not found on project directory. /n Hint:Localize your strings file")
        }
    }
    
    func setLocale(_ langCode:String){
        
        kdefaults.set([langCode], forKey: "AppleLanguages")//replaces Locale.preferredLanguages
        kdefaults.synchronize()
        self.lprojBasePath =  getSelectedLocale()
    }
}
