//
//  GlobalVariables.swift
//  Wall of Analytics
//
//  Created by Mitra Sangroula on 8/15/16.
//  Copyright © 2016 Ensue. All rights reserved.
//

import Foundation
import UIKit


enum GlobalConstants {

   static let englishCode = "en"
   static let spanishCode = "es"
   static let hindiCode = "hi"
   static let gujratiCode = "gu"
   static let arabicCode = "ar"
   static let frenchCode = "fr"
   static let japaneseCode = "ja"
   static let germanCode = "de"
   static let portuguese = "pt"
   static let russian = "ru"

    

}


