//
//  CategoryIconVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 01/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

protocol CategoryIconVCDelegate {
    func selectedCategoryIcon(iconName:String)
}

class CategoryIconVC: UIViewController {

    
    var delegate: CategoryIconVCDelegate? = nil
    @IBOutlet weak var viewCenter: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!

    var arrCategoryIcon = NSMutableArray(){
        didSet {
            reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTransparentView))
//        self.view.addGestureRecognizer(tap)
        initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        lblTitle.backgroundColor = UIColor(hexString: getSelectedThemeColor())
     
    }
  
}
extension CategoryIconVC{
    func initialSetup() {
        arrCategoryIcon = ["icn_category_bank","icn_category_basketball","icn_category_bed","icn_category_beer","icn_category_bicycle","icn_category_boat","icn_category_book","icn_category_bus","icn_category_cake1","icn_category_cake2","icn_category_call_center","icn_category_car","icn_category_cloths","icn_category_computer","icn_category_cricket","icn_category_dress","icn_category_drinks","icn_category_dumbell","icn_category_education","icn_category_factory","icn_category_flight","icn_category_food","icn_category_football","icn_category_fuel","icn_category_fun","icn_category_gift","icn_category_glass","icn_category_headphone","icn_category_health","icn_category_home","icn_category_hotel","icn_category_label","icn_category_laptop","icn_category_mechanic","icn_category_mechanic_tools","icn_category_merchandise","icn_category_personal","icn_category_pets","icn_category_pizza","icn_category_plumber","icn_category_question","icn_category_restaurent","icn_category_restaurent2","icn_category_road","icn_category_rugby","icn_category_sandle","icn_category_school","icn_category_scooter","icn_category_shoes","icn_category_taxi","icn_category_telephone","icn_category_tickets","icn_category_tip","icn_category_train","icn_category_transport","icn_category_tshirt"]
    }
    
    func reloadData() {
        runOnMainThread {
            self.collectionView.reloadData()
        }
    }
    
    
    @objc func tapOnTransparentView() {
        //dissmissView()
  }
 
 func dissmissView(){
     self.dismiss(animated: true, completion: nil)
  }
    
}
//# MARK:- Extension: UICollection
extension CategoryIconVC : UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrCategoryIcon.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier.Cell.CategoryIconCVC, for: indexPath) as! CategoryIconCVC
       
        cell.viewCategory.backgroundColor = AppDelegate.sharedInstance.getCategoryColor(iconName: arrCategoryIcon[indexPath.row] as! String)
        
        if let cType = arrCategoryIcon[indexPath.row] as? String, let image = UIImage(named:AppDelegate.sharedInstance.getCategoryImage(iconName: cType)) {
            cell.imgCategory.image  = image
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil{
            delegate?.selectedCategoryIcon(iconName: arrCategoryIcon[indexPath.row] as! String)
             dissmissView()
        }
    }
}
