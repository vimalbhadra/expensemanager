//
//  AddCategoryVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 01/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

protocol AddCategoryVCDelegate {
    func refreshCategoryData()
}


class AddCategoryVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewCenter: UIView!
    @IBOutlet weak var txtCategoryName: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnSelecteCatory: UIButton!
    @IBOutlet weak var viewCategory: UIView!

     var delegate: AddCategoryVCDelegate? = nil
     var arrCategory = [CategoryManagment]()
     var categoryInfo: CategoryManagment = CategoryManagment()
     var isBoolEdit : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnTransparentView))
        self.view.addGestureRecognizer(tap)
        if isBoolEdit {
            categoryInfo.categoryId = arrCategory[0].categoryId
            lblTitle.text = Title.EditCategory
            txtCategoryName.text = arrCategory[0].categoryName
            txtCategoryName.resignFirstResponder()
            setButton(iconName: arrCategory[0].categoryIconName!)
        }else{
            lblTitle.text = AppDelegate.sharedInstance.LocalizedString(text: Title.AddCategory)
            btnSelecteCatory.tag = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblTitle.backgroundColor = UIColor(hexString: getSelectedThemeColor())
        btnSave.setTitle(AppDelegate.sharedInstance.LocalizedString(text: "Save"), for: .normal)
        btnCancel.setTitle(AppDelegate.sharedInstance.LocalizedString(text: "Cancel"), for: .normal)
        btnCancel.setTitleColor(UIColor(hexString: getSelectedThemeColor()), for: .normal)
        btnSave.setTitleColor(UIColor(hexString: getSelectedThemeColor()), for: .normal)
    }
  
}
extension AddCategoryVC{
    @objc func tapOnTransparentView() {
        dissmissView()
    }

    func dissmissView(){
        AppDelegate.sharedInstance.adscount = AppDelegate.sharedInstance.adscount + 1
         self.dismiss(animated: true, completion: nil)
    }
    
    func Validation()->Bool{
        let validation : Bool = true
        if var text = txtCategoryName.text, text.trim().count == 0 {
             APPDELEGATE.showToastMessage(Messages.categoryNameBlank, wantBottomSide: true, hideAutomatically: true)
            return false
        }else if btnSelecteCatory.tag == 0 {
            APPDELEGATE.showToastMessage(Messages.categoryiconsel, wantBottomSide: true, hideAutomatically: true)
            return false
        }else if var text = txtCategoryName.text,text.trim().count > 0{
            
            
            let results = arrCategory.filter { el in el.categoryName == txtCategoryName.text }
            if results.count > 0 {
                APPDELEGATE.showToastMessage(Messages.categoryExist, wantBottomSide: true, hideAutomatically: true)
                return false
            }
        }
        
        return validation
        
    }
    
    func EditValidation()->Bool{
        let validation : Bool = true
        if var text = txtCategoryName.text, text.trim().count == 0 {
            APPDELEGATE.showToastMessage(Messages.categoryNameBlank, wantBottomSide: true, hideAutomatically: true)
            return false
        }else if btnSelecteCatory.tag == 0 {
            APPDELEGATE.showToastMessage(Messages.categoryiconsel, wantBottomSide: true, hideAutomatically: true)
            return false
        }else if var text = txtCategoryName.text,text.trim().count > 0{
            categoryInfo.categoryName = txtCategoryName.text
            categoryInfo.categoryCount = "0"
            if let accountInfo = UserDefaultManager.getAccountInfo() {
                categoryInfo.categoryAccountId = accountInfo.id!
            }
            if CategoryManagment.updateCategory(categoryInfo: categoryInfo) {
                return validation
            }else{
                return false
            }
        }
        
        return validation
        
    }
    
    
    
    func setButton(iconName:String){
        btnSelecteCatory.tag = 1
        categoryInfo.categoryIconName = iconName
        btnSelecteCatory.setBackgroundImage(UIImage(named: AppDelegate.sharedInstance.getCategoryImage(iconName: iconName)), for: .normal)
        viewCategory.backgroundColor = AppDelegate.sharedInstance.getCategoryColor(iconName: iconName)
    }
}

extension AddCategoryVC {
    
    @IBAction func btnAddCategoryIconTapped(_ sender: UIButton) {
        let vc = loadVC(Identifier.Storyboard.SB_Category, strVCId:
             Identifier.ViewController.CategoryIconVC) as! CategoryIconVC
        vc.delegate = self
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func btnCancleTapped(_ sender: UIButton) {
        dissmissView()
    }
    
    @IBAction func btnSaveTapped(_ sender: UIButton) {
        
        
        if isBoolEdit {
            
            let validationCheck = EditValidation()
            if validationCheck {
                    if delegate != nil{
                         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                            self.delegate?.refreshCategoryData()
                            self.dissmissView()
                         })
                    }
            }
        }else{
            
            let validationCheck =  Validation()
            if validationCheck {
                categoryInfo.categoryName = txtCategoryName.text
                categoryInfo.categoryCount = "0"
                if let accountInfo = UserDefaultManager.getAccountInfo() {
                    categoryInfo.categoryAccountId = accountInfo.id!
                    
                }
                if CategoryManagment.addCategoryData(categoryInfo: categoryInfo){
                    if delegate != nil{
                        delegate?.refreshCategoryData()
                        dissmissView()
                    }
                }
            }
        }

  
      
    }
}
extension AddCategoryVC : CategoryIconVCDelegate {
    func selectedCategoryIcon(iconName: String) {
        setButton(iconName: iconName)
    }
}
extension AddCategoryVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var updatedText = ""
        if let text = textField.text, let textRange = Range(range, in: text) {
            updatedText = text.replacingCharacters(in: textRange,
                                                   with: string)
            if updatedText.count > 20{
                return false
            }
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
