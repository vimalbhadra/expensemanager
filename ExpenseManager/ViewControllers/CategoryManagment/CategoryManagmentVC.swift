//
//  CategoryManagmentVC.swift
//  ExpenseManager
//
//  Created by Uffizio iMac on 30/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import GoogleMobileAds
protocol CategoryViewDelegate {
    func selectedCategory(_ categoryInfo: CategoryManagment)
}

class CategoryManagmentVC: BaseVC {

    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var tblCategory: UITableView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var vwCategory: UIView!
    
    var delegate: CategoryViewDelegate? = nil
    var arrCategory = [CategoryManagment]()
    var arrsearchCategory = [CategoryManagment]()
    var isBoolEdit : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        adBannerView.adsbannerView.delegate = self

        initialSetup()
        getAllCategory()
    }
    override func viewWillAppear(_ animated: Bool) {
        showbannerAds()
        vwCategory.backgroundColor = UIColor(hexString: getSelectedThemeColor())
    }
  
    
}

extension  CategoryManagmentVC {
    func initialSetup() {
        
        if isBoolEdit {
             setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [], right_action: [], title: AppDelegate.sharedInstance.LocalizedString(text: Title.ManageCategory))
        }else{
            setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [], right_action: [], title: AppDelegate.sharedInstance.LocalizedString(text: Title.SelectCategory))
        }
        tblCategory.tableFooterView = UIView(frame: .zero)
    }
    
    func getAllCategory(){
        arrCategory.removeAll()
        arrsearchCategory.removeAll()
        if let accountInfo = UserDefaultManager.getAccountInfo() {
            arrCategory = CategoryManagment.getAllCategory(account_Id: accountInfo.id!)
            arrsearchCategory = arrCategory
             setData(data: arrCategory)
        }
    }

    //#MARK: SetData
    func setData(data:[CategoryManagment]) {
        searchbar.text = ""
        runOnMainThread {
            if self.lblMessage != nil, self.tblCategory != nil {
                self.lblMessage.isHidden = (self.arrCategory.count > 0) ? true : false
                self.tblCategory.isHidden = !self.lblMessage.isHidden
                self.tblCategory.reloadData()
            }
        }
    }
}
extension  CategoryManagmentVC {
    @IBAction func btnAddCategory(_ sender: UIButton) {
        let vc = loadVC(Identifier.Storyboard.SB_Category, strVCId:
            Identifier.ViewController.AddCategoryVC) as! AddCategoryVC
        vc.arrCategory = arrCategory
        vc.delegate = self
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
extension CategoryManagmentVC : AddCategoryVCDelegate{
    func refreshCategoryData() {
        getAllCategory()
    }
}
extension CategoryManagmentVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.CategoryTVC) as! CategoryTVC
        
        let objectData = arrCategory[indexPath.row]
        cell.lblCategoryName.text = objectData.categoryName
        cell.viewCategory.backgroundColor = AppDelegate.sharedInstance.getCategoryColor(iconName: objectData.categoryIconName!)
        if let cType = objectData.categoryIconName, let image = UIImage(named:AppDelegate.sharedInstance.getCategoryImage(iconName: cType)) {
            cell.imgCategory.image  = image
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isBoolEdit{
            let vc = loadVC(Identifier.Storyboard.SB_Category, strVCId:
                Identifier.ViewController.AddCategoryVC) as! AddCategoryVC
            vc.arrCategory = [self.arrCategory[indexPath.row]]
            vc.delegate = self
            vc.isBoolEdit = isBoolEdit
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
        }else{
            if delegate != nil {
                delegate?.selectedCategory(self.arrCategory[indexPath.row])
                self.navigationController?.popViewController(animated: true)
            }
        }
    
    }
}
extension CategoryManagmentVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count > 0 {
            self.arrCategory = arrsearchCategory.filter({ (object) -> Bool in
                if let strVNO = object.categoryName {
                    if strVNO.lowercased().contains(searchText.lowercased()) {
                        return true
                    }
                }
                return false
            })
        }
        else {
            self.arrCategory = arrsearchCategory
        }
        if self.arrCategory.count > 0 {
            self.lblMessage.isHidden = true
            self.tblCategory.isHidden = false
            self.tblCategory.reloadData()
        }else{
            self.lblMessage.isHidden = false
            self.tblCategory.isHidden = true
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchBar.text = ""
        arrCategory = arrsearchCategory
        setData(data: arrCategory)
    }
}
extension CategoryManagmentVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
