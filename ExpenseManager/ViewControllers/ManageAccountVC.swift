//
//  ManageAccountVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 23/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import GoogleMobileAds
enum AccountStatus: Int {
    case inactive = 0
    case active = 1
}

enum AccountStatusString: String {
    case inactive = "Inactive"
    case active = "Active"
}

class ManageAccountVC: BaseVC {
    
    @IBOutlet weak var tblViewAccounts: UITableView!
    @IBOutlet weak var viewAdd: UIView!

    var arrAccounts = [AccountInfo]() {
        didSet {
            reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adBannerView.adsbannerView.delegate = self
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (kdefaults.bool(forKey: kShowAds) && AppDelegate.sharedInstance.adscount > 3){
            interstitial = createAndLoadInterstitial()
            AppDelegate.sharedInstance.adscount = 0
        }
        getAllAccounts()
        showbannerAds()
        viewAdd.backgroundColor = UIColor(hexString: getSelectedThemeColor())
    }
}

extension ManageAccountVC {
    
    func initialSetup() {
        setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [], right_action: [], title: AppDelegate.sharedInstance.LocalizedString(text: Title.Accounts))
        tblViewAccounts.tableFooterView = UIView(frame: .zero)
    }
    
    func getTotalIncomeAndExpence(dashboardInfo: [DashboardInfo]) -> (Double, Double) {
        
        var incomeExpense = (0.0, 0.0)
        
        incomeExpense.0 = Double(dashboardInfo.filter({ (dashboardInfo) -> Bool in
            return dashboardInfo.transactionType == TransactionTypeName.INCOME.rawValue
        }).compactMap{ Double($0.transactionAmount ?? "0") }.reduce(0, +))
        
        incomeExpense.1 = Double(dashboardInfo.filter({ (dashboardInfo) -> Bool in
            return dashboardInfo.transactionType == TransactionTypeName.EXPENSE.rawValue
        }).compactMap{ Double($0.transactionAmount ?? "0") }.reduce(0, +))
        
        return incomeExpense
    }
    
    func getAllAccounts() {
        
        arrAccounts = AccountInfo.getAllAccount()
        arrAccounts = arrAccounts.enumerated().map { (i, obj) -> AccountInfo in
            
            var temp = obj
            let transactions = DashboardInfo.getAllTransacation(account_Id: arrAccounts[i].id ?? -1)
            let incomeExpense = self.getTotalIncomeAndExpence(dashboardInfo: transactions)
            temp.amount = incomeExpense.0 - incomeExpense.1
            return temp
        }
        
        if let accountInfo = UserDefaultManager.getAccountInfo() {
            setActiveStatus(accountID: accountInfo.id)
        }
    }
    
    func reloadData() {
        runOnMainThread {
            self.tblViewAccounts.reloadData()
        }
    }
    
    func getStatusInString(status: AccountStatus) -> String {
        
        switch status {
        case .active:
            return AccountStatusString.active.rawValue
        case .inactive:
            return AccountStatusString.inactive.rawValue
        }
    }
    
    func setActiveStatus(accountID: Int32?) {
        
        arrAccounts = arrAccounts.map { (object) -> AccountInfo in
            var temp = object
            temp.status = (temp.id == accountID) ? AccountStatus.active.rawValue : AccountStatus.inactive.rawValue
            if temp.id == accountID {
                UserDefaultManager.setAccountInfo(temp)
            }
            return temp
        }
    }
    
    func addAccountView(accountInfo: AccountInfo?) {
        
        if let addAccountView = instanceFromNib(nibName: Identifier.ViewController.AddAccountView) as? AddAccountView {
            addAccountView.delegate = self
            addAccountView.accountInfo = accountInfo
            addAccountView.showInAppWindow()
        }
    }
    
    @objc func switchValueChanged(_ sender: UISwitch) {
        setActiveStatus(accountID: arrAccounts[sender.tag].id)
    }
    
    @objc func btnAddTransactionTapped(_ sender: UIButton) {
        let viewController = loadVC(Identifier.Storyboard.SB_AddEditTransaction, strVCId: Identifier.ViewController.AddEditTransactionVC)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func btnEditAccountTapped(_ sender: UIButton) {
        addAccountView(accountInfo: arrAccounts[sender.tag])
    }
    
    @objc func btnDeleteAccountTapped(_ sender: UIButton) {
        
        if arrAccounts.count == 1 {
            APPDELEGATE.showAlert(title: nil, message: Messages.notDeleteAccount, actions: [Title.Alert.OK], completionHandler: nil)
        } else {
            APPDELEGATE.showAlert(title: Title.Alert.deleteAccount, message: Messages.deleteAccountPermission, actions: [Title.Alert.CANCEL, Title.Alert.OK]) { (index) in
                if index == 1 {
                    
                    if CategoryManagment.deleteAllCategory(categoryAccountID: self.arrAccounts[sender.tag].id!){
                        DLog("Delete All Caltegory Selected Account")
                    }
                    if TransactionInfo.deleteAllTransacation(accountID: self.arrAccounts[sender.tag].id!){
                        DLog("Delete All Transaction Selected Account")
                    }
                    if RecurrentInfo.deleteAllRecurrentTransacation(accountID: self.arrAccounts[sender.tag].id!){
                        DLog("Delete All Recurrent Transaction Selected Account")
                    }
                  
                    if AccountInfo.deleteAccount(accountInfo: self.arrAccounts[sender.tag]) {
                        self.arrAccounts.remove(at: sender.tag)
                        if !self.arrAccounts.contains(where: { (object) -> Bool in
                            return object.status == AccountStatus.active.rawValue
                        }) {
                            self.setActiveStatus(accountID: self.arrAccounts[0].id)
                        }
                    }
                }
            }
        }
    }
}

extension ManageAccountVC {
    
    @IBAction func btnAddAccountTapped(_ sender: UIButton) {
        addAccountView(accountInfo: nil)
    }
}

extension ManageAccountVC: AddAccountViewDelegate {
    
    func accountUpdated(_ accountInfo: AccountInfo) {
        
        if let index = arrAccounts.index(where: { (object) -> Bool in
            return object.id == accountInfo.id
        }) {
            arrAccounts[index] = accountInfo
        }
    }
    
    func refreshAccountData() {
        getAllAccounts()
    }
}

extension ManageAccountVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAccounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.ManageAccountTVC) as! ManageAccountTVC
        cell.switchStatus.addTarget(self, action: #selector(switchValueChanged), for: .valueChanged)
        cell.switchStatus.tag = indexPath.row
        cell.btnAddAccount.addTarget(self, action: #selector(btnAddTransactionTapped(_:)), for: .touchUpInside)
        cell.btnEditAccount.addTarget(self, action: #selector(btnEditAccountTapped), for: .touchUpInside)
        cell.btnDeleteAccount.addTarget(self, action: #selector(btnDeleteAccountTapped), for: .touchUpInside)
        
        cell.btnAddAccount.tag = indexPath.row
        cell.btnEditAccount.tag = indexPath.row
        cell.btnDeleteAccount.tag = indexPath.row
        
        let object = arrAccounts[indexPath.row]
        cell.lblAccountName.text = object.name ?? ""
        cell.lblStatus.text = getStatusInString(status: AccountStatus(rawValue: object.status ?? 0)!)
        cell.lblAmount.text = "\(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\((object.amount ?? 0.0))"
        if (object.amount ?? 0.0) >= 0.0 {
            cell.lblAmount.textColor = Color.textColor.colorIncome
        } else {
            cell.lblAmount.text = "-\(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\((fabs(object.amount ?? 0.0)))"
            cell.lblAmount.textColor = Color.textColor.colorExpense
        }
        
        if let status = object.status, status == AccountStatus.active.rawValue {
            cell.switchStatus.isOn = true
            cell.switchStatus.isEnabled = false
        } else {
            cell.switchStatus.isOn = false
            cell.switchStatus.isEnabled = true
        }
        return cell
    }
}
extension ManageAccountVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
