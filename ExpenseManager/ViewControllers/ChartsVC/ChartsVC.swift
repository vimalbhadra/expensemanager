//
//  ChartsVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 05/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import Charts
import GoogleMobileAds

extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}

class YAxisValueFormatter: NSObject, IAxisValueFormatter {

    let numFormatter: NumberFormatter

    override init() {
        numFormatter = NumberFormatter()
        numFormatter.minimumFractionDigits = 0
        numFormatter.maximumFractionDigits = 0
        numFormatter.negativeSuffix = ""
        numFormatter.positiveSuffix = ""
    }
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return numFormatter.string(from: NSNumber(integerLiteral: Int(value)))!
    }
}

class ChartsVC: BaseVC {

    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var bottomView: BottomView!
    @IBOutlet weak var btnDaily: UIButton!
    @IBOutlet weak var btnMonthly: UIButton!
    @IBOutlet weak var btnYearly: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var viewDaily: UIView!
    @IBOutlet weak var viewMonthly: UIView!
    @IBOutlet weak var lblYearMonthView: UILabel!
    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var viewDateFilter: UIView!


    @IBOutlet weak var const_viewSeparator_leading: NSLayoutConstraint!
    @IBOutlet weak var const_viewDaily_Height: NSLayoutConstraint!
    @IBOutlet weak var const_viewMonthly_Height: NSLayoutConstraint!


    
    var arrDashboard = [DashboardInfo]()
    var arrDuplicateDashboard = [DashboardInfo]()
    var strselectedDate : String = ""
    var strYear : String = ""
    var strMonth : String = ""
    var arrDates = [String]()
    var arrMonth = [String]()
    var arrYear = [String]()
    var selectedTag : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        adBannerView.adsbannerView.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        showbannerAds()
        if (kdefaults.bool(forKey: kShowAds) && AppDelegate.sharedInstance.adscount > 3){
            interstitial = createAndLoadInterstitial()
            AppDelegate.sharedInstance.adscount = 0
        }
        btnDaily.setTitle("\(AppDelegate.sharedInstance.LocalizedString(text: "Daily"))", for: .normal)
        btnMonthly.setTitle("\(AppDelegate.sharedInstance.LocalizedString(text: "Monthly"))", for: .normal)
        btnYearly.setTitle("\(AppDelegate.sharedInstance.LocalizedString(text: "Yearly"))", for: .normal)
        configureChart()
        setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [], right_action: [], title: AppDelegate.sharedInstance.LocalizedString(text: Title.Charts))
        if let accountInfo = UserDefaultManager.getAccountInfo() {

            getAllDashbaordData(accountID: accountInfo.id!)
            selectMenu(sender: btnDaily)
        }
        
        viewDateFilter.backgroundColor = UIColor(hexString: getSelectedThemeColor())
    }

}
extension ChartsVC {
    
    //MARK: - Configure Chart
    func configureChart(){
        
        barChart.maxVisibleCount = 7
        barChart.setVisibleXRangeMaximum(7)
        barChart.rightAxis.enabled = true
        barChart.rightAxis.axisMinimum = 0
        barChart.rightAxis.spaceTop = 0.15

        barChart.noDataText = ""
        barChart.xAxis.drawGridLinesEnabled = false
        barChart.leftAxis.drawGridLinesEnabled = false
        barChart.legend.enabled = true
        barChart.xAxis.labelPosition = .top
        barChart.leftAxis.axisMinimum = 0
        barChart.chartDescription?.enabled = false
        barChart.scaleYEnabled = true
        barChart.drawValueAboveBarEnabled = true
        barChart.doubleTapToZoomEnabled = true
        barChart.leftAxis.granularityEnabled = true
        barChart.xAxis.granularity = 1.0
        barChart.leftAxis.granularity = 1.0
        barChart.leftAxis.spaceTop = 0.15

        barChart.xAxis.labelTextColor = UIColor(hexString: "212121")
        barChart.xAxis.axisLineColor = UIColor(hexString: "e0e0e0")
        barChart.leftAxis.labelTextColor = UIColor(hexString: "212121")
        barChart.leftAxis.axisLineColor = UIColor(hexString: "e0e0e0")
        barChart.backgroundColor = UIColor.white
        barChart.gridBackgroundColor = UIColor(hexString: "eeeeee")
        barChart.drawGridBackgroundEnabled = true
        
    }
    
    func setDateLableDaily(selectedDate:Date,data:[DashboardInfo]){
            self.lblMonth.text = selectedDate.getStringFromDate(dateFormat: DateFormat.month)
            self.lblYear.text = selectedDate.getStringFromDate(dateFormat: DateFormat.year)
            sortingDataDaily(data: arrDuplicateDashboard, selectDate: selectedDate)
            setIncomeExpenseBalanceValue()
        
    }
    func setDateLableMonthly(selectedDate:Date,data:[DashboardInfo]){
        self.lblYearMonthView.text = selectedDate.getStringFromDate(dateFormat: DateFormat.year)
        sortingDataMonthly(data: arrDuplicateDashboard, selectDate: selectedDate)
        setIncomeExpenseBalanceValue()
    }
    
    
    func setIncomeExpenseBalanceValue(){
        bottomView.lblIncome.text = "\(AppDelegate.sharedInstance.LocalizedString(text: "INCOME")) \n \(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(getIncomevalue()))"
        bottomView.lblExpense.text = "\(AppDelegate.sharedInstance.LocalizedString(text: "EXPENSE")) \n \(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(getExpensevalue()))"
        let balance = getIncomevalue() - getExpensevalue()
        bottomView.lblBalance.text = "\(AppDelegate.sharedInstance.LocalizedString(text: "BALANCE")) \n \(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(balance))"
        bottomView.lblIncome.textColor = Color.textColor.colorIncome
        bottomView.lblExpense.textColor = Color.textColor.colorExpense
    }
    func getIncomevalue()-> Double{
        
        var totalIncome: Double = 0
        _ = self.arrDashboard.filter { (object) -> Bool in
            if let strType = object.transactionType?.lowercased(), strType == "income" {
                if totalIncome == 0 {
                    totalIncome = Double(object.transactionAmount ?? "")!
                } else {
                    totalIncome = totalIncome +  Double(object.transactionAmount ?? "")!
                }
                return true
            }
            return false
        }
        
        return (totalIncome)
        
    }
    func getExpensevalue()-> Double{
        
        var totalExpense: Double = 0
        _ = self.arrDashboard.filter { (object) -> Bool in
            if let strType = object.transactionType?.lowercased(), strType == "expense" {
                if totalExpense == 0 {
                    totalExpense = Double(object.transactionAmount ?? "")!
                } else {
                    totalExpense = totalExpense +  Double(object.transactionAmount ?? "")!
                }
                return true
            }
            return false
        }
        
        return (totalExpense)
        
    }
    
    
    func getAllDashbaordData(accountID:Int32){
        
        arrDuplicateDashboard.removeAll()
        arrDuplicateDashboard = DashboardInfo.getAllTransacation(account_Id: accountID)
        
      
    }
    
    func sortingDataDaily(data:[DashboardInfo],selectDate:Date){
        arrDashboard.removeAll()
        arrDates.removeAll()
        barChart.clear()
        configureChart()
        data.enumerated().forEach { (index,object) in
            
            let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: object.transactionDate!)
            let tempStrDate = getStringFromDate(dateFormat: DateFormat.yearmonth, date: tempDate!)
            
            if  tempStrDate == strselectedDate{
                arrDashboard.append(object)
            }
        }
        
        arrDates = getAllDates(month: Int(selectDate.getStringFromDate(dateFormat: DateFormat.monthmm))!, year: Int(selectDate.getStringFromDate(dateFormat: DateFormat.year))!)
        DLog("arrDates:\(arrDates)")
        setChartDataDaily(data: arrDashboard, arrDates: arrDates)
        
    }
    func sortingDataMonthly(data:[DashboardInfo],selectDate:Date){
        arrDashboard.removeAll()
        arrMonth.removeAll()
        barChart.clear()
        configureChart()
        data.enumerated().forEach { (index,object) in
            
            let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: object.transactionDate!)
            let tempStrDate = getStringFromDate(dateFormat: DateFormat.year, date: tempDate!)
            
            if  tempStrDate == getStringFromDate(dateFormat: DateFormat.year, date: selectDate){
                arrDashboard.append(object)
            }
        }
        
        arrMonth = getAllMonthName(selectedDate: selectDate)
        DLog("arrMonths:\(arrMonth)")
        setChartDataMonth(data: arrDashboard, arrMonths: arrMonth)
        
    }
    //#MARK: SetData
    
    func setChartDataDaily(data:[DashboardInfo],arrDates:[String]) {
        
        var dataEntryExpense : [BarChartDataEntry] = []
        var dataEntryIncome : [BarChartDataEntry] = []

        for (index, dates) in arrDates.enumerated() {
            
            var expense = 0.0
            var income = 0.0
            
            for (_,object) in data.enumerated() {
                
                let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: object.transactionDate!)
                let compareDate = getStringFromDate(dateFormat: DateFormat.ddmmyyyy, date: tempDate!)
                
                if object.transactionType!.lowercased() == "expense" && compareDate == dates{
                    expense = expense + Double(object.transactionAmount!)!
                }else  if object.transactionType!.lowercased() == "income" && compareDate == dates {
                     income = income + Double(object.transactionAmount!)!
                }
               
            }
            dataEntryExpense.append(BarChartDataEntry(x: Double(index), yValues: [expense]))
            dataEntryIncome.append(BarChartDataEntry(x: Double(index), yValues: [income]))
        }
     
        setupChart(dataEntryExpense: dataEntryExpense, dataEntryIncome: dataEntryIncome, index: arrDates)

    }
    
    func setChartDataMonth(data:[DashboardInfo],arrMonths:[String]) {
        
        var dataEntryExpense : [BarChartDataEntry] = []
        var dataEntryIncome : [BarChartDataEntry] = []
        
        for (index, dates) in arrMonths.enumerated() {
            
            var expense = 0.0
            var income = 0.0
            
            for (_,object) in data.enumerated() {
                
                let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: object.transactionDate!)
                let compareDate = getStringFromDate(dateFormat: DateFormat.monthNameyear, date: tempDate!)
                
                if object.transactionType!.lowercased() == "expense" && compareDate == dates{
                    expense = expense + Double(object.transactionAmount!)!
                }else  if object.transactionType!.lowercased() == "income" && compareDate == dates {
                    income = income + Double(object.transactionAmount!)!
                }
                
            }
            dataEntryExpense.append(BarChartDataEntry(x: Double(index), yValues: [expense]))
            dataEntryIncome.append(BarChartDataEntry(x: Double(index), yValues: [income]))
        }
        
        setupChart(dataEntryExpense: dataEntryExpense, dataEntryIncome: dataEntryIncome, index: arrMonths)
        
    }
    
    func setChartDataYearly(data:[DashboardInfo],arrYear:[String]) {
        
        var dataEntryExpense : [BarChartDataEntry] = []
        var dataEntryIncome : [BarChartDataEntry] = []
        
        for (index, dates) in arrYear.enumerated() {
            
            var expense = 0.0
            var income = 0.0
            
            for (_,object) in data.enumerated() {
                
                let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: object.transactionDate!)
                let compareDate = getStringFromDate(dateFormat: DateFormat.year, date: tempDate!)
                
                if object.transactionType!.lowercased() == "expense" && compareDate == dates{
                    expense = expense + Double(object.transactionAmount!)!
                }else  if object.transactionType!.lowercased() == "income" && compareDate == dates {
                    income = income + Double(object.transactionAmount!)!
                }
                
            }
            dataEntryExpense.append(BarChartDataEntry(x: Double(index), yValues: [expense]))
            dataEntryIncome.append(BarChartDataEntry(x: Double(index), yValues: [income]))
        }
        
        setupChart(dataEntryExpense: dataEntryExpense, dataEntryIncome: dataEntryIncome, index: arrYear)
        
    }
    
    //MARK: - SETUP CHART
    func setupChart(dataEntryExpense:[ChartDataEntry],dataEntryIncome:[ChartDataEntry],index:[String]){
    
     
        
        let dataSetExpense = BarChartDataSet(values: dataEntryExpense, label: AppDelegate.sharedInstance.LocalizedString(text: "EXPENSE"))
        dataSetExpense.setColor(Color.textColor.colorExpense)
        dataSetExpense.drawValuesEnabled = true

        let dataSetIncome = BarChartDataSet(values: dataEntryIncome, label: AppDelegate.sharedInstance.LocalizedString(text: "INCOME"))
        dataSetIncome.setColor(Color.textColor.colorIncome)
        dataSetIncome.drawValuesEnabled = true
        
    
         var dataSets : [BarChartDataSet] = []
        dataSets.append(dataSetExpense)
        dataSets.append(dataSetIncome)
        
        
        let data = BarChartData(dataSets: dataSets)
        let xaxis : XAxis = XAxis()
        
      
        xaxis.valueFormatter = IndexAxisValueFormatter(values: index)
        barChart.xAxis.drawGridLinesEnabled = true
        barChart.xAxis.granularityEnabled = true
        barChart.xAxis.centerAxisLabelsEnabled = true
        barChart.xAxis.labelPosition = .top
        barChart.xAxis.valueFormatter = xaxis.valueFormatter

        let yaxis : YAxis = YAxis()
        yaxis.valueFormatter = YAxisValueFormatter()
        yaxis.drawLimitLinesBehindDataEnabled = false
     
        let groupSpace = 0.3
        let barSpace = 0.05
        let barWidth = 0.3
        
        let groupCount = index.count
        
        data.barWidth = barWidth
        barChart.xAxis.axisMinimum = 0.0
        
        let gg = data.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        print("Groupspace: \(gg)")
        barChart.xAxis.axisMaximum = Double(0.0) + gg * Double(groupCount)
        data.groupBars(fromX: Double(0.0), groupSpace: groupSpace, barSpace: barSpace)
        barChart.data = data
        barChart.fitBars = true
        barChart.notifyDataSetChanged()
        barChart.setNeedsDisplay()

    }

    
    func selectMenu(sender: UIButton) {
        
        let views: [UIButton] = [btnDaily, btnMonthly, btnYearly]
        views.enumerated().forEach { (index, view) in
            view.isSelected = (view == sender) ? true : false
        }
        
        selectedTag = sender.tag
        
        if selectedTag == 0{
            viewDaily.isHidden = false
            viewMonthly.isHidden = true
            const_viewDaily_Height.constant = 50
            const_viewMonthly_Height.constant = 50
            strYear = Date().getStringFromDate(dateFormat: DateFormat.year)
            strMonth =  Date().getStringFromDate(dateFormat: DateFormat.monthmm)
            strselectedDate = strYear + "-" + strMonth
            
            let tempDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            setDateLableDaily(selectedDate: tempDate!, data: arrDuplicateDashboard)
            
        }else if selectedTag == 1 {
            viewDaily.isHidden = true
            viewMonthly.isHidden = false
            const_viewDaily_Height.constant = 50
            const_viewMonthly_Height.constant = 50
            let tempDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            setDateLableMonthly(selectedDate: tempDate!, data: arrDuplicateDashboard)
        }else{
            viewDaily.isHidden = true
            viewMonthly.isHidden = true
            const_viewDaily_Height.constant = 0
            const_viewMonthly_Height.constant = 0

            arrYear.removeAll()
            arrDashboard.removeAll()
            barChart.clear()
            configureChart()
            arrYear = getAllYear(data: arrDuplicateDashboard)
            arrDashboard = arrDuplicateDashboard
            setIncomeExpenseBalanceValue()
            setChartDataYearly(data: arrDashboard, arrYear: arrYear)
        }
        
        runOnMainThread {
            self.const_viewSeparator_leading.constant = (sender.superview?.frame.origin.x)!
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func getAllDates(month: Int, year: Int) -> [String] {
        let dateComponents = DateComponents(year: year, month: month)
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        var temparrDates = [String]()
        for day in 1...numDays {
            var dateString = ""
            if day < 10 {
                dateString = "0\(day)-0\(month)-\(year)"
            }else{
                if month < 10{
                    dateString = "\(day)-0\(month)-\(year)"
                }else{
                    dateString = "\(day)-\(month)-\(year)"
                }
                
            }
            temparrDates.append(dateString)
        }
        
        return temparrDates
    }
    func getAllMonthName(selectedDate:Date)->[String]{
        let dateFormatter = DateFormatter()
        
        let currentCalendar = Calendar.current
        var yearComponents: DateComponents? = currentCalendar.dateComponents([.year], from: selectedDate)
        let currentYear = Int(yearComponents!.year!)
        var temparrMonth = [String]()
        for months in 0..<12 {
            temparrMonth.append("\(dateFormatter.shortMonthSymbols[months]) \(currentYear)")
        }
        
        
        return temparrMonth
    }
    
    func getAllYear(data:[DashboardInfo])->[String]{
        var temparrYear = [String]()
        
        
        
        for (_, object) in data.enumerated() {
            
            let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: object.transactionDate!)
            let compareDate = getStringFromDate(dateFormat: DateFormat.year, date: tempDate!)
            temparrYear.append(compareDate)
        }

        temparrYear.removeDuplicates()
        temparrYear.sort { $0 < $1 }
        
        return temparrYear
    }
    
  
    
   
}

// MARK: axisFormatDelegate
//extension ChartsVC: IAxisValueFormatter {
    
//    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
//
//        if selectedTag == 0 {
//            return  arrDates[Int(value)]
//        }else if selectedTag == 1{
//            return  arrMonth[Int(value)]
//        }else{
//            return  ""
//        }
//
//
//    }
    
//}
extension ChartsVC {
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        selectMenu(sender: sender)
    }
 
    @IBAction func btnTappedMonthNextPrv(_ sender: UIButton) {
        
        var nextMonth : Date? = nil
        if sender.tag == 1000{
            //prev month click
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            nextMonth = Calendar.current.date(byAdding: .month, value: -1, to: temDate!)
            strMonth = getStringFromDate(dateFormat: DateFormat.monthmm, date: nextMonth!)
            
        }else{
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: temDate!)
            strMonth = getStringFromDate(dateFormat: DateFormat.monthmm, date: nextMonth!)
            
        }
        strselectedDate = strYear + "-" + strMonth
        let tempDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
        
        setDateLableDaily(selectedDate: tempDate!, data: arrDuplicateDashboard)
        
    }
    @IBAction func btnTappedYearNextPrv(_ sender: UIButton) {
        
        var nextYear : Date? = nil
        
        if sender.tag == 2000{
            //prev year click
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            nextYear = Calendar.current.date(byAdding: .year, value: -1, to: temDate!)
            strYear = getStringFromDate(dateFormat: DateFormat.year, date: nextYear!)
        }else{
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            nextYear = Calendar.current.date(byAdding: .year, value: 1, to: temDate!)
            strYear = getStringFromDate(dateFormat: DateFormat.year, date: nextYear!)
        }
        strselectedDate = strYear + "-" + strMonth
        let tempDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
        setDateLableDaily(selectedDate: tempDate!, data: arrDuplicateDashboard)

    }
    @IBAction func btnTappedMonthOfYearNextPrv(_ sender: UIButton) {
        
        var nextYear : Date? = nil
        
        if sender.tag == 3000{
            //prev year click
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            nextYear = Calendar.current.date(byAdding: .year, value: -1, to: temDate!)
            strYear = getStringFromDate(dateFormat: DateFormat.year, date: nextYear!)
        }else{
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            nextYear = Calendar.current.date(byAdding: .year, value: 1, to: temDate!)
            strYear = getStringFromDate(dateFormat: DateFormat.year, date: nextYear!)
        }
        strselectedDate = strYear + "-" + strMonth
        let tempDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
        
        setDateLableMonthly(selectedDate: tempDate!, data: arrDuplicateDashboard)
    }
  
}

extension ChartsVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
