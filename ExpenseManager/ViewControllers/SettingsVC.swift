//
//  SettingsVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 01/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import UserNotifications
import GoogleMobileAds
import MessageUI

struct Notification: Codable {
    
    var isOn: Bool
    var time: String
    
    enum CodingKeys: String, CodingKey {
        case isOn = "isOn"
        case time = "time"
    }
    
    init(isOn: Bool, time: String) {
        self.isOn = isOn
        self.time = time
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isOn = (try values.decodeIfPresent(Bool.self, forKey: .isOn))!
        time = try values.decodeIfPresent(String.self, forKey: .time)!
    }
}

struct SectionTag {
    
    static let Customize = 0
    static let Notification = 1
    static let TouchID = 2
    static let SupportUs = 3
    static let Other = 4
}

struct RowTag {
    static let ThemeColor = 0
    static let ChangeLanguage = 1
    static let Notification = 0
    static let NotificationTime = 1
    static let TouchID = 0
    static let RateApp = 0
    static let ShareFriends = 1
    static let RecommendedApps = 2
    static let Feedback = 0
    static let PrivacyPolicy = 1
    static let About = 2
    static let AppVersion = 3
}

struct SettingsRowData {
    
    var title: String
    var value: String?
    var value2: String?
    var switchStatus: Bool?
    var identifierVC: String?
    var storyboardID: String?
    var isAddSubView: Bool?
    
    init(title: String, value: String? = nil, value2: String? = nil, switchStatus: Bool? = nil, identifierVC: String? = nil, storyboardID: String? = nil, isAddSubView: Bool? = nil) {
        self.title = title
        self.value = value
        self.value2 = value2
        self.switchStatus = switchStatus
        self.identifierVC = identifierVC
        self.storyboardID = storyboardID
        self.isAddSubView = isAddSubView
    }
}

struct SettingsSectionData {
    var title: String
    var arrSettingsRowData: [SettingsRowData]
}

class SettingsVC: BaseVC {
    
    @IBOutlet weak var tblViewSettings: UITableView!

    var arrSettingsSectionData = [SettingsSectionData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        adBannerView.adsbannerView.delegate = self
        showbannerAds()
        initialSetup()
        fillArrayWithData()
        self.view.layoutSubviews()
    }
}

extension SettingsVC {
    
    func initialSetup() {
        setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [], right_action: [], title: AppDelegate.sharedInstance.LocalizedString(text: Title.Settings))
        tblViewSettings.tableFooterView = UIView(frame: .zero)
    }
    
    func fillArrayWithData() {
        
        arrSettingsSectionData = [
            
            SettingsSectionData(title: "Customize", arrSettingsRowData: [
                SettingsRowData(title: "Theme color", value: "Change app theme color", identifierVC: Identifier.ViewController.ThemeColorVC, storyboardID: Identifier.Storyboard.Setting, isAddSubView: true),
                SettingsRowData(title: "Change language", value: kdefaults.value(forKey: kLanguage) as? String ?? "English" , identifierVC: Identifier.ViewController.SelectLanguageVC, storyboardID: Identifier.Storyboard.Main, isAddSubView: false)]),
            
            SettingsSectionData(title: "Notification", arrSettingsRowData: [
                SettingsRowData(title: "Notification", switchStatus: UserDefaultManager.getNotification()?.isOn ?? true)]),
            
            SettingsSectionData(title: "Touch ID", arrSettingsRowData: [
                SettingsRowData(title: "Touch ID", value: "Use your own Touch ID to open app", switchStatus: (UserDefaultManager.getData(forKey: Parameters.UserDefault.TouchID) as? Bool) ?? false)]),
            
            SettingsSectionData(title: "Support us", arrSettingsRowData: [
                SettingsRowData(title: "Rate app", value: "If you love our app, please take a moment to rate and review it in the App Store"),
                SettingsRowData(title: "Share with friends", value: "Support us by sharing the app with friends"),
                SettingsRowData(title: "Recommended apps", value: "If you like this app, don't forget to check out our other apps")]),
            
            SettingsSectionData(title: "Other", arrSettingsRowData: [
                SettingsRowData(title: "Feedback", value: "If you have any suggestions, questions or a problem, don't hesitate to contact us"),
                SettingsRowData(title: "Privacy Policy",identifierVC: Identifier.ViewController.PrivacyVC, storyboardID: Identifier.Storyboard.Setting),
                SettingsRowData(title: "About"),
                SettingsRowData(title: "App version", value: ConstantValues.APP_VERSION)])
        ]
        
        if UserDefaultManager.getNotification()?.isOn ?? true {
            
            arrSettingsSectionData[SectionTag.Notification].arrSettingsRowData.append(SettingsRowData(title: "Notification Time", value2: UserDefaultManager.getNotification()?.time ?? ConstantValues.DEFAULT_NOTIFICATION_TIME, identifierVC: Identifier.ViewController.DatePickerView, isAddSubView: true))
        }
        reloadData()
    }
    
    func reloadData() {
        runOnMainThread {
            self.tblViewSettings.reloadData()
        }
    }
    
    func addRemoveRow(isAdd: Bool, indexPath: IndexPath) {
        
        runOnMainThread {
            self.tblViewSettings.beginUpdates()
            if isAdd {
                self.tblViewSettings.insertRows(at: [indexPath], with: .left)
                self.arrSettingsSectionData[indexPath.section].arrSettingsRowData.insert(SettingsRowData(title: "Notification Time", value2: UserDefaultManager.getNotification()?.time ?? ConstantValues.DEFAULT_NOTIFICATION_TIME), at: indexPath.row)
            } else {
                self.tblViewSettings.deleteRows(at: [indexPath], with: .left)
                self.arrSettingsSectionData[indexPath.section].arrSettingsRowData.remove(at: indexPath.row)
            }
            self.tblViewSettings.endUpdates()
        }
    }
}

extension SettingsVC: ThemeColorVCDelegate {
    
    func savedThemeColorSuccessful() {
        self.viewWillAppear(true)
    }
}

extension SettingsVC: DatePickerViewDelegate {
    
    func getSelectedDate(date: Date) {
        
        let time = date.getStringFromDate(dateFormat: DateFormat.hoursMinutes)
        if var notification = UserDefaultManager.getNotification() {
            notification.time = time
            UserDefaultManager.setNotification(notification)
        }
        
        arrSettingsSectionData[SectionTag.Notification].arrSettingsRowData[RowTag.NotificationTime].value2 = time
        self.tblViewSettings.reloadRows(at: [IndexPath(row: RowTag.NotificationTime, section: SectionTag.Notification)], with: .none)
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        APPDELEGATE.scheduleLocalNotification()
    }
    
    func getDates(dates: (Date, Date, Bool)) {
        
    }
}

extension SettingsVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSettingsSectionData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettingsSectionData[section].arrSettingsRowData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.SettingsTVC) as! SettingsTVC
        let object = arrSettingsSectionData[indexPath.section].arrSettingsRowData[indexPath.row]
        cell.lblTitle.text = object.title
        cell.lblValue.text = object.value
        cell.switchNotification.isHidden = true
        cell.lblTime.isHidden = true
        
        if object.switchStatus != nil {
            cell.switchNotification.isHidden = false
            cell.switchNotification.isOn = object.switchStatus!
        }
        
        if object.value2 != nil {
            cell.lblTime.isHidden = false
        }
        
        cell.lblTime.text = object.value2
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        view.backgroundColor = .white
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.width, height: 40))
        label.font = FONT(FontName.roboto_bold.rawValue, size: FontSize.title)
        label.textAlignment = .left
        label.text = arrSettingsSectionData[section].title
        label.textColor = UIColor(hexString: getSelectedThemeColor())
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = arrSettingsSectionData[indexPath.section].arrSettingsRowData[indexPath.row]
        
        if indexPath.section == SectionTag.Customize {
            if indexPath.row == RowTag.ThemeColor {
                if let identifierVC = object.identifierVC, let storyboardID = object.storyboardID {
                    let vc = loadVC(storyboardID, strVCId: identifierVC) as! ThemeColorVC
                    vc.delegate = self
                    if let isAddSubView = object.isAddSubView, isAddSubView {
                        vc.showInAppWindow()
                    }
                }
            } else if indexPath.row == RowTag.ChangeLanguage {
                if let identifierVC = object.identifierVC, let storyboardID = object.storyboardID {
                    let vc = loadVC(storyboardID, strVCId: identifierVC)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        } else if indexPath.section == SectionTag.Notification {
            
            if indexPath.row == RowTag.Notification {
                
                arrSettingsSectionData[indexPath.section].arrSettingsRowData[indexPath.row].switchStatus = !(arrSettingsSectionData[indexPath.section].arrSettingsRowData[indexPath.row].switchStatus ?? false)
                
                self.tblViewSettings.reloadRows(at: [indexPath], with: .none)
                if var notification = UserDefaultManager.getNotification() {
                    notification.isOn = arrSettingsSectionData[indexPath.section].arrSettingsRowData[indexPath.row].switchStatus!
                    UserDefaultManager.setNotification(notification)
                }
                
                if arrSettingsSectionData[indexPath.section].arrSettingsRowData[indexPath.row].switchStatus ?? true {
                    addRemoveRow(isAdd: true, indexPath: IndexPath(row: RowTag.NotificationTime, section: indexPath.section))
                    APPDELEGATE.scheduleLocalNotification()
                } else {
                    addRemoveRow(isAdd: false, indexPath: IndexPath(row: RowTag.NotificationTime, section: indexPath.section))
                    UNUserNotificationCenter.current().removeAllPendingNotificationRequests()

                }
                
            } else if indexPath.row == RowTag.NotificationTime {
                
                let datePickerView = DatePickerView.instanceFromNib()
                datePickerView.delegate = self
                datePickerView.datePickerMode = .time
                datePickerView.selectedDate = (object.value2 ?? "").getDateFromString(dateFormat: DateFormat.hoursMinutes)
                datePickerView.showInAppWindow()
            }
            
        } else if indexPath.section == SectionTag.TouchID {
            
            if indexPath.row == RowTag.TouchID {
                
                arrSettingsSectionData[indexPath.section].arrSettingsRowData[indexPath.row].switchStatus = !(arrSettingsSectionData[indexPath.section].arrSettingsRowData[indexPath.row].switchStatus ?? false)
                
                self.tblViewSettings.reloadRows(at: [indexPath], with: .none)
                UserDefaultManager.setData(value: arrSettingsSectionData[indexPath.section].arrSettingsRowData[indexPath.row].switchStatus!, forKey: Parameters.UserDefault.TouchID)
            }
        }else if indexPath.section == SectionTag.SupportUs{
             if indexPath.row == RowTag.RateApp{
                rateApp()
             }else if indexPath.row == RowTag.ShareFriends{
                shareApp()
            }
        }else if indexPath.section == SectionTag.Other{
            if indexPath.row == RowTag.Feedback{
                let picker = MFMailComposeViewController()
                picker.mailComposeDelegate = self
                picker.setSubject("Expense Manager Feedback")
                picker.setToRecipients(["gwersdoerfer@gmail.com"])
                present(picker, animated: true, completion: nil)
            }else if indexPath.row == RowTag.PrivacyPolicy{
                if let identifierVC = object.identifierVC, let storyboardID = object.storyboardID {
                    let vc = loadVC(storyboardID, strVCId: identifierVC)
                    self.navigationController?.pushViewController(vc, animated: true)
                }

            } else if indexPath.row == RowTag.About {
                
                if let aboutUSView = instanceFromNib(nibName: "AboutUSView") as? AboutUSView {
                    aboutUSView.showInAppWindow()
                if let identifierVC = object.identifierVC, let storyboardID = object.storyboardID {
                    let vc = loadVC(storyboardID, strVCId: identifierVC)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
             }
            }
        }
    }
}
extension SettingsVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
extension SettingsVC:MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}
