//
//  AnalyticsVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 12/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import GoogleMobileAds
enum PreviousNext: Int {
    case previousDay = 0
    case nextDay
    case previousMonth
    case nextMonth
}

enum MenuType: Int {
    case daily = 0
    case monthly
    case custom
}

enum PaymentTypeName: String {
    case All = "All"
    case CASH = "CASH"
    case CHEQUE = "CHEQUE"
    case BANK_TRANSFER = "BANK TRANSFER"
    case CREDIT_CARD = "CREDIT CARD"
    case DEBIT_CARD = "DEBIT CARD"
}

enum PaymentTypeTag: Int {
    case All = 6001
    case CASH
    case CHEQUE
    case BANK_TRANSFER
    case CREDIT_CARD
    case DEBIT_CARD
}

func getPaymentTagFromName(paymentTypeName: PaymentTypeName) -> Int {
    
    switch paymentTypeName {
    case .All:
        return PaymentTypeTag.All.rawValue
    case .CASH:
        return PaymentTypeTag.CASH.rawValue
    case .CHEQUE:
        return PaymentTypeTag.CHEQUE.rawValue
    case .BANK_TRANSFER:
        return PaymentTypeTag.BANK_TRANSFER.rawValue
    case .CREDIT_CARD:
        return PaymentTypeTag.CREDIT_CARD.rawValue
    case .DEBIT_CARD:
        return PaymentTypeTag.DEBIT_CARD.rawValue
    }
}

struct AnalyticsSectionData {
    var categoryImageName: String
    var categoryName: String
    var transactionCategoryID: Int32
    var noOfTransaction: String
    var income: String
    var expense: String
    var arrRowData: [DashboardInfo]
    var isExpand: Bool
}

struct AnalyticsData {
    
    var displayDate: String
    var strSearch: String
    var arrSectionData: [AnalyticsSectionData]
    var displayDateFormat: String
    var FilterDateFormat: String
    var startDate: String?
    var endDate: String?
    var previous: PreviousNext?
    var next: PreviousNext?
    
    init(displayDate: String, strSearch: String, arrSectionData: [AnalyticsSectionData], displayDateFormat: String, FilterDateFormat: String, startDate: String? = nil, endDate: String? = nil, previous: PreviousNext? = nil, next: PreviousNext? = nil) {
        self.displayDate = displayDate
        self.strSearch = strSearch
        self.arrSectionData = arrSectionData
        self.displayDateFormat = displayDateFormat
        self.FilterDateFormat = FilterDateFormat
        self.startDate = startDate
        self.endDate = endDate
        self.previous = previous
        self.next = next
    }
}

class AnalyticsVC: BaseVC {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnDaily: UIButton!
    @IBOutlet weak var btnMonthly: UIButton!
    @IBOutlet weak var btnCustom: UIButton!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var const_viewSeparator_leading: NSLayoutConstraint!
    @IBOutlet weak var viewDateFilter: UIView!

    var arrPaymentType = [
        PaymentType(titleLeft: (PaymentTypeName.All.rawValue, true, true), titleRight: (PaymentTypeName.CASH.rawValue, false)),
        PaymentType(titleLeft: (PaymentTypeName.CHEQUE.rawValue, false, false), titleRight: (PaymentTypeName.BANK_TRANSFER.rawValue, false)),
        PaymentType(titleLeft: (PaymentTypeName.CREDIT_CARD.rawValue, false, false), titleRight: (PaymentTypeName.DEBIT_CARD.rawValue, false))
        ] {
        didSet {
            
        }
    }
    
    var arrAnalyticsData = [AnalyticsData]() {
        didSet {
            reloadData()
        }
    }
    var arrSearchAnalyticsData = [AnalyticsData]()
    var selectedIndex: Int = MenuType.daily.rawValue
    var arrAllDashboardInfo = [DashboardInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adBannerView.adsbannerView.delegate = self
        initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        showbannerAds()
        if (kdefaults.bool(forKey: kShowAds) && AppDelegate.sharedInstance.adscount > 3){
            interstitial = createAndLoadInterstitial()
            AppDelegate.sharedInstance.adscount = 0
        }
        viewDateFilter.backgroundColor = UIColor(hexString: getSelectedThemeColor())
    }
}

extension AnalyticsVC {
    
    func filterDataBasedOnDate(filterDate: String, displayDate: String) {
        
        let arrFilterData = arrAllDashboardInfo.filter { (object) -> Bool in
            
            var result = false
            if let startDate = arrAnalyticsData[selectedIndex].startDate, let endDate = arrAnalyticsData[selectedIndex].endDate {
                result = (object.transactionDate ?? "" >= startDate && object.transactionDate ?? "" <= endDate)
            } else {
                result = object.transactionDate?.contains(filterDate) ?? false
            }
            
            if result {
                
                result = arrPaymentType.contains(where: { (paymentType) -> Bool in
                    
                    if paymentType.titleLeft.2, paymentType.titleLeft.1 {
                        return result
                    } else if paymentType.titleLeft.1, paymentType.titleLeft.0.lowercased() == object.transactionPayType?.lowercased() {
                        return true
                    } else if paymentType.titleRight.1, paymentType.titleRight.0.lowercased() == object.transactionPayType?.lowercased() {
                        return true
                    }
                    
                    return false
                })
            }
            
            return result
        }
        
        var analyticsData = arrAnalyticsData[selectedIndex]
        analyticsData.arrSectionData = []
        analyticsData.displayDate = displayDate
        
        arrFilterData.enumerated().forEach { (index, object) in
            if let index = analyticsData.arrSectionData.index(where: { (temp) -> Bool in
                return temp.transactionCategoryID == object.transactionCatId
            }) {
                analyticsData.arrSectionData[index].arrRowData.append(object)
            } else {
                analyticsData.arrSectionData.append(AnalyticsSectionData(categoryImageName: object.categoryIconName ?? "", categoryName: object.categoryName ?? "", transactionCategoryID: object.transactionCatId!, noOfTransaction: "1", income: "0", expense: "0", arrRowData: [object], isExpand: false))
            }
        }
        
        analyticsData.arrSectionData = analyticsData.arrSectionData.map { (object) -> AnalyticsSectionData in
            
            var temp = object
            temp.noOfTransaction = String(object.arrRowData.count)
            
            temp.income = String(temp.arrRowData.filter({ (dashboardInfo) -> Bool in
                return dashboardInfo.transactionType == TransactionTypeName.INCOME.rawValue
            }).compactMap{ Double($0.transactionAmount ?? "0") }.reduce(0, +))

            temp.expense = String(temp.arrRowData.filter({ (dashboardInfo) -> Bool in
                return dashboardInfo.transactionType == TransactionTypeName.EXPENSE.rawValue
            }).compactMap{ Double($0.transactionAmount ?? "0") }.reduce(0, +))
            
            return temp
        }
        
        arrAnalyticsData[selectedIndex] = analyticsData
    }
    
    func initialSetup() {
        
        view.layoutIfNeeded()
        btnDaily.setTitle("\(AppDelegate.sharedInstance.LocalizedString(text: "Daily"))", for: .normal)
        btnMonthly.setTitle("\(AppDelegate.sharedInstance.LocalizedString(text: "Monthly"))", for: .normal)
        btnCustom.setTitle("\(AppDelegate.sharedInstance.LocalizedString(text: "Custom"))", for: .normal)

        setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [], right_action: [], title: AppDelegate.sharedInstance.LocalizedString(text: Title.Analytics))
        
        arrAnalyticsData.append(AnalyticsData(displayDate: Date().getStringFromDate(dateFormat: DateFormat.dayDateMonthYear), strSearch: "", arrSectionData: [], displayDateFormat: DateFormat.dayDateMonthYear, FilterDateFormat: DateFormat.yearmonthday, previous: .previousDay, next: .nextDay))
        
        arrAnalyticsData.append(AnalyticsData(displayDate: Date().getStringFromDate(dateFormat: DateFormat.MonthYear), strSearch: "", arrSectionData: [], displayDateFormat: DateFormat.MonthYear, FilterDateFormat: DateFormat.yearmonth, previous: .previousMonth, next: .nextMonth))
        
        var startDate = "2019-02-06" // Date().getStringFromDate(dateFormat: DateFormat.yearmonthday)
        var endDate = "2019-02-15"  // Date().tomorrow.getStringFromDate(dateFormat: DateFormat.yearmonthday)
        var displayStartDate = "06-02-2019"
        var displayEndDate = "15-02-2019"

        if let startOfWeek = Date().startOfWeek, let endOfWeek = Date().endOfWeek {
            startDate = startOfWeek.getStringFromDate(dateFormat: DateFormat.yearmonthday)
            endDate = endOfWeek.getStringFromDate(dateFormat: DateFormat.yearmonthday)
            displayStartDate = startOfWeek.getStringFromDate(dateFormat: DateFormat.ddmmyyyy)
            displayEndDate = endOfWeek.getStringFromDate(dateFormat: DateFormat.ddmmyyyy)
        }
        
        arrAnalyticsData.append(AnalyticsData(displayDate: displayStartDate + " to " + displayEndDate, strSearch: "", arrSectionData: [], displayDateFormat: DateFormat.ddmmyyyy, FilterDateFormat: DateFormat.yearmonthday, startDate: startDate, endDate: endDate))
        
        if let accountInfo = UserDefaultManager.getAccountInfo() {
            
            arrAllDashboardInfo = DashboardInfo.getAllTransacation(account_Id: accountInfo.id!)
            
            let arrSelectedIndex = [MenuType.daily.rawValue, MenuType.monthly.rawValue, MenuType.custom.rawValue]
            for index in arrSelectedIndex {
                selectedIndex = index
                filterDataBasedOnDate(filterDate: Date().getStringFromDate(dateFormat: arrAnalyticsData[selectedIndex].FilterDateFormat), displayDate: arrAnalyticsData[selectedIndex].displayDate)
            }
            selectedIndex = MenuType.daily.rawValue
        }
        
        btnMenuTapped(btnDaily)
    }
    
    func reloadData() {
        runOnMainThread {
            self.collectionView.reloadData()
        }
    }
    
    @objc func expandCollapseCell(sender: UITapGestureRecognizer) {
        
        if let cell = sender.view as? AnalyticsSectionTVC {
            arrAnalyticsData[selectedIndex].arrSectionData[cell.tag].isExpand = !arrAnalyticsData[selectedIndex].arrSectionData[cell.tag].isExpand
        }
    }
    
    func getMenuButtonFromIndex() -> UIButton? {
        
        let views: [UIButton] = [btnDaily, btnMonthly, btnCustom]
        
        let arrFilter = views.filter { (button) -> Bool in
            return button.tag == selectedIndex
        }
        
        if arrFilter.count > 0 {
            return arrFilter[0]
        }
        
        return nil
    }
    
    func animateCollectionView(sender: UIButton) {
        
        runOnMainThread {
            self.collectionView.setContentOffset(CGPoint(x: self.collectionView.frame.width * CGFloat(sender.tag), y: 0), animated: true)
        }
    }
    
    func selectMenu(sender: UIButton) {
        
        let views: [UIButton] = [btnDaily, btnMonthly, btnCustom]
        views.enumerated().forEach { (index, view) in
            view.isSelected = (view == sender) ? true : false
        }
        
        runOnMainThread {
            self.const_viewSeparator_leading.constant = (sender.superview?.frame.origin.x)!
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func getTotalIncomeAndExpence() -> (Double, Double) {
        
        var incomeExpense = (0.0, 0.0)
        
        for i in 0..<arrAnalyticsData[selectedIndex].arrSectionData.count {
            
            let strIncome = arrAnalyticsData[selectedIndex].arrSectionData[i].income
            if let income = Double(strIncome) {
                incomeExpense.0 += income
            }
            
            let strExpense = arrAnalyticsData[selectedIndex].arrSectionData[i].expense
            if let expense = Double(strExpense) {
                incomeExpense.1 += expense
            }
        }
        
        return incomeExpense
    }
    
    @objc func btnDateTapped() {
        
        if selectedIndex == MenuType.daily.rawValue {
            
            let datePickerView = DatePickerView.instanceFromNib()
            datePickerView.delegate = self
            datePickerView.selectedDate = arrAnalyticsData[selectedIndex].displayDate.getDateFromString(dateFormat: arrAnalyticsData[selectedIndex].displayDateFormat)
            datePickerView.showInAppWindow()
            
        } else if selectedIndex == MenuType.custom.rawValue {
            
            let vc = loadVC(Identifier.Storyboard.Main, strVCId: Identifier.ViewController.SelectDatesVC) as! SelectDatesVC
            let startDate = arrAnalyticsData[selectedIndex].startDate?.getDateFromString(dateFormat: DateFormat.yearmonthday)?.getStringFromDate(dateFormat: DateFormat.ddmmyyyy)
            let endDate = arrAnalyticsData[selectedIndex].endDate?.getDateFromString(dateFormat: DateFormat.yearmonthday)?.getStringFromDate(dateFormat: DateFormat.ddmmyyyy)
            
            vc.dates = (startDate!, endDate!)
            vc.delegate = self
            vc.showInAppWindow()
            self.addChildViewController(vc)
        }
    }
    
    @objc func btnPreviousDateTapped() {
        
        if let previousDate = arrAnalyticsData[selectedIndex].displayDate.getDateFromString(dateFormat: arrAnalyticsData[selectedIndex].displayDateFormat)?.getDateFromPreviousNext(previousNext: arrAnalyticsData[selectedIndex].previous!) {
            
            arrAnalyticsData[selectedIndex].displayDate = (previousDate.getStringFromDate(dateFormat: arrAnalyticsData[selectedIndex].displayDateFormat))
            filterDataBasedOnDate(filterDate: previousDate.getStringFromDate(dateFormat: arrAnalyticsData[selectedIndex].FilterDateFormat), displayDate: arrAnalyticsData[selectedIndex].displayDate)
        }
    }
    
    @objc func btnNextDateTapped() {
        
        if let nextDate = arrAnalyticsData[selectedIndex].displayDate.getDateFromString(dateFormat: arrAnalyticsData[selectedIndex].displayDateFormat)?.getDateFromPreviousNext(previousNext: arrAnalyticsData[selectedIndex].next!) {
            
            arrAnalyticsData[selectedIndex].displayDate = (nextDate.getStringFromDate(dateFormat: arrAnalyticsData[selectedIndex].displayDateFormat))
            
            filterDataBasedOnDate(filterDate: nextDate.getStringFromDate(dateFormat: arrAnalyticsData[selectedIndex].FilterDateFormat), displayDate: arrAnalyticsData[selectedIndex].displayDate)
        }
    }
    
    @objc func btnFilterTapped() {
        
        let vc = loadVC(Identifier.Storyboard.Main, strVCId: Identifier.ViewController.PaymentTypeVC) as! PaymentTypeVC
        vc.arrPaymentType = arrPaymentType
        vc.delegate = self
        vc.showInAppWindow()
        self.addChildViewController(vc)
    }
}

extension AnalyticsVC {
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        
        selectedIndex = sender.tag
        selectMenu(sender: sender)
        animateCollectionView(sender: sender)
        reloadData()
    }
}

extension AnalyticsVC: SelectDatesVCDelegate {
    
    func getDates(dates: (String, String)) {
        
        arrAnalyticsData[selectedIndex].startDate = dates.0.getDateFromString(dateFormat: DateFormat.ddmmyyyy)?.getStringFromDate(dateFormat: DateFormat.yearmonthday)
        arrAnalyticsData[selectedIndex].endDate = dates.1.getDateFromString(dateFormat: DateFormat.ddmmyyyy)?.getStringFromDate(dateFormat: DateFormat.yearmonthday)
        arrAnalyticsData[selectedIndex].displayDate = dates.0 + " to " + dates.1
        
        filterDataBasedOnDate(filterDate: Date().getStringFromDate(dateFormat: arrAnalyticsData[selectedIndex].FilterDateFormat), displayDate: arrAnalyticsData[selectedIndex].displayDate)
    }
}

extension AnalyticsVC: DatePickerViewDelegate {
    
    func getDates(dates: (Date, Date, Bool)) {
        
    }
    
    func getSelectedDate(date: Date) {
        
        arrAnalyticsData[selectedIndex].displayDate = date.getStringFromDate(dateFormat: arrAnalyticsData[selectedIndex].displayDateFormat)
        
        filterDataBasedOnDate(filterDate: date.getStringFromDate(dateFormat: arrAnalyticsData[selectedIndex].FilterDateFormat), displayDate: arrAnalyticsData[selectedIndex].displayDate)
    }
}

extension AnalyticsVC: PaymentTypeVCDelegate {
    
    func getPaymentTypeArray(arrPaymentType: [PaymentType]) {
        
        self.arrPaymentType = arrPaymentType
        if let date = arrAnalyticsData[selectedIndex].displayDate.getDateFromString(dateFormat: arrAnalyticsData[selectedIndex].displayDateFormat) {
            filterDataBasedOnDate(filterDate: date.getStringFromDate(dateFormat: arrAnalyticsData[selectedIndex].FilterDateFormat), displayDate: arrAnalyticsData[selectedIndex].displayDate)
        }
    }
}

extension AnalyticsVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAnalyticsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier.Cell.AnalyticsCVC, for: indexPath) as! AnalyticsCVC
        
        let object = arrAnalyticsData[selectedIndex]
        cell.btnDate.setTitle(object.displayDate, for: .normal)
        cell.btnDate.addTarget(self, action: #selector(btnDateTapped), for: .touchUpInside)
        
        cell.btnPreviousDate.addTarget(self, action: #selector(btnPreviousDateTapped), for: .touchUpInside)
        cell.btnNextDate.addTarget(self, action: #selector(btnNextDateTapped), for: .touchUpInside)
        cell.btnFilter.addTarget(self, action: #selector(btnFilterTapped), for: .touchUpInside)

        if selectedIndex == MenuType.custom.rawValue {
            cell.btnPreviousDate.isHidden = true
            cell.btnNextDate.isHidden = true
            if let image = UIImage(named: ImageName.DrawerMenu.ic_edit) {
                
                cell.btnDate.layoutIfNeeded()
                cell.btnDate.setImage(image, for: .normal)
                cell.btnDate.imageView?.contentMode = .scaleAspectFit
                cell.btnDate.contentEdgeInsets = .init(top: 0, left: -40, bottom: 0, right: 0)
                cell.btnDate.imageEdgeInsets = .init(top: 0, left: 400, bottom: 0, right: 0)
            }
        } else {
            cell.btnPreviousDate.isHidden = false
            cell.btnNextDate.isHidden = false
            cell.btnDate.setImage(nil, for: .normal)
            cell.btnDate.contentEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        cell.searchBar.delegate = self
        cell.searchBar.text = object.strSearch
        if object.strSearch.count > 0, indexPath.item == selectedIndex {
            runOnMainThread {
                cell.searchBar.becomeFirstResponder()
            }
        }
        
        let totalIncomeExpense = getTotalIncomeAndExpence()
        cell.setTotalIncomeExpenseBalanceValue(income: totalIncomeExpense.0, expense: totalIncomeExpense.1)
        cell.setChartData(object: object)
        
        cell.tblView.dataSource = self
        cell.tblView.delegate = self
        cell.tblView.reloadData()
        
        if object.arrSectionData.count > 0 {
            cell.lblNoTransaction.isHidden = true
            cell.tblView.isHidden = false
            cell.chartSuperView.isHidden = false
        } else {
            cell.lblNoTransaction.isHidden = false
            cell.tblView.isHidden = true
            cell.chartSuperView.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        selectedIndex = Int(collectionView.contentOffset.x / collectionView.frame.width)
        if let menu = getMenuButtonFromIndex() {
            selectMenu(sender: menu)
        }
        reloadData()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        selectedIndex = Int(collectionView.contentOffset.x / collectionView.frame.width)
        if let menu = getMenuButtonFromIndex() {
            selectMenu(sender: menu)
        }
    }
}

extension AnalyticsVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if let string = searchBar.text, let textRange = Range(range, in: string) {
            
            var updatedText = string.replacingCharacters(in: textRange,
                                                         with: text)
            
            arrAnalyticsData[selectedIndex].strSearch = updatedText.trimNewLines()
            if arrSearchAnalyticsData.indices.contains(selectedIndex) {
                arrSearchAnalyticsData[selectedIndex].strSearch = updatedText.trimNewLines()
            }
            
            if updatedText.trimNewLines().count > 0 {
                
                if arrSearchAnalyticsData.count == 0 {
                    arrSearchAnalyticsData = arrAnalyticsData
                }
                
                arrAnalyticsData[selectedIndex].arrSectionData = arrSearchAnalyticsData[selectedIndex].arrSectionData.filter { (object) -> Bool in
                    return object.categoryName.lowercased().contains(updatedText.lowercased())
                }
                
            } else {
                if arrSearchAnalyticsData.count > 0 {
                    arrAnalyticsData = arrSearchAnalyticsData
                    arrSearchAnalyticsData = []
                }
            }
        }
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var text = searchText
        if text.trimNewLines().count == 0 {
            
            arrAnalyticsData[selectedIndex].strSearch = text
            if arrSearchAnalyticsData.indices.contains(selectedIndex) {
                arrSearchAnalyticsData[selectedIndex].strSearch = text
            }
            
            if arrSearchAnalyticsData.count > 0 {
                arrAnalyticsData = arrSearchAnalyticsData
                arrSearchAnalyticsData = []
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        runAfterTime(0.1) {
            self.view.endEditing(true)
        }
    }
}

extension AnalyticsVC: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return arrAnalyticsData[selectedIndex].arrSectionData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrAnalyticsData[selectedIndex].arrSectionData[section].isExpand {
            return arrAnalyticsData[selectedIndex].arrSectionData[section].arrRowData.count
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.AnalyticsTVC) as! AnalyticsTVC
        let object = arrAnalyticsData[selectedIndex].arrSectionData[indexPath.section].arrRowData[indexPath.row]
        if let date = object.transactionDate?.getDateFromString(dateFormat: DateFormat.yearmonthday)?.getStringFromDate(dateFormat: DateFormat.daymonth) {
            
            let arrDateMonth = date.split(separator: " ")
            if arrDateMonth.indices.contains(1) {
                cell.lblDate.attributedText = getAttibutedString(attrs1Color: UIColor(cgColor: Color.textColor.textColorGrey), attrs2Color: UIColor(cgColor: Color.textColor.textColorSemiDark), string1: String(arrDateMonth[0]), string2: "\n\(arrDateMonth[1])", font1: FONT(FontName.roboto_medium.rawValue, size: FontSize.navigationTitle), font2: FONT(FontName.roboto_regular.rawValue, size: FontSize.small))
            }
        }
        cell.lblPaymentMethod.text = object.transactionPayType
        
        if let transactionType = object.transactionType {
            
            if transactionType.lowercased() == TransactionTypeName.INCOME.rawValue.lowercased() {
                cell.lblAmount.textColor = Color.textColor.colorIncome
            } else {
                cell.lblAmount.textColor = Color.textColor.colorExpense
            }
        }
        
        cell.lblAmount.text = "\(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(Double(object.transactionAmount ?? "0.0") ?? 0.0)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.AnalyticsSectionTVC) as! AnalyticsSectionTVC
        let object = arrAnalyticsData[selectedIndex].arrSectionData[section]
        if let image = UIImage(named:AppDelegate.sharedInstance.getCategoryImage(iconName: object.categoryImageName)) {
            cell.imgViewIcon.image = image
        }
        
        cell.viewBGColor.backgroundColor = AppDelegate.sharedInstance.getCategoryColor(iconName: object.categoryImageName)
        cell.lblCategory.text = object.categoryName
        cell.lblNoOfTransaction.text = object.noOfTransaction + " \(ConstantValues.TRANSACTIONS)"
        
        cell.lblIncome.text = "\(ConstantValues.shortIncome) \(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(object.income))"
        cell.lblIncome.textColor = Color.textColor.colorIncome
        cell.lblExpense.text = "\(ConstantValues.shortExpense) \(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(object.expense))"
        cell.lblExpense.textColor = Color.textColor.colorExpense
        
        cell.tag = section
        let tap = UITapGestureRecognizer(target: self, action: #selector(expandCollapseCell(sender:)))
        cell.addGestureRecognizer(tap)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 61
    }
}

extension AnalyticsVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
