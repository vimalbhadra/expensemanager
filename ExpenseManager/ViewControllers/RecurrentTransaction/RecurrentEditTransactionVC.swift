//
//  RecurrentEditTransactionVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 04/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import GoogleMobileAds
import IQKeyboardManagerSwift
struct ReTransactionData {
    var strDisc: String
    var strValue: String
}
struct RePaymentType{
    var strType : String
}
class RecurrentEditTransactionVC: BaseVC {
    
    @IBOutlet weak var textViewNotes: UITextView!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var tblRecurrentTransaction: UITableView!
    @IBOutlet weak var btnIncome: UIButton!
    @IBOutlet weak var btnExpense: UIButton!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var const_viewSeparator_leading: NSLayoutConstraint!
    @IBOutlet weak var viewIncomeExpense: UIView!

    var arrReTransactionData = [ReTransactionData]()
    var arrPaymentType = [String]()
    var arrRecurrentType = [String]()
    var arrAccount = [AccountInfo]()
    var arrRecurrentlist = [RecurrentInfo]()
    var arrReTransactionInfo: RecurrentInfo = RecurrentInfo()
    var selectedIndex: Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        adBannerView.adsbannerView.delegate = self

        if self.textViewNotes != nil {
            self.textViewNotes.layer.cornerRadius = 5.0
            self.textViewNotes.layer.borderWidth = 1.0
            self.textViewNotes.layer.borderColor = Color.textColor.textColorGrey
        }
        initialSetup()
    }
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = AppDelegate.sharedInstance.LocalizedString(text: Title.Alert.Done)

        viewIncomeExpense.backgroundColor = UIColor(hexString: getSelectedThemeColor())
        self.lblPlaceHolder.isHidden = textViewNotes.text == "" ? false: true
    }
 
}
extension RecurrentEditTransactionVC {
    func initialSetup() {
        
        view.layoutIfNeeded()
        
      
            
        setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [ImageName.Navigation.navcorrect, ImageName.Navigation.navdelete], right_action: [#selector(addTransacation), #selector(deleteTransacation)], title: AppDelegate.sharedInstance.LocalizedString(text: Title.EditTransaction))
            arrReTransactionInfo.recurrentColumnId = arrRecurrentlist[0].recurrentColumnId!
        textViewNotes.text = arrRecurrentlist[0].recurrentNote!
        arrReTransactionInfo.recurrentNote =  arrRecurrentlist[0].recurrentNote!

      if arrRecurrentlist[0].recurrentTransactionType?.lowercased() == "income"{
            btnMenuTapped(btnIncome)
        }else{
            btnMenuTapped(btnExpense)
        }
   
        initializePaymenType()
        initializeRecurrentType()
        initilizeTransactionData()
        reloadData()
    }
    func initilizeTransactionData(){
        
        let tempStrAmount = arrRecurrentlist[0].recurrentAmount!
        
        //            if let dotRange = tempStrAmount.range(of: ".") {
        //                tempStrAmount.removeSubrange(dotRange.lowerBound..<tempStrAmount.endIndex)
        //            }
        arrReTransactionData.append(ReTransactionData(strDisc: "AMOUNT:", strValue: tempStrAmount))
        arrReTransactionInfo.recurrentAmount = tempStrAmount

        
        let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: arrRecurrentlist[0].recurrentNextDate!)
        arrReTransactionData.append(ReTransactionData(strDisc: "DATE:", strValue: getStringFromDate(dateFormat: DateFormat.dayDateMonthYear, date: tempDate!)))
        arrReTransactionInfo.recurrentNextDate = getStringFromDate(dateFormat: DateFormat.yearmonthday, date: tempDate!)
       
        arrReTransactionData.append(ReTransactionData(strDisc: "CATEGORY:", strValue: arrRecurrentlist[0].recurrentcategoryName!))
        arrReTransactionInfo.recurrentCatId =  arrRecurrentlist[0].recurrentCatId!
        
        arrReTransactionData.append(ReTransactionData(strDisc: "PAYMENT TYPE:", strValue: arrRecurrentlist[0].recurrentPayType!))
        arrReTransactionInfo.recurrentPayType =  arrRecurrentlist[0].recurrentPayType
        
        arrReTransactionData.append(ReTransactionData(strDisc: "RECURRENT TYPE:", strValue: arrRecurrentlist[0].recurrentType!))
        arrReTransactionInfo.recurrentType =  arrRecurrentlist[0].recurrentType!
        
        arrReTransactionInfo.recurrentCatId = arrRecurrentlist[0].recurrentCatId!
        arrReTransactionInfo.recurrentcategoryName = arrRecurrentlist[0].recurrentcategoryName!
        arrReTransactionInfo.recurrentcategoryIconName = arrRecurrentlist[0].recurrentcategoryName!

        
    }
    func initializePaymenType(){
        arrPaymentType = ["CASH","CHEQUE","BANK TRANSFER","CREDIT CARD","DEBIT CARD"]
        arrReTransactionInfo.recurrentPayType =  arrRecurrentlist[0].recurrentPayType!
    }
    func initializeRecurrentType(){
        arrRecurrentType = ["DAILY","WEEKLY","MONTHLY"]
    }
    
    
    func reloadData() {
        runOnMainThread {
            self.tblRecurrentTransaction.reloadData()
        }
    }
    
    
    func getMenuButtonFromIndex() -> UIButton? {
        
        let views: [UIButton] = [btnIncome, btnExpense]
        
        let arrFilter = views.filter { (button) -> Bool in
            return button.tag == selectedIndex
        }
        
        if arrFilter.count > 0 {
            return arrFilter[0]
        }
        
        return nil
    }
    
    func animateCollectionView(sender: UIButton) {
        
        runOnMainThread {
            self.tblRecurrentTransaction.setContentOffset(CGPoint(x: self.tblRecurrentTransaction.frame.width * CGFloat(sender.tag), y: 0), animated: false)
        }
    }
    
    func selectMenu(sender: UIButton) {
        
        let views: [UIButton] = [btnIncome, btnExpense]
        views.enumerated().forEach { (index, view) in
            view.isSelected = (view == sender) ? true : false
        }
        
        runOnMainThread {
            self.const_viewSeparator_leading.constant = (sender.superview?.frame.origin.x)!
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func validationCheckTransacation()->Bool{
        let validation = true
        if arrReTransactionInfo.recurrentAmount == "0" || arrReTransactionInfo.recurrentAmount == "" || arrReTransactionInfo.recurrentAmount == nil{
            APPDELEGATE.showToastMessage(Messages.enteramount, wantBottomSide: true, hideAutomatically: true)
            
            return false
        }else if arrReTransactionInfo.recurrentCatId == nil || arrReTransactionInfo.recurrentCatId == 0{
            APPDELEGATE.showToastMessage(Messages.selectcategory, wantBottomSide: true, hideAutomatically: true)
            return false
            
        }else{
            return validation
        }
    }
    

}
extension RecurrentEditTransactionVC {
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            arrReTransactionInfo.recurrentTransactionType = "INCOME"
        }else{
            arrReTransactionInfo.recurrentTransactionType = "EXPENSE"
        }
        selectMenu(sender: sender)
        //animateCollectionView(sender: sender)
    }
    
    @objc func addTransacation() {
        let validationCheck = validationCheckTransacation()
        if validationCheck {
            arrReTransactionInfo.recurrentAmount = arrReTransactionInfo.recurrentAmount!
            if RecurrentInfo.editRecurrentTransaction(retransactionInfo: arrReTransactionInfo){
                APPDELEGATE.showToastMessage(Messages.TransactionUpdate, wantBottomSide: true, hideAutomatically: true)
                AppDelegate.sharedInstance.adscount = AppDelegate.sharedInstance.adscount + 1
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    @objc func deleteTransacation() {
        APPDELEGATE.showAlert(title: Title.Alert.Delete, message: Messages.deleteTransactionPermission, actions: [Title.Alert.CANCEL, Title.Alert.DELETE]) { (index) in
            if index == 1 {
                if RecurrentInfo.deleteReCurrentTransacation(recurrentColumnID: self.arrRecurrentlist[0].recurrentColumnId!){
                    DLog("Recurrent Transaction Delete")
                    APPDELEGATE.showToastMessage(Messages.TransactionDelete, wantBottomSide: true, hideAutomatically: true)
                    AppDelegate.sharedInstance.adscount = AppDelegate.sharedInstance.adscount + 1
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
        }
    }
}
extension RecurrentEditTransactionVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReTransactionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.AddEditAmountTVC) as! AddEditAmountTVC
            cell.lblDisc.text = arrReTransactionData[indexPath.row].strDisc
            cell.txtAmountValue.text = arrReTransactionData[indexPath.row].strValue
            cell.txtAmountValue.keyboardType = .decimalPad
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.AddEditTVC) as! AddEditTVC
            cell.lblDisc.text = arrReTransactionData[indexPath.row].strDisc
            cell.lblValue.text = arrReTransactionData[indexPath.row].strValue
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var nextVC :CategoryManagmentVC? = nil
        self.view.endEditing(true)
        if indexPath.row == 1 {
            let datePickerView = DatePickerView.instanceFromNib()
            datePickerView.delegate = self
            datePickerView.selectedDate = arrReTransactionData[indexPath.row].strValue.getDateFromString(dateFormat: DateFormat.dayDateMonthYear)
            datePickerView.showInAppWindow()
        }else if indexPath.row == 2 {
            nextVC = (loadVC(Identifier.Storyboard.SB_Category, strVCId: Identifier.ViewController.CategoryManagmentVC) as! CategoryManagmentVC)
            nextVC!.delegate = self
            navigationController?.pushViewController(nextVC!, animated: true)
        }else if indexPath.row == 3{
            let vc = PickerViewVC(nibName: "PickerViewVC", bundle: nil)
            vc.delegate = self
            vc.arrPaymentType = arrPaymentType
            vc.selcatedValue = arrReTransactionData[3].strValue
            vc.selctedType = TransactionType(rawValue: 0)!
            if let rootViewController = APPDELEGATE.window?.rootViewController {
                var frame: CGRect = vc.view.frame
                if let presentedViewController = rootViewController.presentedViewController {
                    frame = rootViewController.view.frame
                    presentedViewController.view.addSubview(vc.view)
                    presentedViewController.addChildViewController(vc)
                } else {
                    frame = rootViewController.view.frame
                    rootViewController.view.addSubview(vc.view)
                    rootViewController.addChildViewController(vc)
                }
                vc.view.frame = frame
            }
        }else if indexPath.row == 4{
            
      
                let vc = PickerViewVC(nibName: "PickerViewVC", bundle: nil)
                vc.delegate = self
                vc.arrRecurrentType = arrRecurrentType
                vc.selcatedValue = arrReTransactionData[4].strValue
                vc.selctedType = TransactionType(rawValue: 1)!
                if let rootViewController = APPDELEGATE.window?.rootViewController {
                    var frame: CGRect = vc.view.frame
                    if let presentedViewController = rootViewController.presentedViewController {
                        frame = rootViewController.view.frame
                        presentedViewController.view.addSubview(vc.view)
                        presentedViewController.addChildViewController(vc)
                    } else {
                        frame = rootViewController.view.frame
                        rootViewController.view.addSubview(vc.view)
                        rootViewController.addChildViewController(vc)
                    }
                    vc.view.frame = frame
                }
       
        }
    }
}
extension RecurrentEditTransactionVC : DatePickerViewDelegate {
    
    func getDates(dates: (Date, Date, Bool)) {
    
    }
    
    func getSelectedDate(date: Date) {
        arrReTransactionData[1].strValue = date.getStringFromDate(dateFormat: DateFormat.dayDateMonthYear)
        arrReTransactionInfo.recurrentNextDate = date.getStringFromDate(dateFormat: DateFormat.yearmonthday)
        self.tblRecurrentTransaction.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
    }
}
extension RecurrentEditTransactionVC : PickerViewDelegate{
    func getSelectedAccount(strAType: String, accountID: Int32) {
        
    }
    
    
    func getSelectedPaymentType(strPType: String) {
        arrReTransactionData[3].strValue = strPType
        arrReTransactionInfo.recurrentPayType = strPType
        self.tblRecurrentTransaction.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .automatic)
    }
    
    func getSelectedRecurrentType(strRType: String, selectedIndex: Int) {
        
            arrReTransactionData[4].strValue = strRType
            arrReTransactionInfo.recurrentType = strRType
            self.tblRecurrentTransaction.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
        
    }
 
}
extension RecurrentEditTransactionVC: CategoryViewDelegate{
    
    func selectedCategory(_ categoryInfo: CategoryManagment) {
        arrReTransactionData[2].strValue = categoryInfo.categoryName!
        arrReTransactionInfo.recurrentCatId = categoryInfo.categoryId
        arrReTransactionInfo.recurrentcategoryName = categoryInfo.categoryName!
        arrReTransactionInfo.recurrentcategoryIconName = categoryInfo.categoryName!
        self.tblRecurrentTransaction.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
    }
}
extension RecurrentEditTransactionVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                return true
            }
        }
        var updatedText = ""
        if let text = textField.text, let textRange = Range(range, in: text) {
            
            updatedText = text.replacingCharacters(in: textRange,
                                                   with: string)
            
            if !updatedText.containsEmoji {
                
                let allowedCharacters = CharacterSet(charactersIn:"0123456789.,")//Here change this characters based on your requirement
                let characterSet = CharacterSet(charactersIn: string)
                if allowedCharacters.isSuperset(of: characterSet) {
                    updatedText = updatedText.replacingOccurrences(of: ",", with: ".")
                    if updatedText.count > 10 || (Double(updatedText) == nil){
                        return false
                    }
                    
                    return true
                }
            }
        }
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension RecurrentEditTransactionVC : UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        lblPlaceHolder.isHidden = true
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
            lblPlaceHolder.isHidden = true
            arrReTransactionInfo.recurrentNote =  textView.text!
        }else{
            lblPlaceHolder.isHidden = false
            arrReTransactionInfo.recurrentNote =  textView.text!
        }
    }
}
extension RecurrentEditTransactionVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
