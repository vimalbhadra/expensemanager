//
//  RecurrentTransactionListVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 04/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import GoogleMobileAds
class RecurrentTransactionListVC: BaseVC {
    @IBOutlet weak var tblRecurrentlist: UITableView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewAdd: UIView!

    var arrRecurrntList = [RecurrentInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        adBannerView.adsbannerView.delegate = self

        setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [], right_action: [], title: AppDelegate.sharedInstance.LocalizedString(text: Title.RecurrentTransaction))
        tblRecurrentlist.tableFooterView = UIView(frame: .zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showbannerAds()
        if (kdefaults.bool(forKey: kShowAds) && AppDelegate.sharedInstance.adscount > 3){
            interstitial = createAndLoadInterstitial()
            AppDelegate.sharedInstance.adscount = 0
        }
        viewAdd.backgroundColor = UIColor(hexString: getSelectedThemeColor())
        if let accountInfo = UserDefaultManager.getAccountInfo() {
            getAllRecurrentData(accountID: accountInfo.id!)
        }
    }
 
}
extension RecurrentTransactionListVC{
    
    func getAllRecurrentData(accountID:Int32){
        arrRecurrntList.removeAll()
        arrRecurrntList = RecurrentInfo.getAllRecurrentTransacation(account_Id: accountID)
        setData(data: arrRecurrntList)
    }
    //#MARK: SetData
    func setData(data:[RecurrentInfo]) {
        runOnMainThread {
            if self.lblMessage != nil, self.tblRecurrentlist != nil {
                self.lblMessage.isHidden = (self.arrRecurrntList.count > 0) ? true : false
                self.tblRecurrentlist.isHidden = !self.lblMessage.isHidden
                self.tblRecurrentlist.reloadData()
            }
        }
    }
}
extension RecurrentTransactionListVC {
    @IBAction func btnAddTapped(_ sender: UIButton) {
        let viewController = loadVC(Identifier.Storyboard.SB_AddEditTransaction, strVCId: Identifier.ViewController.AddEditTransactionVC)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
   
}
extension RecurrentTransactionListVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRecurrntList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.ExpenseManagerTVC) as! ExpenseManagerTVC
        let objectData = arrRecurrntList[indexPath.row]
        cell.lblCategoryName.text = objectData.recurrentcategoryName!
        cell.lblNotes.text = objectData.recurrentNote!
        cell.lblTranscationAmount.text = objectData.recurrentAmount!
        let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: objectData.recurrentNextDate!)
        cell.lblTranscationDate.text = getStringFromDate(dateFormat: DateFormat.daymonth, date: tempDate!)
        cell.viewCategory.backgroundColor = AppDelegate.sharedInstance.getCategoryColor(iconName: objectData.recurrentcategoryIconName!)
        if let cType = objectData.recurrentcategoryIconName, let image = UIImage(named:AppDelegate.sharedInstance.getCategoryImage(iconName: cType)) {
            cell.imgCategory.image  = image
        }
        if objectData.recurrentTransactionType?.lowercased() == "income" {
            cell.lblTranscationAmount.textColor = Color.textColor.colorIncome
        }else{
            cell.lblTranscationAmount.textColor = Color.textColor.colorExpense
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = loadVC(Identifier.Storyboard.SB_RecurrentTransaction, strVCId: Identifier.ViewController.RecurrentEditTransactionVC) as! RecurrentEditTransactionVC
        viewController.arrRecurrentlist = [arrRecurrntList[indexPath.row]]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
extension RecurrentTransactionListVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
