//
//  SelectLanguageVC.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 10/03/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

//--------------------------------------------------------

// MARK:- Frameworks

//--------------------------------------------------------

import UIKit

//--------------------------------------------------------

// MARK:- Class: SelectLanguageVC

//--------------------------------------------------------

class SelectLanguageVC: BaseVC {
    
    //--------------------------------------------------------
    
    // MARK:- Properties: IBOutlets
    
    //--------------------------------------------------------
    
    @IBOutlet weak var tblLanguage: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    
    var arrLanguage = [("English", "en"),
                       ("Portuguese (Português)", "pt"),
                       ("Spanish (Español)", "es"),
                       ("French (français)", "fr"),
                       ("Russian (русский)", "ru"),
                       ("Arabic (عربى)", "ar"),
                       ("Hindi (हिंदी)", "hi"),
                       ("German (Deutsche)", "de"),
                       ("Japanese (日本人)", "ja")]
    
    var selectedIndexPath = IndexPath(row: 0, section: 0) {
        didSet {
            runOnMainThread {
                if self.tblLanguage != nil {
                    self.tblLanguage.reloadData()
                }
            }
        }
    }
    
    //--------------------------------------------------------
    
    // MARK:- Memory Management Methods
    
    //--------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //--------------------------------------------------------
    
    // MARK:- View Life Cycle Methods
    
    //--------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    //--------------------------------------------------------
}

//--------------------------------------------------------

// MARK:- Action Methods

//--------------------------------------------------------

extension SelectLanguageVC {
    
    //--------------------------------------------------------
    
    @IBAction func btnContinueTapped(_ sender: UIButton) {
        
        kdefaults.set(true, forKey: klaunchedBefore)
        kdefaults.set(true, forKey: kshowpopupRecurrent)
        
        kdefaults.set(arrLanguage[selectedIndexPath.row].1, forKey : kLanguagecode)
        kdefaults.set(arrLanguage[selectedIndexPath.row].0, forKey : kLanguage)
        
        APPDELEGATE.setLanguage()
        APPDELEGATE.setDrawerMenu()
        // popVC()
    }
}

//--------------------------------------------------------

// MARK:- Custom Methods

//--------------------------------------------------------

extension SelectLanguageVC {
    
    //--------------------------------------------------------
    
    fileprivate func initialSetup() {
        
        setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [], right_action: [], title: AppDelegate.sharedInstance.LocalizedString(text: Title.SelectLanguage))
        tblLanguage.tableFooterView = UIView(frame: .zero)
        if let themeColor = UserDefaultManager.getData(forKey: Parameters.UserDefault.ThemeColor) as? String {
            btnContinue.backgroundColor = UIColor(hexString: themeColor)
        }
        
        
        if let code = kdefaults.value(forKey: kLanguagecode) as? String {
            if let index = arrLanguage.index(where: { (arg0) -> Bool in
                let (_, lanCode) = arg0
                return code == lanCode
            }) {
                selectedIndexPath = IndexPath(row: index, section: 0)
            }
        }
    }
    
    override func btnBackTapped() {
        
        if !kdefaults.bool(forKey: klaunchedBefore) {
            
            kdefaults.set(true, forKey: klaunchedBefore)
            kdefaults.set(true, forKey: kshowpopupRecurrent)
            
            APPDELEGATE.setLanguage()
            APPDELEGATE.setDrawerMenu()
            
        } else {
            popVC()
        }
    }
}

//--------------------------------------------------------

// MARK:- UITableViewDataSource Methods

//--------------------------------------------------------

extension SelectLanguageVC: UITableViewDataSource {
    
    //--------------------------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLanguage.count
    }
    
    //--------------------------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectLanguageTVC") as! SelectLanguageTVC
        cell.lblLanguage.text = arrLanguage[indexPath.row].0
        if indexPath == selectedIndexPath {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    //--------------------------------------------------------
}

//--------------------------------------------------------

// MARK:- UITableViewDelegate Methods

//--------------------------------------------------------

extension SelectLanguageVC: UITableViewDelegate {
    
    //--------------------------------------------------------
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
    }
    
    //--------------------------------------------------------
}
