//
//  ExpenseManagerVC.swift
//  ExpenseManager
//
//  Created by Piyu on 24/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ExpenseManagerVC: BaseVC {

    @IBOutlet weak var tblDashboard: UITableView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var bottomView: BottomView!
    @IBOutlet weak var viewAdd: UIView!


    var arrDashboard = [DashboardInfo]()
    var arrDuplicateDashboard = [DashboardInfo]()
    var strselectedDate : String = ""
    var strYear : String = ""
    var strMonth : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        adBannerView.adsbannerView.delegate = self
        showbannerAds()
    }
    override func viewWillAppear(_ animated: Bool) {
      
        if (kdefaults.bool(forKey: kShowAds) && AppDelegate.sharedInstance.adscount >= 3){
            interstitial = createAndLoadInterstitial()
            AppDelegate.sharedInstance.adscount = 0
        }
       
        setNavigationbarleft_title([ImageName.Navigation.navmenu], left_action: [#selector(btnDrawerTapped)], right_imageName: [], right_action: [], title: AppDelegate.sharedInstance.LocalizedString(text: Title.ExpenseManager))
        tblDashboard.tableFooterView = UIView(frame: .zero)
        
        viewAdd.backgroundColor = UIColor(hexString:  getSelectedThemeColor())
        
        if let accountInfo = UserDefaultManager.getAccountInfo() {
            
            strYear = Date().getStringFromDate(dateFormat: DateFormat.year)
            strMonth =  Date().getStringFromDate(dateFormat: DateFormat.monthmm)
            strselectedDate = strYear + "-" + strMonth
            getAllDashbaordData(accountID: accountInfo.id!)
        }
    }
    
   
 
}
extension ExpenseManagerVC{
    
    func setDateLable(selectedDate:Date,data:[DashboardInfo]){
        self.lblMonth.text = selectedDate.getStringFromDate(dateFormat: DateFormat.month)
        self.lblYear.text = selectedDate.getStringFromDate(dateFormat: DateFormat.year)
        sortingData(data: arrDuplicateDashboard)
    }
    
    func setIncomeExpenseBalanceValue(){
        bottomView.lblIncome.text = "\(AppDelegate.sharedInstance.LocalizedString(text: "INCOME")) \n \(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(getIncomevalue()))"
        bottomView.lblIncome.textColor = Color.textColor.colorIncome
        bottomView.lblExpense.text = "\(AppDelegate.sharedInstance.LocalizedString(text: "EXPENSE")) \n \(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(getExpensevalue()))"
        bottomView.lblExpense.textColor = Color.textColor.colorExpense

        let balance = getIncomevalue() - getExpensevalue()
        bottomView.lblBalance.text = "\(AppDelegate.sharedInstance.LocalizedString(text: "BALANCE")) \n \(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(balance))"
    }

    
    
    func getIncomevalue()-> Double{
        
        var totalIncome: Double = 0
        _ = self.arrDashboard.filter { (object) -> Bool in
            if let strType = object.transactionType?.lowercased(), strType == "income" {
                if totalIncome == 0 {
                    totalIncome = Double(object.transactionAmount ?? "")!
                } else {
                    totalIncome = totalIncome +  Double(object.transactionAmount ?? "")!
                }
                return true
            }
            return false
        }
        
        return (totalIncome)
        
    }
    func getExpensevalue()-> Double{
        
        var totalExpense: Double = 0
        _ = self.arrDashboard.filter { (object) -> Bool in
            if let strType = object.transactionType?.lowercased(), strType == "expense" {
                if totalExpense == 0 {
                    totalExpense = Double(object.transactionAmount ?? "")!
                } else {
                    totalExpense = totalExpense +  Double(object.transactionAmount ?? "")!
                }
                return true
            }
            return false
        }
        
        return (totalExpense)
        
    }

    
    func getAllDashbaordData(accountID:Int32){
        
        arrDuplicateDashboard.removeAll()
        arrDuplicateDashboard = DashboardInfo.getAllTransacation(account_Id: accountID)
        
        let tempDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
        setDateLable(selectedDate: tempDate!, data: arrDuplicateDashboard)
    }
    
    func sortingData(data:[DashboardInfo]){
        arrDashboard.removeAll()
        data.enumerated().forEach { (index,object) in
            
            let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: object.transactionDate!)
            let tempStrDate = getStringFromDate(dateFormat: DateFormat.yearmonth, date: tempDate!)
            
            if  tempStrDate == strselectedDate{
                arrDashboard.append(object)
            }
        }
         setData(data: arrDashboard)
        
    }
    
    //#MARK: SetData
    func setData(data:[DashboardInfo]) {
        runOnMainThread {
            if self.lblMessage != nil, self.tblDashboard != nil {
                self.lblMessage.isHidden = (self.arrDashboard.count > 0) ? true : false
                self.tblDashboard.isHidden = !self.lblMessage.isHidden
                self.tblDashboard.reloadData()
                self.setIncomeExpenseBalanceValue()
            }
        }
    }
}
extension ExpenseManagerVC {
    
    @IBAction func btnAddTapped(_ sender: UIButton) {
        
        let viewController = loadVC(Identifier.Storyboard.SB_AddEditTransaction, strVCId: Identifier.ViewController.AddEditTransactionVC)
        self.navigationController?.pushViewController(viewController, animated: true)
       
    }
    @IBAction func btnTappedMonthNextPrv(_ sender: UIButton) {
        
        var nextMonth : Date? = nil
        if sender.tag == 1000{
            //prev month click
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            nextMonth = Calendar.current.date(byAdding: .month, value: -1, to: temDate!)
            strMonth = getStringFromDate(dateFormat: DateFormat.monthmm, date: nextMonth!)
            
        }else{
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: temDate!)
            strMonth = getStringFromDate(dateFormat: DateFormat.monthmm, date: nextMonth!)

        }
        strselectedDate = strYear + "-" + strMonth
        let tempDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
        
        setDateLable(selectedDate: tempDate!, data: arrDuplicateDashboard)

    }
    @IBAction func btnTappedYearNextPrv(_ sender: UIButton) {
        
        var nextYear : Date? = nil
        
        if sender.tag == 2000{
            //prev year click
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
           nextYear = Calendar.current.date(byAdding: .year, value: -1, to: temDate!)
            strYear = getStringFromDate(dateFormat: DateFormat.year, date: nextYear!)
        }else{
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)
            nextYear = Calendar.current.date(byAdding: .year, value: 1, to: temDate!)
            strYear = getStringFromDate(dateFormat: DateFormat.year, date: nextYear!)
        }
        strselectedDate = strYear + "-" + strMonth
        let tempDate = getDateFromString(dateFormat: DateFormat.yearmonth, strDate: strselectedDate)

        setDateLable(selectedDate: tempDate!, data: arrDuplicateDashboard)

        
    }
    
}
extension ExpenseManagerVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDashboard.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.ExpenseManagerTVC) as! ExpenseManagerTVC
        let objectData = arrDashboard[indexPath.row]
        cell.lblCategoryName.text = objectData.categoryName!
        cell.lblNotes.text = objectData.transactionNote!
        cell.lblTranscationAmount.text = (AppDelegate.sharedInstance.getCurrencyDetails()?.cURRENCYSYMBOL)! + objectData.transactionAmount!
        let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: objectData.transactionDate!)
        cell.lblTranscationDate.text = getStringFromDate(dateFormat: DateFormat.daymonth, date: tempDate!)
        cell.viewCategory.backgroundColor = AppDelegate.sharedInstance.getCategoryColor(iconName: objectData.categoryIconName!)
        if let cType = objectData.categoryIconName, let image = UIImage(named:AppDelegate.sharedInstance.getCategoryImage(iconName: cType)) {
            cell.imgCategory.image  = image
        }
        if objectData.transactionType?.lowercased() == "income" {
            cell.lblTranscationAmount.textColor = Color.textColor.colorIncome
        }else{
            cell.lblTranscationAmount.textColor = Color.textColor.colorExpense

        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = loadVC(Identifier.Storyboard.SB_AddEditTransaction, strVCId: Identifier.ViewController.AddEditTransactionVC) as! AddEditTransactionVC
        viewController.isBoolEdit = true
        viewController.arrDashboard = [arrDashboard[indexPath.row]]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
extension ExpenseManagerVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
