//
//  BackupVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 28/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import SwiftyDropbox
import Zip
import GoogleMobileAds
class BackupVC: BaseVC {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnLinkDropbox: UIButton!
    @IBOutlet weak var btnTakeBackup: UIButton!
    @IBOutlet weak var tblViewFiles: UITableView!

    let dataBasePath = Util.getDD().appendingPathComponent(ConstantValues.DATABASE_NAME)
    var folderName = Util.getDD().appendingPathComponent(ConstantValues.ZIP_FOLDERNAME)
    let dataBaseTxtPath = Util.getDD().appendingPathComponent(ConstantValues.ZIP_FOLDERNAME).appendingPathComponent(ConstantValues.DATABASE_NAME_TXT)
    let zipFilePathURL = Util.getDD().appendingPathComponent(ConstantValues.ZIP_FOLDERNAME).appendingPathComponent(ConstantValues.ZIP_FILENAME + ConstantValues.zipExtension)
    
    var arrFiles = [Files.Metadata]() {
        didSet {
            self.reloadData()
        }
    }
    
    @IBAction func btnLinkDropboxTapped(_ sender: UIButton) {
        
        if btnLinkDropbox.isSelected {
            unlinkDropbox()
        } else {
            linkDropbox()
        }
    }
    
    @IBAction func btnTakeBackupTapped(_ sender: UIButton) {
        
        isZipCreated { (result, data) in
            if result, data != nil {
                uploadToDropbox(data: data!)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adBannerView.adsbannerView.delegate = self
        initialSetup()
        getListOfBackupFromDropbox()
    }
    override func viewWillAppear(_ animated: Bool) {
        showbannerAds()
        btnLinkDropbox.backgroundColor = UIColor(hexString: getSelectedThemeColor())
        btnTakeBackup.backgroundColor = UIColor(hexString: getSelectedThemeColor())

    }
}

extension BackupVC {
    
    func initialSetup() {
        setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], title: AppDelegate.sharedInstance.LocalizedString(text: Title.Backup))
        tblViewFiles.tableFooterView = UIView(frame: .zero)
    }
    
    func reloadData() {
        
        runOnMainThread {
            if self.arrFiles.count > 0 {
                self.tblViewFiles.isHidden = false
                self.lblMessage.isHidden = true
                self.tblViewFiles.reloadData()
            } else {
                self.tblViewFiles.isHidden = true
                self.lblMessage.isHidden = false
                if DropboxClientsManager.authorizedClient == nil {
                    self.btnTakeBackup.isEnabled = false
                    self.btnLinkDropbox.isSelected = false
                    self.lblMessage.text = Messages.noDropboxAccount
                } else {
                    self.btnTakeBackup.isEnabled = true
                    self.btnLinkDropbox.isSelected = true
                    self.lblMessage.text = Messages.notBackupFiles
                }
            }
        }
    }
    
    @objc func btnRestoreTapped(_ sender: UIButton) {
        
        APPDELEGATE.showAlert(title: nil, message: Messages.backupRestorePermission, actions: [Title.Alert.CANCEL, Title.Alert.OK]) { (index) in
            if index == 1 {
                if let pathDisplay = self.arrFiles[sender.tag].pathDisplay {
                    self.downloadFromDropbox(path: pathDisplay, index: sender.tag)
                } else {
                    APPDELEGATE.showToastMessage(Messages.backupDeletedFail)
                }
            }
        }
    }
    
    @objc func btnDeleteTapped(_ sender: UIButton) {
        
        APPDELEGATE.showAlert(title: nil, message: Messages.backupDeletedPermission, actions: [Title.Alert.CANCEL, Title.Alert.DELETE]) { (index) in
            if index == 1 {
                if let pathDisplay = self.arrFiles[sender.tag].pathDisplay {
                    self.deleteFileFromDropbox(path: pathDisplay, index: sender.tag)
                } else {
                    APPDELEGATE.showToastMessage(Messages.backupDeletedFail)
                }
            }
        }
    }
    
    func isZipCreated(completionHandler: ((Bool, NSData?) -> Void)) {
        
        do {
            
            if !FileManager.default.fileExists(atPath: folderName.absoluteString) {
                try FileManager.default.createDirectory(at: folderName, withIntermediateDirectories: true, attributes: nil)
            }
            
            try FileManager.default.copyItem(at: dataBasePath, to: dataBaseTxtPath)
            try Zip.zipFiles(paths: [dataBaseTxtPath], zipFilePath: zipFilePathURL, password: nil, progress: nil)
            
            if let fileData = NSData(contentsOf: zipFilePathURL) {
                completionHandler(true, fileData)
            } else {
                APPDELEGATE.showToastMessage(Messages.backupUnsuccessful)
                completionHandler(false, nil)
            }
            
        } catch {
            
            APPDELEGATE.showToastMessage(Messages.backupUnsuccessful)
            completionHandler(false, nil)
        }
    }
    
    func isUnZipFile() -> Bool {
        
        do {
            try Zip.unzipFile(zipFilePathURL, destination: folderName, overwrite: true, password: nil)
            return true
        } catch {
            APPDELEGATE.showToastMessage(Messages.backupRestoredFail)
            return false
        }
    }
    
    func isStoreZipFileToBackupFolder(data: Data) -> Bool {
        
        do {
            if !FileManager.default.fileExists(atPath: self.folderName.absoluteString) {
                try FileManager.default.createDirectory(at: self.folderName, withIntermediateDirectories: true, attributes: nil)
            }
            
            try data.write(to: self.zipFilePathURL, options: Data.WritingOptions.atomicWrite)
            return true
            
        } catch {
            APPDELEGATE.showToastMessage(Messages.backupRestoredFail)
            return false
        }
    }
    
    func isCopyTxtFileToSqliteFile() -> Bool {
        
        do {
            try FileManager.default.removeItem(at: self.dataBasePath)
            try FileManager.default.copyItem(at: self.dataBaseTxtPath, to: self.dataBasePath)
            return true
        } catch {
            APPDELEGATE.showToastMessage(Messages.backupRestoredFail)
            return false
        }
    }
    
    func isDeleteBackupFolder(message: String) -> Bool {
        
        do {
            try FileManager.default.removeItem(at: folderName)
            return true
        } catch {
            APPDELEGATE.showToastMessage(message)
            return false
        }
    }
    
    func linkDropbox() {
        
        APPDELEGATE.completionHadler = { result in
            if result {
                self.getListOfBackupFromDropbox()
            }
        }
        
        DropboxClientsManager.authorizeFromController(UIApplication.shared, controller: self) { (url) in
            DLog(url)
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                APPDELEGATE.showToastMessage(Messages.tryAgain)
            }
        }
    }
    
    func unlinkDropbox() {
        
        DropboxClientsManager.authorizedClient = nil
        DropboxOAuthManager.sharedOAuthManager.clearStoredAccessTokens()
        
        arrFiles = []
        APPDELEGATE.showToastMessage(Messages.unlinkDropboxAccount)
    }
    
    func uploadToDropbox(data: NSData) {
        
        APPDELEGATE.showProgressHUD()
        if let authorizedClient = DropboxClientsManager.authorizedClient {
            authorizedClient.files.upload(path: ConstantValues.BACKUP_FOLDERNAME + ConstantValues.ZIP_FILENAME + ConstantValues.zipExtension, input: data as Data)
                .response { response, error in
                    
                    APPDELEGATE.hideProgressHUD()
                    if let response = response {
                        DLog(response)
                        self.arrFiles.insert(response, at: 0)
                        _ = self.isDeleteBackupFolder(message: Messages.backupUnsuccessful)
                    } else if let error = error {
                        DLog(error)
                        APPDELEGATE.showToastMessage(Messages.backupUnsuccessful)
                    }
            }
        }
    }
    
    func downloadFromDropbox(path: String, index: Int) {
        
        if let authorizedClient = DropboxClientsManager.authorizedClient {
            
            APPDELEGATE.showProgressHUD()
            authorizedClient.files.download(path: path).response { response, error in
                APPDELEGATE.hideProgressHUD()
                
                if let response = response {
                    
                    if self.isStoreZipFileToBackupFolder(data: response.1) {
                        if self.isUnZipFile() {
                            if self.isCopyTxtFileToSqliteFile() {
                                if self.isDeleteBackupFolder(message: Messages.backupRestoredFail) {
                                    APPDELEGATE.showToastMessage(Messages.backupRestored)
                                }
                            }
                        }
                    }
                } else if let error = error {
                    DLog(error)
                    APPDELEGATE.showToastMessage(Messages.backupRestoredFail)
                }
            }
        }
    }
    
    func deleteFileFromDropbox(path: String, index: Int) {
        
        if let authorizedClient = DropboxClientsManager.authorizedClient {
            
            APPDELEGATE.showProgressHUD()
            
            authorizedClient.files.deleteV2(path: path).response { response, error in
                APPDELEGATE.hideProgressHUD()
                if let response = response {
                    DLog(response)
                    APPDELEGATE.showToastMessage(Messages.backupDeleted)
                    self.arrFiles.remove(at: index)
                } else if let error = error {
                    DLog(error)
                    APPDELEGATE.showToastMessage(Messages.backupDeletedFail)
                }
            }
        }
    }
    
    func getListOfBackupFromDropbox() {
        
//        let documentDirectory = Util.getDD()
//        let backupPath = documentDirectory.appendingPathComponent(BACKUP_FOLDERNAME)
//        let destination: (URL, HTTPURLResponse) -> URL = { temporaryURL, response in
//            return documentDirectory
//        }
        
        if let authorizedClient = DropboxClientsManager.authorizedClient {
            
            btnLinkDropbox.isSelected = true
            btnTakeBackup.isEnabled = true
            
            APPDELEGATE.showProgressHUD()
            
            authorizedClient.files.listFolder(path: ConstantValues.BACKUP_FOLDERNAME).response { response, error in
                APPDELEGATE.hideProgressHUD()
                if let response = response {
                    self.arrFiles = response.entries
                } else if let error = error {
                    DLog(error)
                }
            }
        } else {
            arrFiles = []
        }
    }
}

extension BackupVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.BackupTVC) as! BackupTVC
        
        cell.btnRestore.addTarget(self, action: #selector(btnRestoreTapped(_:)), for: .touchUpInside)
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteTapped(_:)), for: .touchUpInside)
        
        cell.btnRestore.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        
        let object = arrFiles[indexPath.row]
        cell.lblDate.text = object.name
        return cell
    }
}
extension BackupVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
