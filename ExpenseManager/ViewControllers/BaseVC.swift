//
//  BaseVC.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 11/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import SideMenu
import GoogleMobileAds

class BaseVC: UIViewController, UIGestureRecognizerDelegate {
    let dateFormatter = DateFormatter()
    @IBOutlet weak var adBannerView: AdmobBannerView!
    @IBOutlet weak var adsHeights: NSLayoutConstraint!
    var interstitial: GADInterstitial!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension BaseVC {
    
    func shareApp(){
        let urlString = kShareText
        let linkToShare = [urlString]
        let activityController = UIActivityViewController(activityItems: linkToShare, applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
    }
    
    
     func rateApp() {
        openUrl(kAppUrl)
    }
    func openMoreApp(){
       openUrl(kAppUrl)

    }
    func rateUSApp(id:String){
       openUrl("itms-apps://itunes.apple.com/app/\(id)")
    }
     func openUrl(_ urlString:String) {
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    func createAndLoadInterstitial() -> GADInterstitial {

        let request = GADRequest()
        interstitial = GADInterstitial(adUnitID: admobInterstitialID)
         request.testDevices = [kGADSimulatorID]
        interstitial.load(request)
        interstitial.delegate = self
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
    }
    
    
    func showbannerAds(){
        if (kdefaults.bool(forKey: kShowAds) && AppDelegate.sharedInstance.reachability.connection != .none){
            adsHeights.constant = 50
            displayBannerAds()
        }else{
            adsHeights.constant = 0
        }
    }
    
    func showAds(){
        if (kdefaults.bool(forKey: kShowAds)){
            adsHeights.constant = 50
        }
    }
    
    func hidebannerAds(){
        adsHeights.constant = 0
    }
    
    func displayBannerAds(){
        adBannerView.adsbannerView.adUnitID = admobBannerID
        adBannerView.adsbannerView.rootViewController = self
        let request = GADRequest()
        adBannerView.adsbannerView.load(request)
    }
    
    
    func setNavigationbarleft_title(_ left_imageName: [String] = [],
                                    left_action: [Selector] = [],
                                    right_imageName: [String] = [],
                                    right_action: [Selector] = [],
                                    title: String
        ){
        
        if self.navigationController != nil {
            
            if let themeColor = UserDefaultManager.getData(forKey: Parameters.UserDefault.ThemeColor) as? String {
                self.navigationController!.navigationBar.barTintColor = UIColor(hexString: themeColor)
            } else {
                self.navigationController!.navigationBar.barTintColor = UIColor(hexString: ConstantValues.DEFAULT_THEME_COLOR)
            }
            
            self.navigationController?.navigationBar.isTranslucent = false
            
            self.navigationController?.isNavigationBarHidden = false
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.rightBarButtonItem = nil
            self.navigationItem.titleView = UIView()
            
            let lblLine : UILabel = UILabel(frame: CGRect(x: 0,y: 0,width: SCREENWIDTH(),height: 0.2))
            lblLine.backgroundColor = UIColor.lightGray
            self.tabBarController?.tabBar.addSubview(lblLine)
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            
            if  left_imageName.count > 0 && left_imageName.count == left_action.count {
                
                for i in 0..<left_imageName.count {
                    
                    let btnleft =  UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 30))
                    btnleft.tag = i
                    btnleft.setTitleColor(UIColor.white, for: UIControlState.normal)
                    btnleft.setImage(Set_Local_Image(left_imageName[i]), for: UIControlState.normal)
                    btnleft.imageView?.contentMode = .scaleAspectFit
                    btnleft.addTarget(self, action: left_action[i], for: .touchDown)
                    btnleft.contentHorizontalAlignment = .left
                    btnleft.titleEdgeInsets.left = (btnleft.imageView?.frame.width)! - 5
                    let backBarButon: UIBarButtonItem = UIBarButtonItem(customView: btnleft)
                    if self.navigationItem.leftBarButtonItems == nil {
                        self.navigationItem.leftBarButtonItems = []
                    }
                    self.navigationItem.leftBarButtonItems?.append(backBarButon)
                }
                
            }else {
                self.navigationItem.hidesBackButton = true;
                self.navigationItem.leftBarButtonItem = nil;
                self.navigationItem.leftBarButtonItems = nil;
            }
            
            
            if  right_imageName.count > 0 && right_imageName.count == right_action.count {
                
                for i in 0..<right_imageName.count {
                    
                    let btnadd: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 30))
                    btnadd.layer.cornerRadius = btnadd.frame.width/2
                    btnadd.setImage(Set_Local_Image(right_imageName[i]), for: UIControlState.normal)
                    btnadd.addTarget(self, action: right_action[i], for: .touchDown)
                    let rightAdd: UIBarButtonItem = UIBarButtonItem(customView: btnadd)
                    
                    if self.navigationItem.rightBarButtonItems == nil {
                        self.navigationItem.rightBarButtonItems = []
                    }
                    self.navigationItem.rightBarButtonItems?.append(rightAdd)
                }
            } else {
                self.navigationItem.rightBarButtonItem = nil;
                self.navigationItem.rightBarButtonItems = nil;
            }
            
            if title.count > 0 {
                let lblTitle: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
                lblTitle.text = title
                lblTitle.textColor = Color.Navigation.navigationTitle
                lblTitle.font = FONT(FontName.roboto_bold.rawValue, size: FontSize.navigationTitle)
//                if SCREENHEIGHT() > smallDeviceHeight {
//                    lblTitle.font = FONT_BOLD(FontSize.large)
//                } else {
//                    lblTitle.font = FONT_BOLD(FontSize.medium)
//                }
                
                lblTitle.textAlignment = .center
                self.navigationItem.titleView = lblTitle
            }else{
                self.navigationItem.titleView = UIView()
            }
        }
    }
    
    @objc func btnBackTapped() {
        popVC()
    }
    
    @objc func btnDrawerTapped() {
       openDrawerMenu()
    }
    
    @objc func btnDashboard() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func popVC() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func doNothing(){}
    
    func openDrawerMenu() {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func getAttibutedString(attrs1Color: UIColor, attrs2Color: UIColor, string1: String, string2: String, font1: UIFont, font2: UIFont) -> NSAttributedString {
        
        let combination = NSMutableAttributedString()
        let attrs1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor :  attrs1Color]
        let attrs2 = [NSAttributedStringKey.font: font2, NSAttributedStringKey.foregroundColor : attrs2Color]
        
        let attributedString1 = NSMutableAttributedString(string:string1, attributes:attrs1)
        let attributedString2 = NSMutableAttributedString(string: string2, attributes:attrs2)
        combination.append(attributedString1)
        combination.append(attributedString2)
        
        return combination
    }
    func getDateFromString(dateFormat: String, strDate: String) -> Date? {
        
        dateFormatter.dateFormat = dateFormat
      
        return dateFormatter.date(from: strDate)
    }
    func getStringFromDate(dateFormat: String, date: Date) -> String {
        
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
}

extension BaseVC : GADInterstitialDelegate{
    //MARK: -  INTERSTITIAL AD DELEGATE
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        DLog("interstitialDidReceiveAd")
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
    }
    
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        DLog("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        DLog("interstitialWillPresentScreen")
    }
    
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        DLog("interstitialWillDismissScreen")
    }
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        DLog("interstitialWillLeaveApplication")
    }
    
}
