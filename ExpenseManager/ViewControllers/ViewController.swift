//
//  ViewController.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 19/01/17.
//  Copyright © 2017 Vimal Bhadra. All rights reserved.
//

import UIKit

class ViewController: BaseVC {
    
    @IBAction func btnClickTapped(_ sender: Any) {
        openDrawerMenu()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


