//
//  CurrencyVC.swift
//  ExpenseManager
//
//  Created by Piyu on 09/02/19.
//  Copyright © 2019 Jigar Parmar. All rights reserved.
//

import UIKit
import GoogleMobileAds
class CurrencyVC: BaseVC {
    @IBOutlet weak var tblCurrencylist: UITableView!
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var lblMessage: UILabel!

    var arrCurrencyList = [CurrencyDATA]()
    var arrsearchCurrencyList = [CurrencyDATA]()

    var selcurrencyInfo = CurrencyDATA()
    override func viewDidLoad() {
        super.viewDidLoad()
        adBannerView.adsbannerView.delegate = self
        showbannerAds()

        lblMessage.text = AppDelegate.sharedInstance.LocalizedString(text: "No record found")
         setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [ImageName.Navigation.navcorrect], right_action: [#selector(setCurrency)], title: AppDelegate.sharedInstance.LocalizedString(text: Title.SelectCurrency))
        tblCurrencylist.tableFooterView = UIView(frame: .zero)
    }
    override func viewWillAppear(_ animated: Bool) {
        arrCurrencyList.removeAll()
        arrsearchCurrencyList.removeAll()
        let tempCurrency = arrayFromContentsOfFileWithName(fileName: "currency")
        
        for i in 0..<tempCurrency!.count{
            let fullName    = tempCurrency![i]
            let fullNameArr = fullName.components(separatedBy: ",")
            
            let currncyFName = fullNameArr[0]
            let currncySName = fullNameArr[1]
            let currncySymbol = fullNameArr[2]
            
            var tempCurrencyData : CurrencyDATA = CurrencyDATA()
            tempCurrencyData.cURRENCYFNAME = currncyFName
            tempCurrencyData.cURRENCYSNAME = currncySName
            tempCurrencyData.cURRENCYSYMBOL = currncySymbol
            arrCurrencyList.append(tempCurrencyData)
            arrsearchCurrencyList = arrCurrencyList
        }
        if arrCurrencyList.count > 0 {
            self.tblCurrencylist.reloadData()
            self.arrCurrencyList.enumerated().forEach { (index,object) in
                if let cSNAME = object.cURRENCYSNAME, cSNAME == AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSNAME!{
                    let indexPath = IndexPath(row: index , section: 0)
                    self.tblCurrencylist.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.top, animated: true)

                }
            }
        }
        selcurrencyInfo.cURRENCYSNAME = AppDelegate.sharedInstance.getCurrencyDetails()?.cURRENCYSNAME
    }
    
    //#MARK: SetData
    func setData() {
        searchbar.text = ""
        runOnMainThread {
            if self.lblMessage != nil, self.tblCurrencylist != nil {
                self.lblMessage.isHidden = (self.arrCurrencyList.count > 0) ? true : false
                self.tblCurrencylist.isHidden = !self.lblMessage.isHidden
                self.tblCurrencylist.reloadData()
            }
        }
    }

}
extension CurrencyVC{
    @objc func setCurrency() {
        if self.selcurrencyInfo.cURRENCYSYMBOL != nil{
             AppDelegate.sharedInstance.setCurrencyDetails(selcurrencyInfo)
             self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func arrayFromContentsOfFileWithName(fileName: String) -> [String]? {
        guard let path = Bundle.main.path(forResource: fileName, ofType: "txt") else {
            return nil
        }
        
        do {
            let content = try String(contentsOfFile:path, encoding: String.Encoding.utf8)
            return content.components(separatedBy: "\n")
        } catch _ as NSError {
            return nil
        }
    }
}
extension CurrencyVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCurrencyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.CurrencyTVC) as! CurrencyTVC
        let objectData = arrCurrencyList[indexPath.row]
        cell.lblCurencyName.text = "\(objectData.cURRENCYSNAME!) - \(objectData.cURRENCYFNAME!) - \(objectData.cURRENCYSYMBOL!)"
        
        if objectData.cURRENCYSNAME == selcurrencyInfo.cURRENCYSNAME {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
            
        }
        let objectData = arrCurrencyList[indexPath.row]
        selcurrencyInfo.cURRENCYSNAME = objectData.cURRENCYSNAME!
        selcurrencyInfo.cURRENCYSYMBOL = objectData.cURRENCYSYMBOL!
        tblCurrencylist.reloadData()
    }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//
//        if let cell = tableView.cellForRow(at: indexPath) {
//            cell.accessoryType = .none
//        }
//    }
//
}
extension CurrencyVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count > 0 {
            self.arrCurrencyList = arrsearchCurrencyList.filter({ (object) -> Bool in
                if let strFName = object.cURRENCYFNAME , let strSName = object.cURRENCYSNAME , let strSymbol = object.cURRENCYSYMBOL{
                    if strFName.lowercased().contains(searchText.lowercased()) || strSName.lowercased().contains(searchText.lowercased())  || strSymbol.lowercased().contains(searchText.lowercased())  {
                        return true
                    }
                }
                return false
            })
        }
        else {
            self.arrCurrencyList = arrsearchCurrencyList
        }
        if self.arrCurrencyList.count > 0 {
            self.lblMessage.isHidden = true
            self.tblCurrencylist.isHidden = false
            self.tblCurrencylist.reloadData()
        }else{
            self.lblMessage.isHidden = false
            self.tblCurrencylist.isHidden = true
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchBar.text = ""
        arrCurrencyList = arrsearchCurrencyList
        setData()
    }
}
extension CurrencyVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
