//
//  AddEditTransactionVC.swift
//  ExpenseManager
//
//  Created by Uffizio iMac on 28/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import GoogleMobileAds
import IQKeyboardManagerSwift

struct TransactionData {
    var strDisc: String
    var strValue: String
}



class AddEditTransactionVC: BaseVC {

    @IBOutlet weak var textViewNotes: UITextView!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var tblTransaction: UITableView!
    @IBOutlet weak var btnIncome: UIButton!
    @IBOutlet weak var btnExpense: UIButton!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var const_viewSeparator_leading: NSLayoutConstraint!
    @IBOutlet weak var viewAddExpense: UIView!

    var selectedIndex: Int = 1
    var selectedAccount: Int32 = 1
    var selectedAccounName: String = ""

    var arrTransactionData = [TransactionData]() {
        didSet {
            //reloadData()
        }
    }
    var arrPaymentType = [String]()
    var arrRecurrentType = [String]()
    var arrAccount = [AccountInfo]()
    var arrTransaction = [TransactionInfo]()
    var arrTransactionInfo: TransactionInfo = TransactionInfo()
    var arrRecurrentTransactionInfo: RecurrentInfo = RecurrentInfo()
    var isBoolEdit : Bool = false
    var arrDashboard = [DashboardInfo]()
    var isAddRecurrent : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        adBannerView.adsbannerView.delegate = self

        if self.textViewNotes != nil {
            self.textViewNotes.layer.cornerRadius = 5.0
            self.textViewNotes.layer.borderWidth = 1.0
            self.textViewNotes.layer.borderColor = Color.textColor.textColorGrey
        }
     
        initialSetup()

    }
    override func viewWillAppear(_ animated: Bool) {
        showbannerAds()
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = AppDelegate.sharedInstance.LocalizedString(text: Title.Alert.Done)

        viewAddExpense.backgroundColor = UIColor(hexString: getSelectedThemeColor())
        btnIncome.setTitle(AppDelegate.sharedInstance.LocalizedString(text: Messages.INCOME), for: .normal)
        btnExpense.setTitle(AppDelegate.sharedInstance.LocalizedString(text: Messages.EXPENSE), for: .normal)
        self.lblPlaceHolder.isHidden = textViewNotes.text == "" ? false: true
    }
   
}
extension AddEditTransactionVC {
    
    func initialSetup() {
        
        view.layoutIfNeeded()
        
        if isBoolEdit {
            
               setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [ImageName.Navigation.navcorrect, ImageName.Navigation.navdelete], right_action: [#selector(addTransacation), #selector(deleteTransacation)], title: AppDelegate.sharedInstance.LocalizedString(text: Title.EditTransaction))
            
            arrTransactionInfo.transactionId = arrDashboard[0].transactionId!
        }else{
             setNavigationbarleft_title([ImageName.Navigation.arrow_back], left_action: [#selector(btnBackTapped)], right_imageName: [ImageName.Navigation.navcorrect], right_action: [#selector(addTransacation)], title: AppDelegate.sharedInstance.LocalizedString(text: Title.AddTransaction))
        }
        
        textViewNotes.text = isBoolEdit ? arrDashboard[0].transactionNote! : ""
       
        if let accountInfo = UserDefaultManager.getAccountInfo() {
            selectedAccount = isBoolEdit ? arrDashboard[0].transactionAccountId! : accountInfo.id!
            arrTransactionInfo.transactionAccountId = isBoolEdit ? arrDashboard[0].transactionAccountId! : accountInfo.id!
            arrTransactionInfo.transactionNote =  isBoolEdit ? arrDashboard[0].transactionNote! : ""
            selectedAccounName = accountInfo.name!
        }
        if isBoolEdit {
            if arrDashboard[0].transactionType?.lowercased() == "income"{
                btnMenuTapped(btnIncome)

            }else{
                btnMenuTapped(btnExpense)
            }

        }else{
            btnMenuTapped(btnExpense)
        }
        initializePaymenType()
        initializeRecurrentType()
         getAllAccount()
        initilizeTransactionData()
        reloadData()
    }
    func initilizeTransactionData(){
        
        
        if isBoolEdit{
            
            let tempStrAmount = arrDashboard[0].transactionAmount!
            
//            if let dotRange = tempStrAmount.range(of: ".") {
//                tempStrAmount.removeSubrange(dotRange.lowerBound..<tempStrAmount.endIndex)
//            }
            arrTransactionData.append(TransactionData(strDisc: "AMOUNT:", strValue: tempStrAmount))

            arrTransactionInfo.transactionAmount = tempStrAmount
            let tempDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: arrDashboard[0].transactionDate!)
            arrTransactionData.append(TransactionData(strDisc: "DATE:", strValue: getStringFromDate(dateFormat: DateFormat.dayDateMonthYear, date: tempDate!)))
            arrTransactionInfo.transactionDate = getStringFromDate(dateFormat: DateFormat.yearmonthday, date: tempDate!)
        }else{
           
            arrTransactionData.append(TransactionData(strDisc: "AMOUNT:", strValue: ""))

            arrTransactionData.append(TransactionData(strDisc: "DATE:", strValue: Date().getStringFromDate(dateFormat: DateFormat.dayDateMonthYear)))
            arrTransactionInfo.transactionDate = Date().getStringFromDate(dateFormat: DateFormat.yearmonthday)
        }
        
    
        arrTransactionData.append(TransactionData(strDisc: "CATEGORY:", strValue: isBoolEdit ? arrDashboard[0].categoryName! :"Category Name"))
        arrTransactionInfo.transactionCatId = isBoolEdit ? arrDashboard[0].transactionCatId! : 0
        arrTransactionData.append(TransactionData(strDisc: "PAYMENT TYPE:", strValue: isBoolEdit ? arrDashboard[0].transactionPayType! : "CASH"))
        arrTransactionInfo.transactionPayType = isBoolEdit ? arrDashboard[0].transactionPayType : "CASH"
        if !isBoolEdit{
           arrTransactionData.append(TransactionData(strDisc: "RECURRENT TYPE:", strValue: "NEVER"))
        }
        
        arrTransactionData.append(TransactionData(strDisc: "ACCOUNT:", strValue: String(selectedAccounName)))
        
    }
    func initializePaymenType(){
        arrPaymentType = ["CASH","CHEQUE","BANK TRANSFER","CREDIT CARD","DEBIT CARD"]
        arrTransactionInfo.transactionPayType = isBoolEdit ? arrDashboard[0].transactionPayType! : "CASH"
    }
    func initializeRecurrentType(){
        arrRecurrentType = ["NEVER","DAILY","WEEKLY","MONTHLY"]
    }
    
    func getAllAccount(){
        arrAccount = AccountInfo.getAllAccount()
    }
    
    func reloadData() {
        runOnMainThread {
            self.tblTransaction.reloadData()
        }
    }
    
    
    func getMenuButtonFromIndex() -> UIButton? {
        
        let views: [UIButton] = [btnIncome, btnExpense]
        
        let arrFilter = views.filter { (button) -> Bool in
            return button.tag == selectedIndex
        }
        
        if arrFilter.count > 0 {
            return arrFilter[0]
        }
        
        return nil
    }
    
    func animateCollectionView(sender: UIButton) {
        
        runOnMainThread {
            self.tblTransaction.setContentOffset(CGPoint(x: self.tblTransaction.frame.width * CGFloat(sender.tag), y: 0), animated: false)
        }
    }
    
    func selectMenu(sender: UIButton) {
        
        let views: [UIButton] = [btnIncome, btnExpense]
        views.enumerated().forEach { (index, view) in
            view.isSelected = (view == sender) ? true : false
        }
        
        runOnMainThread {
            self.const_viewSeparator_leading.constant = (sender.superview?.frame.origin.x)!
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func validationCheckTransacation()->Bool{
        let validation = true
        if arrTransactionInfo.transactionAmount == "0" || arrTransactionInfo.transactionAmount == "" || arrTransactionInfo.transactionAmount == nil{
            APPDELEGATE.showToastMessage(AppDelegate.sharedInstance.LocalizedString(text: Messages.enteramount), wantBottomSide: true, hideAutomatically: true)

            return false
        }else if arrTransactionInfo.transactionCatId == nil || arrTransactionInfo.transactionCatId == 0{
             APPDELEGATE.showToastMessage(AppDelegate.sharedInstance.LocalizedString(text: Messages.selectcategory), wantBottomSide: true, hideAutomatically: true)
            return false

        }else{
            return validation
        }
    }
    


}

extension AddEditTransactionVC {
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            arrTransactionInfo.transactionType = "INCOME"
        }else{
            arrTransactionInfo.transactionType = "EXPENSE"
        }
        selectMenu(sender: sender)
        //animateCollectionView(sender: sender)
    }
    
  @objc func addTransacation() {
        let validationCheck = validationCheckTransacation()
        if validationCheck {
            arrTransactionInfo.transactionAmount = arrTransactionInfo.transactionAmount!
            
            if isBoolEdit{
                if TransactionInfo.editTransaction(transactionInfo: arrTransactionInfo){
                    APPDELEGATE.showToastMessage(AppDelegate.sharedInstance.LocalizedString(text: Messages.TransactionUpdate), wantBottomSide: true, hideAutomatically: true)
                    AppDelegate.sharedInstance.adscount = AppDelegate.sharedInstance.adscount + 1
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                if TransactionInfo.addTransaction(transactionInfo: arrTransactionInfo){
                    APPDELEGATE.showToastMessage(AppDelegate.sharedInstance.LocalizedString(text: Messages.TransactionSuccess), wantBottomSide: true, hideAutomatically: true)
                    if !isAddRecurrent{
                        AppDelegate.sharedInstance.adscount = AppDelegate.sharedInstance.adscount + 1
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                if isAddRecurrent{
                    arrRecurrentTransactionInfo.recurrentAmount = arrTransactionInfo.transactionAmount?.replacingOccurrences(of: ",", with: ".")
                    arrRecurrentTransactionInfo.recurrentCatId = arrTransactionInfo.transactionCatId
                    arrRecurrentTransactionInfo.recurrentPayType = arrTransactionInfo.transactionPayType//cash
                    arrRecurrentTransactionInfo.recurrentType = self.arrTransactionData[4].strValue//daily
                    arrRecurrentTransactionInfo.recurrentAccountId = arrTransactionInfo.transactionAccountId
                    arrRecurrentTransactionInfo.recurrentNote = arrTransactionInfo.transactionNote
                    arrRecurrentTransactionInfo.recurrentTransactionType = arrTransactionInfo.transactionType//income-expense
                    arrRecurrentTransactionInfo.recurrentNextDate = APPDELEGATE.getNextDate(strRecurrentType: self.arrTransactionData[4].strValue, strDate: arrTransactionInfo.transactionDate!)
                    if RecurrentInfo.addRecurrentTransaction(recurrentTransactionInfo: arrRecurrentTransactionInfo){
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
         }
    }
    @objc func deleteTransacation() {
        APPDELEGATE.showAlert(title:AppDelegate.sharedInstance.LocalizedString(text: Title.Alert.Delete) , message: Messages.deleteTransactionPermission, actions: [AppDelegate.sharedInstance.LocalizedString(text: Title.Alert.CANCEL), AppDelegate.sharedInstance.LocalizedString(text: Title.Alert.DELETE)]) { (index) in
            if index == 1 {
                
                if TransactionInfo.deleteTransacation(transactionID: self.arrDashboard[0].transactionId!){
                    DLog("Transaction Delete")
                    APPDELEGATE.showToastMessage(AppDelegate.sharedInstance.LocalizedString(text: Messages.TransactionDelete), wantBottomSide: true, hideAutomatically: true)
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
        }
    }
}

extension AddEditTransactionVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTransactionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.AddEditAmountTVC) as! AddEditAmountTVC
            cell.lblDisc.text = arrTransactionData[indexPath.row].strDisc
            cell.lblCurrencySymbol.text = AppDelegate.sharedInstance.getCurrencyDetails()?.cURRENCYSYMBOL!
            cell.txtAmountValue.text = arrTransactionData[indexPath.row].strValue
            cell.txtAmountValue.keyboardType = .decimalPad
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.AddEditTVC) as! AddEditTVC
            cell.lblDisc.text = arrTransactionData[indexPath.row].strDisc
            cell.lblValue.text = arrTransactionData[indexPath.row].strValue

            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var nextVC :CategoryManagmentVC? = nil
         self.view.endEditing(true)
        if indexPath.row == 1 {
            let datePickerView = DatePickerView.instanceFromNib()
            datePickerView.delegate = self
            datePickerView.selectedDate = arrTransactionData[indexPath.row].strValue.getDateFromString(dateFormat: DateFormat.dayDateMonthYear)
            datePickerView.showInAppWindow()
        }else if indexPath.row == 2 {
            nextVC = (loadVC(Identifier.Storyboard.SB_Category, strVCId: Identifier.ViewController.CategoryManagmentVC) as! CategoryManagmentVC)
            nextVC!.delegate = self
            navigationController?.pushViewController(nextVC!, animated: true)
        }else if indexPath.row == 3{
            let vc = PickerViewVC(nibName: "PickerViewVC", bundle: nil)
            vc.delegate = self
            vc.arrPaymentType = arrPaymentType
            vc.selcatedValue = arrTransactionData[3].strValue
            vc.selctedType = TransactionType(rawValue: 0)!
            if let rootViewController = APPDELEGATE.window?.rootViewController {
                var frame: CGRect = vc.view.frame
                if let presentedViewController = rootViewController.presentedViewController {
                    frame = rootViewController.view.frame
                    presentedViewController.view.addSubview(vc.view)
                    presentedViewController.addChildViewController(vc)
                } else {
                    frame = rootViewController.view.frame
                    rootViewController.view.addSubview(vc.view)
                    rootViewController.addChildViewController(vc)
                }
                vc.view.frame = frame
            }
        }else if indexPath.row == 4{
            
            if isBoolEdit{
                let vc = PickerViewVC(nibName: "PickerViewVC", bundle: nil)
                vc.delegate = self
                vc.arrAccount = arrAccount
                vc.selcatedValue = arrTransactionData[4].strValue
                vc.selctedType = TransactionType(rawValue: 2)!
                if let rootViewController = APPDELEGATE.window?.rootViewController {
                    var frame: CGRect = vc.view.frame
                    if let presentedViewController = rootViewController.presentedViewController {
                        frame = rootViewController.view.frame
                        presentedViewController.view.addSubview(vc.view)
                        presentedViewController.addChildViewController(vc)
                    } else {
                        frame = rootViewController.view.frame
                        rootViewController.view.addSubview(vc.view)
                        rootViewController.addChildViewController(vc)
                    }
                    vc.view.frame = frame
                }
            }else{
                let vc = PickerViewVC(nibName: "PickerViewVC", bundle: nil)
                vc.delegate = self
                vc.arrRecurrentType = arrRecurrentType
                vc.selcatedValue = arrTransactionData[4].strValue
                vc.selctedType = TransactionType(rawValue: 1)!
                if let rootViewController = APPDELEGATE.window?.rootViewController {
                    var frame: CGRect = vc.view.frame
                    if let presentedViewController = rootViewController.presentedViewController {
                        frame = rootViewController.view.frame
                        presentedViewController.view.addSubview(vc.view)
                        presentedViewController.addChildViewController(vc)
                    } else {
                        frame = rootViewController.view.frame
                        rootViewController.view.addSubview(vc.view)
                        rootViewController.addChildViewController(vc)
                    }
                    vc.view.frame = frame
                }
            }
            
           
        }else if indexPath.row == 5{
            let vc = PickerViewVC(nibName: "PickerViewVC", bundle: nil)
            vc.delegate = self
            vc.arrAccount = arrAccount
            vc.selcatedValue = arrTransactionData[5].strValue
            vc.selctedType = TransactionType(rawValue: 2)!
            if let rootViewController = APPDELEGATE.window?.rootViewController {
                var frame: CGRect = vc.view.frame
                if let presentedViewController = rootViewController.presentedViewController {
                    frame = rootViewController.view.frame
                    presentedViewController.view.addSubview(vc.view)
                    presentedViewController.addChildViewController(vc)
                } else {
                    frame = rootViewController.view.frame
                    rootViewController.view.addSubview(vc.view)
                    rootViewController.addChildViewController(vc)
                }
                vc.view.frame = frame
            }
        }
        
        

    }
}

extension AddEditTransactionVC : DatePickerViewDelegate {
    
    func getDates(dates: (Date, Date, Bool)) {
    
    }
    
    func getSelectedDate(date: Date) {
        arrTransactionData[1].strValue = date.getStringFromDate(dateFormat: DateFormat.dayDateMonthYear)
        arrTransactionInfo.transactionDate = date.getStringFromDate(dateFormat: DateFormat.yearmonthday)
        self.tblTransaction.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)

    }
}
extension AddEditTransactionVC : PickerViewDelegate{
   
    func getSelectedPaymentType(strPType: String) {
        arrTransactionData[3].strValue = strPType
        arrTransactionInfo.transactionPayType = strPType
        self.tblTransaction.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .automatic)
    }
    
    func getSelectedRecurrentType(strRType: String, selectedIndex: Int) {
        
        
        if selectedIndex == 1 || selectedIndex == 2 || selectedIndex == 3 {
           
            if kdefaults.bool(forKey: kshowpopupRecurrent){
                let alert = UIAlertController(title:AppDelegate.sharedInstance.LocalizedString(text: Title.Alert.Alert), message: AppDelegate.sharedInstance.LocalizedString(text: Messages.repeatTransactionPermission), preferredStyle: UIAlertControllerStyle.alert)
                 arrTransactionData[4].strValue = strRType
                self.tblTransaction.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
                alert.addAction(UIAlertAction(title: AppDelegate.sharedInstance.LocalizedString(text: Title.Alert.OK), style: UIAlertActionStyle.default, handler:{ action in
                    self.isAddRecurrent = true
                }))
                alert.addAction(UIAlertAction(title: AppDelegate.sharedInstance.LocalizedString(text:Title.Alert.CANCEL), style: UIAlertActionStyle.default, handler: { action in
                    self.isAddRecurrent = false
                    self.arrTransactionData[4].strValue = "NEVER"
                    self.tblTransaction.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
                }))
                alert.addAction(UIAlertAction(title: AppDelegate.sharedInstance.LocalizedString(text: Title.Alert.Dontshowagain), style: UIAlertActionStyle.default, handler: { action in
                    kdefaults.set(false, forKey: kshowpopupRecurrent)
                    self.isAddRecurrent = true
                    self.arrTransactionData[4].strValue = strRType
                    self.tblTransaction.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                self.isAddRecurrent = true
                arrTransactionData[4].strValue = strRType
                self.tblTransaction.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
            }
   
        }else{
            arrTransactionData[4].strValue = strRType
            self.tblTransaction.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
        }

      
        
    }
    
    func getSelectedAccount(strAType: String,accountID: Int32) {
        if isBoolEdit {
            arrTransactionData[4].strValue = strAType

        }else{
            arrTransactionData[5].strValue = strAType

        }
        arrTransactionInfo.transactionAccountId = accountID
        self.tblTransaction.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
    }
    
    
}

extension AddEditTransactionVC: CategoryViewDelegate{
    
    func selectedCategory(_ categoryInfo: CategoryManagment) {
        arrTransactionData[2].strValue = categoryInfo.categoryName!
        arrTransactionInfo.transactionCatId = categoryInfo.categoryId
        self.tblTransaction.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
    }
}
extension AddEditTransactionVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                return true
            }
        }
        var updatedText = ""
        if let text = textField.text, let textRange = Range(range, in: text) {
            
            updatedText = text.replacingCharacters(in: textRange,
                                                   with: string)
            
            if !updatedText.containsEmoji {
                
                let allowedCharacters = CharacterSet(charactersIn:"0123456789.,")//Here change this characters based on your requirement
                let characterSet = CharacterSet(charactersIn: string)
                if allowedCharacters.isSuperset(of: characterSet) {
                    updatedText = updatedText.replacingOccurrences(of: ",", with: ".")
                    if updatedText.count > 10 || (Double(updatedText) == nil){
                        return false
                    }
                    arrTransactionInfo.transactionAmount = updatedText
                    return true
                }
            }
        }
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension AddEditTransactionVC : UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        lblPlaceHolder.isHidden = true
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
            lblPlaceHolder.isHidden = true
            arrTransactionInfo.transactionNote =  textView.text!

        }else{
            lblPlaceHolder.isHidden = false
            arrTransactionInfo.transactionNote =  textView.text!
        }
     }
}
extension AddEditTransactionVC : GADBannerViewDelegate{
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        showAds()
        DLog("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        hidebannerAds()
        DLog("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        DLog("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        DLog("adViewWillLeaveApplication")
    }
    
}
