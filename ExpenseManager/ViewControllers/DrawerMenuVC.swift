//
//  DrawerMenuVC.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 11/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import MessageUI

struct DrawerMenuData {
    
    var imageName: String
    var title: String
    var identifierVC: String
    var storyboardID: String
    var isAddSubView: Bool
    var isEmail: Bool
    var isShareApp: Bool
    var isMoreApp: Bool
    var accountInfo: AccountInfo?
    
    init(imageName: String, title: String, identifierVC: String, storyboardID: String = Identifier.Storyboard.Main, isAddSubView: Bool = false,isEmail: Bool = false,isShareApp: Bool = false,isMoreApp: Bool = false, accountInfo: AccountInfo? = nil) {
        self.imageName = imageName
        self.title = title
        self.identifierVC = identifierVC
        self.storyboardID = storyboardID
        self.isAddSubView = isAddSubView
        self.isEmail = isEmail
        self.isShareApp = isShareApp
        self.isMoreApp = isMoreApp
        self.accountInfo = accountInfo

    }
}

struct SectionData {
    var sectionName: String
    var arrMenuData: [DrawerMenuData]
}

class DrawerMenuVC: BaseVC {

    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var btnHeader: UIButton!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var const_mainView_Top: NSLayoutConstraint!


    var arrSectionData = [SectionData]() {
        didSet {
            reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        initialSetup()
    }

    override func viewWillAppear(_ animated: Bool) {
        viewHeader.backgroundColor = UIColor(hexString: getSelectedThemeColor())
        fillArrayWithData()
        setActiveAccount()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension DrawerMenuVC {
    
    func initialSetup() {
        
        view.layoutIfNeeded()
        self.navigationController?.isNavigationBarHidden = true
        btnHeader.titleEdgeInsets = .init(top: 0, left: -20, bottom: 0, right: 0)
        btnHeader.imageEdgeInsets = .init(top: 0, left: tblMenu.frame.width - 155, bottom: 0, right: 0)
        
        if isiPhoneX {
            const_mainView_Top.constant = -50
            viewHeader.frame = CGRect(x: viewHeader.frame.origin.x, y: viewHeader.frame.origin.y, width: viewHeader.frame.width, height: viewHeader.frame.height + 30)
        }
    }
    
    func setActiveAccount() {
        
        let attibutedString = getAttibutedString(attrs1Color: UIColor.white, attrs2Color: UIColor.darkGray, string1: "Expense Manager", string2: "\n\(UserDefaultManager.getAccountInfo()?.name ?? "")", font1: FONT(FontName.roboto_regular.rawValue, size: FontSize.title), font2: FONT(FontName.roboto_medium.rawValue, size: FontSize.subTitle))
        btnHeader.setAttributedTitle(attibutedString, for: .normal)
    }
    
    func fillArrayWithData() {
        
        if btnHeader.isSelected {
            setAllAccountInfo()
        } else {
            
            arrSectionData = [SectionData(sectionName: "", arrMenuData: [DrawerMenuData(imageName: ImageName.DrawerMenu.ic_analytics, title: "Analytics", identifierVC: Identifier.ViewController.AnalyticsVC),
                                                                      DrawerMenuData(imageName: ImageName.DrawerMenu.ic_charts, title: "Charts", identifierVC: Identifier.ViewController.ChartsVC,storyboardID: Identifier.Storyboard.SB_Charts),
                                                                      DrawerMenuData(imageName: ImageName.DrawerMenu.ic_recurrent, title: "Recurrent Transactions", identifierVC: Identifier.ViewController.RecurrentTransactionListVC,storyboardID: Identifier.Storyboard.SB_RecurrentTransaction),
                                                                      DrawerMenuData(imageName: ImageName.DrawerMenu.ic_accounts, title: "Accounts", identifierVC: Identifier.ViewController.ManageAccountVC),
                                                                      DrawerMenuData(imageName: ImageName.DrawerMenu.ic_backup, title: "Backup & Restore", identifierVC: Identifier.ViewController.BackupVC, storyboardID: Identifier.Storyboard.Setting),
                                                                      DrawerMenuData(imageName: ImageName.DrawerMenu.ic_category, title: "Category Management", identifierVC: Identifier.ViewController.CategoryManagmentVC,storyboardID: Identifier.Storyboard.SB_Category),
                                                                      DrawerMenuData(imageName: ImageName.DrawerMenu.ic_currency, title: "Currency", identifierVC: Identifier.ViewController.CurrencyVC,storyboardID: Identifier.Storyboard.SB_Currency),
                                                                      DrawerMenuData(imageName: ImageName.DrawerMenu.ic_settings_drawer, title: "Settings", identifierVC: Identifier.ViewController.SettingsVC, storyboardID: Identifier.Storyboard.Setting),
                                                                      DrawerMenuData(imageName: ImageName.DrawerMenu.ic_pro, title: "Upgrade to Pro (ad-free version)", identifierVC: Identifier.ViewController.ProVersionView, isAddSubView: true)]),
                              SectionData(sectionName: "Others", arrMenuData: [DrawerMenuData(imageName: ImageName.DrawerMenu.ic_email, title: "Email Us", identifierVC: Identifier.ViewController.AnalyticsVC,isEmail: true),
                                                                            DrawerMenuData(imageName: ImageName.DrawerMenu.ic_share, title: "Share App", identifierVC: Identifier.ViewController.AnalyticsVC,isShareApp: true),
                                                                            DrawerMenuData(imageName: ImageName.DrawerMenu.ic_more, title: "More Apps", identifierVC: Identifier.ViewController.AnalyticsVC,isMoreApp: true)])
            ]
        }
    }
    
    func setAllAccountInfo() {
        
        let allAccount = AccountInfo.getAllAccount()
        var sectionData = SectionData(sectionName: "", arrMenuData: [])
        if allAccount.count > 0 {
            var arrMenuData = [DrawerMenuData]()
            allAccount.enumerated().forEach { (index, object) in
                let drawerMenuData = DrawerMenuData(imageName: "", title: "", identifierVC: Identifier.ViewController.ExpenseManagerVC, isAddSubView: false, accountInfo: object)
                arrMenuData.append(drawerMenuData)
            }
            sectionData.arrMenuData = arrMenuData
        }
        
        sectionData.arrMenuData.append(contentsOf: [DrawerMenuData(imageName: ImageName.DrawerMenu.ic_add_drawer, title: "Add new account", identifierVC: Identifier.ViewController.AddAccountView, isAddSubView: true),
                                                    DrawerMenuData(imageName: ImageName.DrawerMenu.ic_settings_drawer, title: "Manage accounts", identifierVC: Identifier.ViewController.ManageAccountVC)])
        
        arrSectionData = [sectionData]
    }
    
    func reloadData() {
        runOnMainThread {
            self.tblMenu.reloadData()
        }
    }
}

extension DrawerMenuVC {
    
    @IBAction func btnHeaderTapped(_ sender: UIButton) {
        btnHeader.isSelected = !btnHeader.isSelected
        fillArrayWithData()
    }
}

extension DrawerMenuVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSectionData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSectionData[section].arrMenuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.Cell.DrawerMenuTVC) as! DrawerMenuTVC
        let object = arrSectionData[indexPath.section].arrMenuData[indexPath.row]
        cell.imgViewIcon.image = UIImage(named: object.imageName)
        cell.lblTitle.text = object.title
        tableView.separatorStyle = (indexPath.row == arrSectionData[indexPath.section].arrMenuData.count - 1) ? .none : .singleLine
        
        if btnHeader.isSelected {
            
            if indexPath.row == arrSectionData[indexPath.section].arrMenuData.count - 1 || indexPath.row == arrSectionData[indexPath.section].arrMenuData.count - 2 {
                cell.lblCircleName.isHidden = true
                cell.imgViewIcon.isHidden = false
            } else {
                cell.imgViewIcon.isHidden = true
                cell.lblCircleName.isHidden = false
                if let accountInfo = object.accountInfo {
                    cell.lblTitle.text = accountInfo.name
                    cell.lblCircleName.text = "\(accountInfo.name!.first ?? " ")"
                }
                if cell.lblCircleName.backgroundColor == nil {
                    cell.lblCircleName.backgroundColor = UIColor.random()
                }
            }
            
        } else {
            cell.lblCircleName.isHidden = true
            cell.imgViewIcon.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            
            let sectionHeaderView = UIView(frame: tableView.frame)
            
            let separatorView = UIView(frame: CGRect(x: 10, y: 0, width: sectionHeaderView.frame.width - 20, height: 0.5))
            separatorView.backgroundColor = UIColor.black
            sectionHeaderView.addSubview(separatorView)
            
            let lblTitle = UILabel(frame: CGRect(x: 10, y: 10, width: sectionHeaderView.frame.width - 20, height: 30))
            lblTitle.font = UIFont.systemFont(ofSize: 16)
            lblTitle.text = "Others"
            sectionHeaderView.addSubview(lblTitle)
            return sectionHeaderView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1 {
            return 40
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = arrSectionData[indexPath.section].arrMenuData[indexPath.row]
        if let accountInfo = object.accountInfo {
            UserDefaultManager.setAccountInfo(accountInfo)
        }
        
        if object.isAddSubView {
            
            if let addAccountView = instanceFromNib(nibName: object.identifierVC) as? AddAccountView {
                
                addAccountView.delegate = self
                addAccountView.showInAppWindow()
                
            } else if let proVersionView = instanceFromNib(nibName: object.identifierVC) as? ProVersionView {
                
                runOnMainThread {
                    self.dismiss(animated: true) {
                        proVersionView.showInAppWindow()
                    }
                }
            }
          
        }else if object.isEmail{
            let picker = MFMailComposeViewController()
            picker.mailComposeDelegate = self
            picker.setMessageBody(kShareText, isHTML: true)
            present(picker, animated: true, completion: nil)

        }else if object.isShareApp{
           shareApp()

        }else if object.isMoreApp{
            openMoreApp()
            
        }else if object.identifierVC.lowercased() == Identifier.ViewController.ChartsVC.lowercased(){
            let viewController = (loadVC(object.storyboardID, strVCId: object.identifierVC)as! ChartsVC)
            self.navigationController?.pushViewController(viewController, animated: true)
        }else if object.identifierVC.lowercased() == Identifier.ViewController.CategoryManagmentVC.lowercased(){
            let viewController = (loadVC(object.storyboardID, strVCId: object.identifierVC)as! CategoryManagmentVC)
            viewController.isBoolEdit = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }else if object.identifierVC.lowercased() == Identifier.ViewController.RecurrentTransactionListVC.lowercased(){
            let viewController = (loadVC(object.storyboardID, strVCId: object.identifierVC)as! RecurrentTransactionListVC)
            self.navigationController?.pushViewController(viewController, animated: true)
        } else if object.identifierVC.lowercased() == Identifier.ViewController.CurrencyVC.lowercased(){
            let viewController = (loadVC(object.storyboardID, strVCId: object.identifierVC)as! CurrencyVC)
            self.navigationController?.pushViewController(viewController, animated: true)
        }else {
            let viewController = loadVC(object.storyboardID, strVCId: object.identifierVC)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension DrawerMenuVC: AddAccountViewDelegate {
    
    func accountUpdated(_ accountInfo: AccountInfo) {
        
    }
    
    func refreshAccountData() {
        setAllAccountInfo()
    }
}
extension DrawerMenuVC:MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}
