//
//  AppDelegate.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 19/01/17.
//  Copyright © 2017 Vimal Bhadra. All rights reserved.
//

import UIKit
import SideMenu
import SwiftyDropbox
import IQKeyboardManagerSwift
import UserNotifications
import GoogleMobileAds
import Reachability
import SVProgressHUD

struct categorylist {
    var name: String
    var iconname: String
    var count: String
    var accountId: Int32
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   let dateFormatter = DateFormatter()
    var window: UIWindow?
    let reachability = Reachability()!
    var completionHadler: ((Bool) -> Void)? = nil
    var arrcategorylist = [categorylist]()
    var arrRecurrntList = [RecurrentInfo]()
    var bundle: Bundle = Bundle()
    var adscount : Int = 0
     static let sharedInstance = UIApplication.shared.delegate as! AppDelegate
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        // Initialize Google Mobile Ads SDK
        GADMobileAds.sharedInstance().start(completionHandler: nil)
       
        setDrawerMenu()
        setDatabase()
        initialSetup()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {

        if let authResult = DropboxClientsManager.handleRedirectURL(url) {
            
            switch authResult {
                
            case .success:
                DLog("Success! User is logged into Dropbox.")
                if completionHadler != nil {
                    completionHadler!(true)
                }
            case .cancel:
                DLog("Authorization flow was manually canceled by user!")
                return false
            case .error(_, let description):
                DLog("Error: \(description)")
                return false
            }
        }

        if completionHadler != nil {
            completionHadler!(true)
        }
        
        return true
    }
}
extension UILabel {
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        if self.text != nil {
            self.text =  AppDelegate.sharedInstance.LocalizedString(text: self.text!)
            DLog(self.text)
        }
    }
    
}

extension AppDelegate {
    
    func setLanguage() {
        
        if let code = kdefaults.value(forKey: kLanguagecode) as? String {
            LanguageManager.sharedInstance.setLocale(code)
            AppDelegate.sharedInstance.bundle = LanguageManager.sharedInstance.getCurrentBundle()
        }
    }
    
    func initialSetup() {
        UNUserNotificationCenter.current().delegate = self
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = AppDelegate.sharedInstance.LocalizedString(text: Title.Alert.Done)
        DropboxClientsManager.authorizedClient = nil
        DropboxClientsManager.authorizedTeamClient = nil
        DropboxClientsManager.setupWithAppKey(ConstantValues.DROPBOX_APP_KEY)
        if UserDefaultManager.getNotification() == nil {
            UserDefaultManager.setNotification(Notification(isOn: true, time: ConstantValues.DEFAULT_NOTIFICATION_TIME))
        }
        
        let launchedBefore = kdefaults.bool(forKey: klaunchedBefore)
        if !launchedBefore{
            let lngcode = "en"
            kdefaults.set(lngcode, forKey: kLanguagecode)
            kdefaults.set("English", forKey: kLanguage)
            kdefaults.set(true, forKey: kShowAds)
            scheduleLocalNotification()
        }

        setLanguage()
        
        if UserDefaultManager.getData(forKey: Parameters.UserDefault.ThemeColor) == nil {
            UserDefaultManager.setData(value: ConstantValues.DEFAULT_THEME_COLOR, forKey: Parameters.UserDefault.ThemeColor)
        }
        
       
        
        if UserDefaultManager.getData(forKey: Parameters.UserDefault.TouchID) == nil {
            UserDefaultManager.setData(value: false, forKey: Parameters.UserDefault.TouchID)
        }
        
        if let isTouchIDUse = UserDefaultManager.getData(forKey: Parameters.UserDefault.TouchID) as? Bool, isTouchIDUse {
            PasscodeManager.sharedManager.openPasscodeView()
        }
        
        if UserDefaultManager.getAccountInfo() == nil {
            let arrAccount = AccountInfo.getAllAccount()
            if arrAccount.count > 0 {
                var object = arrAccount[0]
                object.status = AccountStatus.active.rawValue
                UserDefaultManager.setAccountInfo(object)
            }
        }
        
        if self.getCurrencyDetails() == nil {
            let locale = NSLocale.current as NSLocale
            let strCurrencySymbol : String = locale.object(forKey: .currencySymbol) as! String
            let strCurrencyCode : String = locale.object(forKey: .currencyCode) as! String
            
            
            var currencyInfo: CurrencyDATA = CurrencyDATA()
            
            currencyInfo.cURRENCYSNAME = strCurrencyCode
            currencyInfo.cURRENCYSYMBOL = strCurrencySymbol
            
            AppDelegate.sharedInstance.setCurrencyDetails(currencyInfo)
        }
        
        
        checkAddRecurrentTRansaction()
        requestForLocalNotification()
    }
    
    func requestForLocalNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound]) { (result, error) in
            DLog("Local Notification Request : \(result)")
        }
    }
    
    @objc func scheduleLocalNotification() {
        
        let center = UNUserNotificationCenter.current()

        let content = UNMutableNotificationContent()
        content.title = "Expense Manager"
        content.body = "Don't forget to track your expenses."
        content.categoryIdentifier = "alarm"
        content.sound = UNNotificationSound.default()
        var dateComponents = DateComponents()

        
        if let notification = UserDefaultManager.getNotification(), notification.isOn {
            let time = notification.time
            let arrData = time.split(separator: " ")
            if arrData.count > 1 {
               // let localize = APPDELEGATE.LocalizedString(text: String(arrData[1]))
               // time = arrData[0] + " " + localize
                let lngcode = kdefaults.value(forKey: kLanguagecode) as? String
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                dateFormatter.locale = Locale(identifier: lngcode!)
                dateFormatter.amSymbol = "AM"
                dateFormatter.pmSymbol = "PM"
                var date = dateFormatter.date(from: time)
                dateFormatter.dateFormat = "HH:mm"
                
                if date != nil {
                    
                    let Date24 = dateFormatter.string(from: date!)
                    let splitTime = Date24.split(separator: ":")
                    
                    dateComponents.day = Int(Date().getStringFromDate(dateFormat: DateFormat.day))
                    dateComponents.month = Int(Date().getStringFromDate(dateFormat: DateFormat.monthmm))
                    dateComponents.year = Int(Date().getStringFromDate(dateFormat: DateFormat.year))
                    dateComponents.hour = Int(splitTime[0])
                    dateComponents.minute = Int(splitTime[1])
                    dateComponents.second = 00
                    
                } else {
                    
                    date = Date()
                    dateComponents.day = Int(Date().getStringFromDate(dateFormat: DateFormat.day))
                    dateComponents.month = Int(Date().getStringFromDate(dateFormat: DateFormat.monthmm))
                    dateComponents.year = Int(Date().getStringFromDate(dateFormat: DateFormat.year))
                    dateComponents.hour = 08
                    dateComponents.minute = 00
                    dateComponents.second = 00
                }
                
            }
        }
      
        let date = Calendar.current.date(from: dateComponents)
        let triggerDaily = Calendar.current.dateComponents([.hour,.minute,.second,], from: date!)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
    }
    
    func checkAddRecurrentTRansaction(){
        //  let countDays  = 0
        let todayDate = Date().getStringFromDate(dateFormat: DateFormat.yearmonthday)
        arrRecurrntList = RecurrentInfo.getAllRecurrentTransacation()
        
        for i in 0..<arrRecurrntList.count {
            let strNextTransDate = arrRecurrntList[i].recurrentNextDate
            
            if strNextTransDate == todayDate{
                var arrTransactionInfo: TransactionInfo = TransactionInfo()
                arrTransactionInfo.transactionPayType = arrRecurrntList[i].recurrentPayType!
                arrTransactionInfo.transactionDate = arrRecurrntList[i].recurrentNextDate!
                arrTransactionInfo.transactionNote = arrRecurrntList[i].recurrentNote!
                arrTransactionInfo.transactionType = arrRecurrntList[i].recurrentTransactionType!
                arrTransactionInfo.transactionAmount = arrRecurrntList[i].recurrentAmount!
                arrTransactionInfo.transactionCatId = arrRecurrntList[i].recurrentCatId!
                arrTransactionInfo.transactionAccountId = arrRecurrntList[i].recurrentAccountId!
                
                if TransactionInfo.addTransaction(transactionInfo: arrTransactionInfo){
                    let strNewRecurrentDate = getNextDate(strRecurrentType: arrRecurrntList[i].recurrentType!, strDate: strNextTransDate!)
                    if RecurrentInfo.updateRecurrentTransNextDate(nextDate: strNewRecurrentDate, recurrentColumnId: arrRecurrntList[i].recurrentColumnId!){
                        DLog("Recurrent Next Date Updated")
                    }
                }
                
            }
        }
        
    }
    func getNextDate(strRecurrentType:String,strDate:String) ->String{
        var strNewNextDate : String = ""
        
        if strRecurrentType.lowercased() == "daily" {
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: strDate)
            let nextday = Calendar.current.date(byAdding: .day, value: 1, to: temDate!)
            strNewNextDate = getStringFromDate(dateFormat: DateFormat.yearmonthday, date: nextday!)
        }else if strRecurrentType.lowercased() == "weekly"{
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: strDate)
            let nextday = Calendar.current.date(byAdding: .day, value: 7, to: temDate!)
            strNewNextDate = getStringFromDate(dateFormat: DateFormat.yearmonthday, date: nextday!)
        }else{
            let temDate = getDateFromString(dateFormat: DateFormat.yearmonthday, strDate: strDate)
            let nextday = Calendar.current.date(byAdding: .month, value: 1, to: temDate!)
            strNewNextDate = getStringFromDate(dateFormat: DateFormat.yearmonthday, date: nextday!)
        }
        
        return strNewNextDate
    }
    func getDateFromString(dateFormat: String, strDate: String) -> Date? {
        
        dateFormatter.dateFormat = dateFormat
        
        return dateFormatter.date(from: strDate)
    }
    func getStringFromDate(dateFormat: String, date: Date) -> String {
        
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    func addDefaultCategory(accountId:Int32){
        arrcategorylist.removeAll()
        arrcategorylist.append(categorylist(name: "Clothes", iconname: "icn_category_cloths", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Drinks", iconname: "icn_category_drinks", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Education", iconname: "icn_category_education", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Food", iconname: "icn_category_food", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Fuel", iconname: "icn_category_fuel", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Fun", iconname: "icn_category_fun", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Health", iconname: "icn_category_health", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Highway", iconname: "icn_category_road", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Hotel", iconname: "icn_category_hotel", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Merchandise", iconname: "icn_category_merchandise", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Other", iconname: "icn_category_question", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Personal", iconname: "icn_category_personal", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Pets", iconname: "icn_category_pets", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Restaurant", iconname: "icn_category_restaurent", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Tips", iconname: "icn_category_tip", count: "0", accountId:accountId))
        arrcategorylist.append(categorylist(name: "Transport", iconname: "icn_category_transport", count: "0", accountId:accountId))
        
        addCategoryToDatabase()
    }
    func addCategoryToDatabase(){
        var tempCategoryInfo: CategoryManagment = CategoryManagment()
        
        self.arrcategorylist.enumerated().forEach { (index,object) in
            tempCategoryInfo.categoryName = object.name
            tempCategoryInfo.categoryIconName = object.iconname
            tempCategoryInfo.categoryCount = object.count
            tempCategoryInfo.categoryAccountId = object.accountId
            if CategoryManagment.addCategoryData(categoryInfo: tempCategoryInfo){
                DLog("Category Insert Success")
            }
        }
        
    }
    
    func setDrawerMenu() {
        
        let launchedBefore = kdefaults.bool(forKey: klaunchedBefore)
        if !launchedBefore {
            
            let vc = loadVC(Identifier.Storyboard.Main, strVCId: Identifier.ViewController.SelectLanguageVC)
            let nav = UINavigationController(rootViewController: vc)
            self.window?.rootViewController = nav
            self.window?.makeKeyAndVisible()
            
        } else {
            
            let menuLeftNavigationController = UIStoryboard(name: Identifier.Storyboard.Main, bundle: nil).instantiateViewController(withIdentifier: Identifier.ViewController.SideMenuNavigationController) as! UISideMenuNavigationController
            SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
            SideMenuManager.menuWidth = (self.window?.frame.width)! - 100
            SideMenuManager.menuFadeStatusBar = false
            
            let vc = UIStoryboard(name: Identifier.Storyboard.Main, bundle: nil).instantiateInitialViewController()
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
    }
    
    func setDatabase() {
        
        DLog(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        Util.copyFile(fileName: ConstantValues.DATABASE_NAME as NSString)
    }
    
    func showToastMessage(_ message: String, wantBottomSide: Bool = false, hideAutomatically : Bool = true) {
        
        runOnMainThread {
            
            if let window = APPDELEGATE.window {
                
                var toastMsg: MBProgressHUD!
                if let toastView = window.viewWithTag(Tag.toastMessage) as? MBProgressHUD {
                    toastMsg = toastView
                } else {
                    toastMsg = MBProgressHUD.showAdded(to: window, animated: true)
                }
                
                if wantBottomSide {
                    toastMsg.yOffset = Float((window.frame.size.height / 2) - 100)
                }
                
                toastMsg.removeFromSuperViewOnHide = true
                toastMsg?.tag = Tag.toastMessage
                toastMsg?.mode = MBProgressHUDModeText
                //                toastMsg?.detailsLabelFont = FONT_REGULAR(15.0)
                toastMsg?.detailsLabelText = message
                toastMsg.isUserInteractionEnabled = true
                if hideAutomatically {
                    toastMsg?.hide(true, afterDelay: 3.0)
                }
            }
        }
    }
    
    func hideToastMessage(_ message: String) {
        
        runOnMainThread {
            
            if let window = APPDELEGATE.window {
                if let toastView = window.viewWithTag(Tag.toastMessage) as? MBProgressHUD {
                    toastView.hide(true, afterDelay: 3.0)
                }
            }
        }
    }
    
    func showProgressHUD() {
        
        runOnMainThread {
            if let rootViewController = self.window?.rootViewController {
                MBProgressHUD.showAdded(to: rootViewController.view, animated: true)
            }
        }
    }
    
    func hideProgressHUD() {
        
        runOnMainThread {
            if let rootViewController = self.window?.rootViewController {
                MBProgressHUD.hideAllHUDs(for: rootViewController.view, animated: true)
            }
        }
    }
    
    func showAlert(title: String?, message: String?, actions: [String], completionHandler: ((Int) -> Void)?) {
        
        if let rootViewController = APPDELEGATE.window?.rootViewController {
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            for title in actions {
                let okAction = UIAlertAction(title: title, style: .default) { (action) in
                    if completionHandler != nil {
                        completionHandler!(alertController.actions.index(where: { (object) -> Bool in
                            return object == action
                        })!)
                    }
                }
                alertController.addAction(okAction)
            }
            
            DispatchQueue.main.async {
                rootViewController.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func getCategoryImage(iconName:String) -> String{
        
        var category : String = "";
        let categoryType = iconName.lowercased()
        category = "\(categoryType)"
        if (UIImage(named: category) != nil){
            return category
        }else{
            category = "icn_category_label"
            return category
        }
    }
    
    
    func getCategoryColor(iconName: String) -> UIColor{
        
        switch iconName {
        case "icn_category_bank":
            return UIColor(hexString: catogoryColor.icn_category_bank)
        case "icn_category_baseball":
            return UIColor(hexString: catogoryColor.icn_category_baseball)
        case "icn_category_basketball":
            return UIColor(hexString: catogoryColor.icn_category_baseball)
        case "icn_category_beer":
            return UIColor(hexString: catogoryColor.icn_category_beer)
        case "icn_category_bed":
            return UIColor(hexString: catogoryColor.icn_category_bed)
        case "icn_category_bicycle":
            return UIColor(hexString: catogoryColor.icn_category_bicycle)
        case"icn_category_boat":
            return UIColor(hexString: catogoryColor.icn_category_boat)
        case "icn_category_book":
            return UIColor(hexString: catogoryColor.icn_category_book)
        case "icn_category_bus":
            return UIColor(hexString: catogoryColor.icn_category_bus)
        case"icn_category_cake1":
            return UIColor(hexString: catogoryColor.icn_category_cake1)
        case "icn_category_cake2":
            return UIColor(hexString: catogoryColor.icn_category_cake2)
        case "icn_category_call_center":
            return UIColor(hexString: catogoryColor.icn_category_call_center)
        case "icn_category_car":
            return UIColor(hexString: catogoryColor.icn_category_car)
        case "icn_category_cloths":
            return UIColor(hexString: catogoryColor.icn_category_cloths)
        case "icn_category_computer":
            return UIColor(hexString: catogoryColor.icn_category_computer)
        case "icn_category_cricket":
            return UIColor(hexString: catogoryColor.icn_category_cricket)
        case "icn_category_dumbell":
            return UIColor(hexString: catogoryColor.icn_category_dumbell)
        case "icn_category_dress":
            return UIColor(hexString: catogoryColor.icn_category_dress)
        case "icn_category_drinks":
            return UIColor(hexString: catogoryColor.icn_category_drinks)
        case "icn_category_education":
            return UIColor(hexString: catogoryColor.icn_category_education)
        case "icn_category_factory":
            return UIColor(hexString: catogoryColor.icn_category_factory)
        case "icn_category_flight":
            return UIColor(hexString: catogoryColor.icn_category_flight)
        case "icn_category_food":
            return UIColor(hexString: catogoryColor.icn_category_food)
        case "icn_category_football":
            return UIColor(hexString: catogoryColor.icn_category_football)
        case "icn_category_fuel":
            return UIColor(hexString: catogoryColor.icn_category_fuel)
        case "icn_category_fun":
            return UIColor(hexString: catogoryColor.icn_category_fun)
        case "icn_category_glass":
            return UIColor(hexString: catogoryColor.icn_category_glass)
        case "icn_category_gift":
            return UIColor(hexString: catogoryColor.icn_category_gift)
        case "icn_category_headphone":
            return UIColor(hexString: catogoryColor.icn_category_headphone)
        case "icn_category_health":
            return UIColor(hexString: catogoryColor.icn_category_health)
        case "icn_category_home":
            return UIColor(hexString: catogoryColor.icn_category_home)
        case "icn_category_hotel":
            return UIColor(hexString: catogoryColor.icn_category_hotel)
        case "icn_category_label":
            return UIColor(hexString: catogoryColor.icn_category_label)
        case "icn_category_laptop":
            return UIColor(hexString: catogoryColor.icn_category_laptop)
        case "icn_category_mechanic":
            return UIColor(hexString: catogoryColor.icn_category_mechanic)
        case "icn_category_mechanic_tools":
            return UIColor(hexString: catogoryColor.icn_category_mechanic_tools)
        case "icn_category_merchandise":
            return UIColor(hexString: catogoryColor.icn_category_merchandise)
        case "icn_category_personal":
            return UIColor(hexString: catogoryColor.icn_category_personal)
        case "icn_category_pets":
            return UIColor(hexString: catogoryColor.icn_category_pets)
        case "icn_category_plumber":
            return UIColor(hexString: catogoryColor.icn_category_plumber)
        case "icn_category_pizza":
            return UIColor(hexString: catogoryColor.icn_category_pizza)
        case "icn_category_question":
            return UIColor(hexString: catogoryColor.icn_category_question)
        case "icn_category_restaurent":
            return UIColor(hexString: catogoryColor.icn_category_restaurent)
        case "icn_category_restaurent2":
            return UIColor(hexString: catogoryColor.icn_category_restaurent2)
        case "icn_category_road":
            return UIColor(hexString: catogoryColor.icn_category_road)
        case "icn_category_rugby":
            return UIColor(hexString: catogoryColor.icn_category_rugby)
        case "icn_category_sandle":
            return UIColor(hexString: catogoryColor.icn_category_sandle)
        case "icn_category_school":
            return UIColor(hexString: catogoryColor.icn_category_school)
        case "icn_category_scooter":
            return UIColor(hexString: catogoryColor.icn_category_scooter)
        case "icn_category_shoes":
            return UIColor(hexString: catogoryColor.icn_category_shoes)
        case "icn_category_taxi":
            return UIColor(hexString: catogoryColor.icn_category_taxi)
        case "icn_category_telephone":
            return UIColor(hexString: catogoryColor.icn_category_telephone)
        case "icn_category_tip":
            return UIColor(hexString: catogoryColor.icn_category_tip)
        case "icn_category_tickets":
            return UIColor(hexString: catogoryColor.icn_category_tickets)
        case "icn_category_train":
            return UIColor(hexString: catogoryColor.icn_category_train)
        case "icn_category_transport":
            return UIColor(hexString: catogoryColor.icn_category_transport)
        case "icn_category_tshirt":
            return UIColor(hexString: catogoryColor.icn_category_tshirt)
            
        default:
            return UIColor(hexString: catogoryColor.icn_category_personal)
        }
        
    }
    func setCurrencyDetails(_ dictionary: CurrencyDATA?){
        if dictionary != nil {
            if let encode = try? JSONEncoder().encode(dictionary) {
                kdefaults.set(encode, forKey: kcurrencyINFO)
                kdefaults.synchronize()
            }
        }
    }
    
    func getCurrencyDetails() -> CurrencyDATA? {
        
        if let userData = kdefaults.object(forKey: kcurrencyINFO) as? Data {
            if let decode = try? JSONDecoder().decode(CurrencyDATA.self, from: userData) {
                return decode
            }
        }
        return nil
    }
    func LocalizedString(text:String)->String{
        AppDelegate.sharedInstance.bundle =  LanguageManager.sharedInstance.getCurrentBundle()
        let temptext = NSLocalizedString(text, bundle: AppDelegate.sharedInstance.bundle , comment: "")
        return temptext
    }
    func showAppLoader(){
        SVProgressHUD.show(withStatus: "Please wait!")
    }
    
    func removeLoader(){
        SVProgressHUD.dismiss()
    }
   
}
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
}

