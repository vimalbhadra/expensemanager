//
//  AnalyticsTVC.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 13/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

class AnalyticsTVC: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
