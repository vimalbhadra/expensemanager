//
//  CategoryTVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 31/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

class SettingsTVC: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var switchNotification: UISwitch!
    @IBOutlet weak var lblTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
