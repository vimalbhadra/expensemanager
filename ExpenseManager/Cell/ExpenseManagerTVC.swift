//
//  ExpenseManagerTVC.swift
//  ExpenseManager
//
//  Created by Uffizio iMac on 29/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

class ExpenseManagerTVC: UITableViewCell {
    
    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblNotes: UILabel!
    @IBOutlet weak var lblTranscationAmount: UILabel!
    @IBOutlet weak var lblTranscationDate: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
