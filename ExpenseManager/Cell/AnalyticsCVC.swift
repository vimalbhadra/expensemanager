//
//  AnalyticsCVC.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 12/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit
import Charts

class AnalyticsCVC: UICollectionViewCell {
    
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var btnPreviousDate: UIButton!
    @IBOutlet weak var btnNextDate: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var lblNoTransaction: UILabel!
    @IBOutlet weak var chartSuperView: UIView!
    @IBOutlet weak var chartViewIncome: PieChartView!
    @IBOutlet weak var chartViewExpense: PieChartView!
    @IBOutlet weak var bottomView: BottomView!

    override func awakeFromNib() {
        
        self.tblView.tableFooterView = UIView(frame: .zero)
        btnFilter.setBorderAndCornerRadius()
        setChartViewProperties()
    }
}

extension AnalyticsCVC {
    
    func setTotalIncomeExpenseBalanceValue(income: Double, expense: Double) {
        
        bottomView.lblIncome.text = "\(AppDelegate.sharedInstance.LocalizedString(text: "INCOME")) \n\(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(income))"
        bottomView.lblIncome.textColor = Color.textColor.colorIncome
        bottomView.lblExpense.text = "\(AppDelegate.sharedInstance.LocalizedString(text: "EXPENSE")) \n\(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(expense))"
        bottomView.lblExpense.textColor = Color.textColor.colorExpense
        
        let balance = income - expense
        bottomView.lblBalance.text = "\(AppDelegate.sharedInstance.LocalizedString(text: "BALANCE"))\n\(AppDelegate.sharedInstance.getCurrencyDetails()!.cURRENCYSYMBOL!)\(String(balance))"
    }
    
    func setChartViewProperties() {
        
        let chartViews: [PieChartView] = [chartViewIncome, chartViewExpense]
        chartViews.forEach { (object) in
            object.drawEntryLabelsEnabled = false
            object.highlightValues(nil)
            object.drawSlicesUnderHoleEnabled = false
            object.chartDescription = nil
            object.legend.enabled = false
            object.holeRadiusPercent = 0.8
            object.transparentCircleRadiusPercent = 0
            object.noDataText = (object == chartViewIncome) ? Messages.noIncomeFound : Messages.noExpenseFound
            object.isUserInteractionEnabled = false
        }
    }
    
    func setChartData(object: AnalyticsData) {
        
        if object.arrSectionData.count > 0 {
            
            let entriesExpense = (0...object.arrSectionData.count - 1).map { (i) -> PieChartDataEntry in
                return PieChartDataEntry(value: Double(object.arrSectionData[i].expense)!)
            }
            
            if !entriesExpense.contains(where: { (dataEntry) -> Bool in
                return dataEntry.y > 0
            }) {
                setChartEntries(object: object, chartView: chartViewExpense, entries: entriesExpense, message: Messages.expenseFound)
            } else {
                setChartEntries(object: object, chartView: chartViewExpense, entries: entriesExpense, message: Messages.expenseFound)
            }
            
            let entriesIncome = (0...object.arrSectionData.count - 1).map { (i) -> PieChartDataEntry in
                return PieChartDataEntry(value: Double(object.arrSectionData[i].income)!)
            }
            
            if !entriesIncome.contains(where: { (dataEntry) -> Bool in
                return dataEntry.y > 0
            }) {
                setChartEntries(object: object, chartView: chartViewIncome, entries: [PieChartDataEntry(value: 1)], message: Messages.incomeFound)
            } else {
                setChartEntries(object: object, chartView: chartViewIncome, entries: entriesIncome, message: Messages.incomeFound)
            }
            
        } else {
            setChartEntries(object: object, chartView: chartViewExpense, entries: [PieChartDataEntry(value: 1)], message: Messages.expenseFound)
            setChartEntries(object: object, chartView: chartViewIncome, entries: [PieChartDataEntry(value: 1)], message: Messages.incomeFound)
        }
    }
    
    private func setChartEntries(object: AnalyticsData, chartView: PieChartView, entries: [PieChartDataEntry], message: String) {
        
        let set = PieChartDataSet(values: entries, label: nil)
        set.drawIconsEnabled = false
        set.sliceSpace = 2
        set.drawValuesEnabled = false
        set.selectionShift = 0
        
        let data = PieChartData(dataSet: set)
        chartView.data = data
        
        var count = 0
        var arrColors = [UIColor]()
        for tempObject in object.arrSectionData {
            count += tempObject.arrRowData.filter { (dashboardInfo) -> Bool in
                arrColors.append(AppDelegate.sharedInstance.getCategoryColor(iconName: tempObject.categoryImageName))
                if chartView == chartViewExpense {
                    return dashboardInfo.transactionType == TransactionTypeName.EXPENSE.rawValue
                } else {
                    return dashboardInfo.transactionType == TransactionTypeName.INCOME.rawValue
                }
                }.count
        }
        
        if count > 0, arrColors.count > 0 {
            set.colors = arrColors
        } else {
            set.colors = [UIColor.lightGray]
        }
        
        let paragraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        paragraphStyle.alignment = NSTextAlignment.center
        
        var centerMessage = "  \(count) \(message)  "
        if count == 0 {
            centerMessage = (chartView == chartViewIncome) ? Messages.noIncomeFound : Messages.noExpenseFound
        }
        
        let strExpense = centerMessage
        let centerText = NSMutableAttributedString(string: strExpense)
        centerText.setAttributes([NSAttributedStringKey.paragraphStyle: paragraphStyle], range: NSRange(location: 0, length: strExpense.count))
        chartView.centerAttributedText = centerText
    }
}
