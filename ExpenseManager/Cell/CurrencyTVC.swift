//
//  CurrencyTVC.swift
//  ExpenseManager
//
//  Created by Piyu on 09/02/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

class CurrencyTVC: UITableViewCell {
    
    @IBOutlet weak var btnRadio: UIButton!
    @IBOutlet weak var lblCurencyName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
