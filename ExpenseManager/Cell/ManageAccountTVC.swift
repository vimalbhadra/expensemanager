//
//  AnalyticsTVC.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 13/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

class ManageAccountTVC: UITableViewCell {
    
    @IBOutlet weak var lblAccountName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var switchStatus: UISwitch!
    @IBOutlet weak var btnAddAccount: UIButton!
    @IBOutlet weak var btnEditAccount: UIButton!
    @IBOutlet weak var btnDeleteAccount: UIButton!
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var viewShadow: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
