//
//  AddEditTVC.swift
//  ExpenseManager
//
//  Created by Uffizio iMac on 29/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

class AddEditTVC: UITableViewCell {

    @IBOutlet weak var lblDisc: UILabel!
    @IBOutlet weak var lblValue: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class AddEditAmountTVC: UITableViewCell {
    
    @IBOutlet weak var lblDisc: UILabel!
    @IBOutlet weak var lblCurrencySymbol: UILabel!

    @IBOutlet weak var txtAmountValue: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
