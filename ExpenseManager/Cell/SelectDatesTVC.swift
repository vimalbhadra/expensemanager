//
//  PaymentTypeTVC.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 13/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

class SelectDatesTVC: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
