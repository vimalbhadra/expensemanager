//
//  PaymentTypeTVC.swift
//  ExpenseManager
//
//  Created by Vimal Bhadra on 13/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

class PaymentTypeTVC: UITableViewCell {
    
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
