//
//  DrawerMenuTVC.swift
//  ExpenseManager
//
//  Created by Apple Customer on 12/01/19.
//  Copyright © 2019 Vimal Bhadra. All rights reserved.
//

import UIKit

class TableSectionHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
}

class DrawerMenuTVC: UITableViewCell {

    @IBOutlet weak var imgViewIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCircleName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
